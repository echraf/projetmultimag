﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouchDAO.Entities
{
    public class Magasin
    {
        public String code { get; set; }
        public string désignation { get; set; }
        public string obs { get; set; }

    }
}
