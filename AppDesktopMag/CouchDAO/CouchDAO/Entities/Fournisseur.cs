﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouchDAO.Entities
{
    public class Fournisseur
    {
        public string FRS_CODE { get; set; }
        public string FRS_NOM { get; set; }
        public string FRS_ADR { get; set; }
        public string FRS_TEL { get; set; }
        public string FRS_FAX { get; set; }
        public string FRS_EMAIL { get; set; }
        public string FRS_RAIOSOC { get; set; }
        public string FRS_OBSERV { get; set; }
        public string FRS_VILE { get; set; }
        public string FRS_TYPE { get; set; }
        public string FRS_ETAT { get; set; }
        public string FRS_OBS1 { get; set; }
       
    }
}
