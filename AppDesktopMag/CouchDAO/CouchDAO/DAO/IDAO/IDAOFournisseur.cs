﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CouchDAO.DAO.IDAO;
using CouchDAO.Entities;

namespace CouchDAO.DAO.IDAO
{
    interface IDAOFournisseur
    {
        void addFournisseur(Fournisseur f);
        void updateFournisseur(String FRS_CODE);
        Fournisseur searchFournisseur(string FRS_CODE);
        List<Fournisseur> GetListeFournisseur();
    }
}
