﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CouchDAO.Entities;

namespace CouchDAO.DAO.IDAO
{
    interface IDAOMagasin
    {
        void addMagasin(Magasin m);
        void updateMagasin(String code);
        Fournisseur searchMagasin(string code);
        List<Magasin> GetListeMagasin();
    }
}
