﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CouchDAO.Entities;
namespace CouchDAO.DAO.IDAO
{
    interface IDAOArticle
    {
        void addArticle(Article a);
        void updateArticle(String CODART);
        // Article searchArticle(string CODART);
        //List<Article> GetListeArticle();
    }
}
