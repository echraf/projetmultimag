﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CouchDAO.DAO.IDAO;
using CouchDAO.Entities;
using CouchDAO.Connexion;
using System.Data.SqlClient;
using System.Data;

namespace CouchDAO.DAO.DAOImp
{
    public class DAOImpArticle : IDAOArticle
    {
        SqlCommand cmd = null;
        SqlConnection conn = new SqlConnection();
        string chaineConnexion = "Data Source=ADMIN-PC\\SQL2014;Initial Catalog=DATA;Integrated Security=True";
        ConnexionBD conBD=new ConnexionBD();
       
        public void addArticle(Article a)
        {
            conn = new SqlConnection();
            conBD = new ConnexionBD();
            conn = conBD.GetDBConnection(chaineConnexion);
            conn.Open();
            cmd = conn.CreateCommand();
            cmd.CommandText =" insert ARTICLE(CODART, departement, rayon, Famille, DESART, REFART, fournisseur) values(?,?,?,?,?,?,?)";
            cmd.Parameters.Add("CODART", SqlDbType.VarChar).Value = a.CODART;
            cmd.Parameters.Add("departement", SqlDbType.VarChar).Value = a.departement;
            cmd.Parameters.Add("rayon", SqlDbType.VarChar).Value = a.rayon;
            cmd.Parameters.Add("Famille", SqlDbType.VarChar).Value =a.Famille;
            cmd.Parameters.Add("DESART", SqlDbType.VarChar).Value = a.DESART;
            cmd.Parameters.Add("REFART", SqlDbType.VarChar).Value = a.REFART;
            cmd.Parameters.Add("fournisseur", SqlDbType.VarChar).Value = a.fournisseur;
            
            conn.Close();

        }/*
        public Article searchArticle(string CODART)
        {
            cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT * from ARTICLE where CODART ='" + CODART + "';";
            cmd.ExecuteNonQuery();

            SqlDataReader dr = cmd.ExecuteReader();
            Article art = null;

            if (dr.Read())
            art = new Article(Convert.ToString(dr.GetValue(0)), Convert.ToString(dr.GetValue(1)), Convert.ToString(dr.GetValue(2)), Convert.ToString(dr.GetValue(3)), Convert.ToString(dr.GetValue(4)), Convert.ToString(dr.GetValue(5)), Convert.ToString(dr.GetValue(6)), Convert.ToString(dr.GetValue(7)), Convert.ToString(dr.GetValue(8)));

            dr.Close();
            conn.Close();

            return art;
        }

        */
        /*
        public List<Article> GetListeArticle()
        {
            conn = new SqlConnection();
            conBD = new ConnexionBD();
            conn = conBD.GetDBConnection(chaineConnexion);
            String Requete = "SELECT * from ARTICLE";
            cmd = new SqlCommand(Requete, conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            List<Article> Liste_Article = new List<Article>();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Article art = new Article(Convert.ToString(dr.GetValue(0)), Convert.ToString(dr.GetValue(1)), Convert.ToString(dr.GetValue(2)), Convert.ToString(dr.GetValue(3)), Convert.ToString(dr.GetValue(4)), Convert.ToString(dr.GetValue(5)), Convert.ToString(dr.GetValue(6)), Convert.ToString(dr.GetValue(7)), Convert.ToString(dr.GetValue(8)));
                    Liste_Article.Add(art);
                }

            }

            dr.Close();
            conn.Close();

            return Liste_Article;
        }
        */
        public void updateArticle(string CODART)
        {
        }
    }

}
