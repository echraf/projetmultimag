﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace CouchDAO.Connexion
{
   public  class ConnexionBD
    {
        private  SqlConnection conn;
        public ConnexionBD()
        {

        }

        public SqlConnection GetDBConnection(String chaineConnexionBD)
        {
            conn = new SqlConnection();

            try
            {
                conn.ConnectionString = chaineConnexionBD;
                conn.Open();
                Console.WriteLine("Connected");
            }
            catch (SqlException e)
            {

                Console.WriteLine(e.Message);
            }
            conn.Close();

            return conn;
        }

    }

}
