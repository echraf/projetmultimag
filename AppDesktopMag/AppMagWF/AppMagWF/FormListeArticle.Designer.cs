﻿namespace AppMagWF
{
    partial class FormListeArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label1 = new System.Windows.Forms.Label();
            this.DataGridArticle = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridArticle)).BeginInit();
            this.SuspendLayout();
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label1.Location = new System.Drawing.Point(446, 42);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(180, 24);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "Listes Des Articles";
            // 
            // DataGridArticle
            // 
            this.DataGridArticle.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridArticle.BackgroundColor = System.Drawing.SystemColors.ButtonShadow;
            this.DataGridArticle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridArticle.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.DataGridArticle.Location = new System.Drawing.Point(19, 139);
            this.DataGridArticle.Name = "DataGridArticle";
            this.DataGridArticle.Size = new System.Drawing.Size(1104, 218);
            this.DataGridArticle.TabIndex = 2;
            // 
            // FormListeArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1135, 450);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.DataGridArticle);
            this.Name = "FormListeArticle";
            this.Text = "FormListeArticle";
            this.Load += new System.EventHandler(this.FormListeArticle_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridArticle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.DataGridView DataGridArticle;
    }
}