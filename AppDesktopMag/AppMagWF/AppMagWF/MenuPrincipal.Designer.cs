﻿namespace AppMagWF
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuPrincipal));
            this.MenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.FichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.QuitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ArticleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NouveauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ListeDesArticlesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ModifierArticleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FournisseurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NouveauToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ListeDesFournisseursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CommandeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NouvelleCommandeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ConsulterCommandeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuStrip1
            // 
            this.MenuStrip1.BackColor = System.Drawing.Color.MediumPurple;
            this.MenuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FichierToolStripMenuItem,
            this.ArticleToolStripMenuItem,
            this.FournisseurToolStripMenuItem,
            this.CommandeToolStripMenuItem,
            this.ToolStripMenuItem2,
            this.ToolStripMenuItem1});
            this.MenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip1.Name = "MenuStrip1";
            this.MenuStrip1.Size = new System.Drawing.Size(201, 406);
            this.MenuStrip1.TabIndex = 1;
            this.MenuStrip1.Text = "MenuStrip1";
            // 
            // FichierToolStripMenuItem
            // 
            this.FichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.QuitterToolStripMenuItem});
            this.FichierToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FichierToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FichierToolStripMenuItem.Margin = new System.Windows.Forms.Padding(15, 50, 15, 15);
            this.FichierToolStripMenuItem.Name = "FichierToolStripMenuItem";
            this.FichierToolStripMenuItem.Size = new System.Drawing.Size(158, 29);
            this.FichierToolStripMenuItem.Text = "Fichier";
            // 
            // QuitterToolStripMenuItem
            // 
            this.QuitterToolStripMenuItem.Name = "QuitterToolStripMenuItem";
            this.QuitterToolStripMenuItem.Size = new System.Drawing.Size(180, 30);
            this.QuitterToolStripMenuItem.Text = "Quitter";
            // 
            // ArticleToolStripMenuItem
            // 
            this.ArticleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NouveauToolStripMenuItem,
            this.ListeDesArticlesToolStripMenuItem,
            this.ModifierArticleToolStripMenuItem});
            this.ArticleToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ArticleToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ArticleToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ArticleToolStripMenuItem.Image")));
            this.ArticleToolStripMenuItem.Margin = new System.Windows.Forms.Padding(15);
            this.ArticleToolStripMenuItem.Name = "ArticleToolStripMenuItem";
            this.ArticleToolStripMenuItem.Size = new System.Drawing.Size(158, 29);
            this.ArticleToolStripMenuItem.Text = "Article";
            this.ArticleToolStripMenuItem.Click += new System.EventHandler(this.ArticleToolStripMenuItem_Click);
            // 
            // NouveauToolStripMenuItem
            // 
            this.NouveauToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("NouveauToolStripMenuItem.Image")));
            this.NouveauToolStripMenuItem.Name = "NouveauToolStripMenuItem";
            this.NouveauToolStripMenuItem.Size = new System.Drawing.Size(264, 30);
            this.NouveauToolStripMenuItem.Text = "NouveauArticle";
            this.NouveauToolStripMenuItem.Click += new System.EventHandler(this.NouveauToolStripMenuItem_Click);
            // 
            // ListeDesArticlesToolStripMenuItem
            // 
            this.ListeDesArticlesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ListeDesArticlesToolStripMenuItem.Image")));
            this.ListeDesArticlesToolStripMenuItem.Name = "ListeDesArticlesToolStripMenuItem";
            this.ListeDesArticlesToolStripMenuItem.Size = new System.Drawing.Size(264, 30);
            this.ListeDesArticlesToolStripMenuItem.Text = "Liste des articles";
            this.ListeDesArticlesToolStripMenuItem.Click += new System.EventHandler(this.ListeDesArticlesToolStripMenuItem_Click);
            // 
            // ModifierArticleToolStripMenuItem
            // 
            this.ModifierArticleToolStripMenuItem.Name = "ModifierArticleToolStripMenuItem";
            this.ModifierArticleToolStripMenuItem.Size = new System.Drawing.Size(264, 30);
            this.ModifierArticleToolStripMenuItem.Text = "Modifier Article";
            // 
            // FournisseurToolStripMenuItem
            // 
            this.FournisseurToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NouveauToolStripMenuItem1,
            this.ListeDesFournisseursToolStripMenuItem});
            this.FournisseurToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FournisseurToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FournisseurToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("FournisseurToolStripMenuItem.Image")));
            this.FournisseurToolStripMenuItem.Margin = new System.Windows.Forms.Padding(15);
            this.FournisseurToolStripMenuItem.Name = "FournisseurToolStripMenuItem";
            this.FournisseurToolStripMenuItem.Size = new System.Drawing.Size(158, 29);
            this.FournisseurToolStripMenuItem.Text = "Fournisseur";
            // 
            // NouveauToolStripMenuItem1
            // 
            this.NouveauToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("NouveauToolStripMenuItem1.Image")));
            this.NouveauToolStripMenuItem1.Name = "NouveauToolStripMenuItem1";
            this.NouveauToolStripMenuItem1.Size = new System.Drawing.Size(317, 30);
            this.NouveauToolStripMenuItem1.Text = "Nouveau Fournisseur";
            // 
            // ListeDesFournisseursToolStripMenuItem
            // 
            this.ListeDesFournisseursToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ListeDesFournisseursToolStripMenuItem.Image")));
            this.ListeDesFournisseursToolStripMenuItem.Name = "ListeDesFournisseursToolStripMenuItem";
            this.ListeDesFournisseursToolStripMenuItem.Size = new System.Drawing.Size(317, 30);
            this.ListeDesFournisseursToolStripMenuItem.Text = "Liste des fournisseurs";
            // 
            // CommandeToolStripMenuItem
            // 
            this.CommandeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NouvelleCommandeToolStripMenuItem,
            this.ConsulterCommandeToolStripMenuItem});
            this.CommandeToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CommandeToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.CommandeToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("CommandeToolStripMenuItem.Image")));
            this.CommandeToolStripMenuItem.Margin = new System.Windows.Forms.Padding(15);
            this.CommandeToolStripMenuItem.Name = "CommandeToolStripMenuItem";
            this.CommandeToolStripMenuItem.Size = new System.Drawing.Size(158, 29);
            this.CommandeToolStripMenuItem.Text = "Commande";
            // 
            // NouvelleCommandeToolStripMenuItem
            // 
            this.NouvelleCommandeToolStripMenuItem.Name = "NouvelleCommandeToolStripMenuItem";
            this.NouvelleCommandeToolStripMenuItem.Size = new System.Drawing.Size(309, 30);
            this.NouvelleCommandeToolStripMenuItem.Text = "Nouvelle Commande";
            // 
            // ConsulterCommandeToolStripMenuItem
            // 
            this.ConsulterCommandeToolStripMenuItem.Name = "ConsulterCommandeToolStripMenuItem";
            this.ConsulterCommandeToolStripMenuItem.Size = new System.Drawing.Size(309, 30);
            this.ConsulterCommandeToolStripMenuItem.Text = "Consulter Commande";
            // 
            // ToolStripMenuItem2
            // 
            this.ToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem3,
            this.ToolStripMenuItem4});
            this.ToolStripMenuItem2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToolStripMenuItem2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ToolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripMenuItem2.Image")));
            this.ToolStripMenuItem2.Margin = new System.Windows.Forms.Padding(15);
            this.ToolStripMenuItem2.Name = "ToolStripMenuItem2";
            this.ToolStripMenuItem2.Size = new System.Drawing.Size(158, 29);
            this.ToolStripMenuItem2.Text = "Magasin";
            // 
            // ToolStripMenuItem3
            // 
            this.ToolStripMenuItem3.Name = "ToolStripMenuItem3";
            this.ToolStripMenuItem3.Size = new System.Drawing.Size(291, 30);
            this.ToolStripMenuItem3.Text = "Nouveau Magasin";
            // 
            // ToolStripMenuItem4
            // 
            this.ToolStripMenuItem4.Name = "ToolStripMenuItem4";
            this.ToolStripMenuItem4.Size = new System.Drawing.Size(291, 30);
            this.ToolStripMenuItem4.Text = "Liste Des Magasins";
            // 
            // ToolStripMenuItem1
            // 
            this.ToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ToolStripMenuItem1.Name = "ToolStripMenuItem1";
            this.ToolStripMenuItem1.Size = new System.Drawing.Size(188, 29);
            this.ToolStripMenuItem1.Text = "?";
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 406);
            this.Controls.Add(this.MenuStrip1);
            this.Name = "MenuPrincipal";
            this.Text = "MenuPrincipal";
            this.MenuStrip1.ResumeLayout(false);
            this.MenuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.MenuStrip MenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem FichierToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem QuitterToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ArticleToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem NouveauToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ListeDesArticlesToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ModifierArticleToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem FournisseurToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem NouveauToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ListeDesFournisseursToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem CommandeToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem NouvelleCommandeToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ConsulterCommandeToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem2;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem3;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem4;
    }
}