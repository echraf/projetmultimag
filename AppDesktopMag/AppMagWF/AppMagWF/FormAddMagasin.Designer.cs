﻿namespace AppMagWF
{
    partial class FormAddMagasin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddMagasin));
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.ButtonModifier = new System.Windows.Forms.Button();
            this.Label11 = new System.Windows.Forms.Label();
            this.ButtonSupprimer = new System.Windows.Forms.Button();
            this.obs = new System.Windows.Forms.TextBox();
            this.ButtonAjouter = new System.Windows.Forms.Button();
            this.Label3 = new System.Windows.Forms.Label();
            this.ButtonNouveau = new System.Windows.Forms.Button();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.dscrpt = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.code = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.DataGridView1);
            this.GroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.Location = new System.Drawing.Point(24, 261);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(449, 229);
            this.GroupBox1.TabIndex = 128;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Listes Des Magasins";
            // 
            // DataGridView1
            // 
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridView1.Location = new System.Drawing.Point(3, 17);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.Size = new System.Drawing.Size(443, 209);
            this.DataGridView1.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "CodeMag";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "AdressemMag";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Description";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "OBS";
            this.Column4.Name = "Column4";
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel1.Controls.Add(this.ButtonModifier);
            this.Panel1.Controls.Add(this.Label11);
            this.Panel1.Controls.Add(this.ButtonSupprimer);
            this.Panel1.Controls.Add(this.obs);
            this.Panel1.Controls.Add(this.ButtonAjouter);
            this.Panel1.Controls.Add(this.Label3);
            this.Panel1.Controls.Add(this.ButtonNouveau);
            this.Panel1.Controls.Add(this.TextBox1);
            this.Panel1.Controls.Add(this.dscrpt);
            this.Panel1.Controls.Add(this.Label2);
            this.Panel1.Controls.Add(this.Label1);
            this.Panel1.Controls.Add(this.code);
            this.Panel1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Panel1.Location = new System.Drawing.Point(97, 62);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(320, 194);
            this.Panel1.TabIndex = 127;
            // 
            // ButtonModifier
            // 
            this.ButtonModifier.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonModifier.BackgroundImage")));
            this.ButtonModifier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonModifier.Location = new System.Drawing.Point(258, 56);
            this.ButtonModifier.Name = "ButtonModifier";
            this.ButtonModifier.Size = new System.Drawing.Size(42, 40);
            this.ButtonModifier.TabIndex = 128;
            this.ButtonModifier.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonModifier.UseVisualStyleBackColor = true;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label11.Location = new System.Drawing.Point(23, 153);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(39, 16);
            this.Label11.TabIndex = 151;
            this.Label11.Text = "OBS";
            // 
            // ButtonSupprimer
            // 
            this.ButtonSupprimer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonSupprimer.BackgroundImage")));
            this.ButtonSupprimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonSupprimer.Location = new System.Drawing.Point(258, 94);
            this.ButtonSupprimer.Name = "ButtonSupprimer";
            this.ButtonSupprimer.Size = new System.Drawing.Size(42, 38);
            this.ButtonSupprimer.TabIndex = 127;
            this.ButtonSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonSupprimer.UseVisualStyleBackColor = true;
            // 
            // obs
            // 
            this.obs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.obs.Location = new System.Drawing.Point(129, 149);
            this.obs.Name = "obs";
            this.obs.Size = new System.Drawing.Size(107, 20);
            this.obs.TabIndex = 150;
            // 
            // ButtonAjouter
            // 
            this.ButtonAjouter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonAjouter.BackgroundImage")));
            this.ButtonAjouter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonAjouter.Location = new System.Drawing.Point(258, 130);
            this.ButtonAjouter.Name = "ButtonAjouter";
            this.ButtonAjouter.Size = new System.Drawing.Size(41, 36);
            this.ButtonAjouter.TabIndex = 125;
            this.ButtonAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonAjouter.UseVisualStyleBackColor = true;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label3.Location = new System.Drawing.Point(23, 110);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(100, 16);
            this.Label3.TabIndex = 149;
            this.Label3.Text = "Adresse Mag";
            // 
            // ButtonNouveau
            // 
            this.ButtonNouveau.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonNouveau.BackgroundImage")));
            this.ButtonNouveau.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonNouveau.Location = new System.Drawing.Point(258, 17);
            this.ButtonNouveau.Name = "ButtonNouveau";
            this.ButtonNouveau.Size = new System.Drawing.Size(42, 42);
            this.ButtonNouveau.TabIndex = 126;
            this.ButtonNouveau.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonNouveau.UseVisualStyleBackColor = true;
            // 
            // TextBox1
            // 
            this.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TextBox1.Location = new System.Drawing.Point(129, 106);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(107, 20);
            this.TextBox1.TabIndex = 148;
            // 
            // dscrpt
            // 
            this.dscrpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dscrpt.Location = new System.Drawing.Point(129, 62);
            this.dscrpt.Name = "dscrpt";
            this.dscrpt.Size = new System.Drawing.Size(106, 20);
            this.dscrpt.TabIndex = 120;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label2.Location = new System.Drawing.Point(15, 66);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(91, 16);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "Description ";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label1.Location = new System.Drawing.Point(15, 17);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(108, 16);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "Code Magasin";
            // 
            // code
            // 
            this.code.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.code.Location = new System.Drawing.Point(129, 17);
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(107, 20);
            this.code.TabIndex = 6;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.Black;
            this.Label4.Location = new System.Drawing.Point(114, 10);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(290, 28);
            this.Label4.TabIndex = 126;
            this.Label4.Text = "Ajouter Nouveau Magasin";
            // 
            // FormAddMagasin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 450);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.Label4);
            this.Name = "FormAddMagasin";
            this.Text = "FormAddMagasin";
            this.GroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        internal System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Button ButtonModifier;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Button ButtonSupprimer;
        internal System.Windows.Forms.TextBox obs;
        internal System.Windows.Forms.Button ButtonAjouter;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Button ButtonNouveau;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.TextBox dscrpt;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox code;
        internal System.Windows.Forms.Label Label4;
    }
}