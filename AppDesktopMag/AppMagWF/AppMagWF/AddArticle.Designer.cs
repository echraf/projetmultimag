﻿namespace AppMagWF
{
    partial class AddArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddArticle));
            this.Panel1 = new System.Windows.Forms.Panel();
            this.ButtonSupprimer = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.ButtonModifier = new System.Windows.Forms.Button();
            this.codeCat = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Panel11 = new System.Windows.Forms.Panel();
            this.Panel10 = new System.Windows.Forms.Panel();
            this.Panel9 = new System.Windows.Forms.Panel();
            this.Panel8 = new System.Windows.Forms.Panel();
            this.Panel7 = new System.Windows.Forms.Panel();
            this.Panel5 = new System.Windows.Forms.Panel();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textMar = new System.Windows.Forms.TextBox();
            this.textFournisseur = new System.Windows.Forms.TextBox();
            this.textRayon = new System.Windows.Forms.TextBox();
            this.textDep = new System.Windows.Forms.TextBox();
            this.textSFamille = new System.Windows.Forms.TextBox();
            this.textFamille = new System.Windows.Forms.TextBox();
            this.textRef = new System.Windows.Forms.TextBox();
            this.textDescri = new System.Windows.Forms.TextBox();
            this.textCodCat = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.textCode = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel1.Controls.Add(this.ButtonSupprimer);
            this.Panel1.Controls.Add(this.Button1);
            this.Panel1.Controls.Add(this.Button2);
            this.Panel1.Controls.Add(this.ButtonModifier);
            this.Panel1.Controls.Add(this.codeCat);
            this.Panel1.Controls.Add(this.Label11);
            this.Panel1.Controls.Add(this.Label10);
            this.Panel1.Controls.Add(this.Label9);
            this.Panel1.Controls.Add(this.Label8);
            this.Panel1.Controls.Add(this.Label7);
            this.Panel1.Controls.Add(this.Label6);
            this.Panel1.Controls.Add(this.Label5);
            this.Panel1.Controls.Add(this.Panel11);
            this.Panel1.Controls.Add(this.Panel10);
            this.Panel1.Controls.Add(this.Panel9);
            this.Panel1.Controls.Add(this.Panel8);
            this.Panel1.Controls.Add(this.Panel7);
            this.Panel1.Controls.Add(this.Panel5);
            this.Panel1.Controls.Add(this.Panel4);
            this.Panel1.Controls.Add(this.Panel3);
            this.Panel1.Controls.Add(this.Panel2);
            this.Panel1.Controls.Add(this.panel6);
            this.Panel1.Controls.Add(this.textMar);
            this.Panel1.Controls.Add(this.textFournisseur);
            this.Panel1.Controls.Add(this.textRayon);
            this.Panel1.Controls.Add(this.textDep);
            this.Panel1.Controls.Add(this.textSFamille);
            this.Panel1.Controls.Add(this.textFamille);
            this.Panel1.Controls.Add(this.textRef);
            this.Panel1.Controls.Add(this.textDescri);
            this.Panel1.Controls.Add(this.textCodCat);
            this.Panel1.Controls.Add(this.Label2);
            this.Panel1.Controls.Add(this.Label1);
            this.Panel1.Controls.Add(this.textCode);
            this.Panel1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Panel1.Location = new System.Drawing.Point(64, 71);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(672, 300);
            this.Panel1.TabIndex = 124;
            // 
            // ButtonSupprimer
            // 
            this.ButtonSupprimer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonSupprimer.BackgroundImage")));
            this.ButtonSupprimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonSupprimer.Location = new System.Drawing.Point(270, 14);
            this.ButtonSupprimer.Name = "ButtonSupprimer";
            this.ButtonSupprimer.Size = new System.Drawing.Size(42, 38);
            this.ButtonSupprimer.TabIndex = 148;
            this.ButtonSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonSupprimer.UseVisualStyleBackColor = true;
            // 
            // Button1
            // 
            this.Button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button1.BackgroundImage")));
            this.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Button1.Location = new System.Drawing.Point(205, 16);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(41, 36);
            this.Button1.TabIndex = 146;
            this.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Button2
            // 
            this.Button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button2.BackgroundImage")));
            this.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Button2.Location = new System.Drawing.Point(406, 10);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(42, 42);
            this.Button2.TabIndex = 149;
            this.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button2.UseVisualStyleBackColor = true;
            // 
            // ButtonModifier
            // 
            this.ButtonModifier.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonModifier.BackgroundImage")));
            this.ButtonModifier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonModifier.Location = new System.Drawing.Point(340, 12);
            this.ButtonModifier.Name = "ButtonModifier";
            this.ButtonModifier.Size = new System.Drawing.Size(42, 40);
            this.ButtonModifier.TabIndex = 147;
            this.ButtonModifier.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonModifier.UseVisualStyleBackColor = true;
            // 
            // codeCat
            // 
            this.codeCat.AutoSize = true;
            this.codeCat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codeCat.Location = new System.Drawing.Point(344, 93);
            this.codeCat.Name = "codeCat";
            this.codeCat.Size = new System.Drawing.Size(117, 16);
            this.codeCat.TabIndex = 145;
            this.codeCat.Text = "Code Categorie";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.Location = new System.Drawing.Point(344, 252);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(59, 16);
            this.Label11.TabIndex = 144;
            this.Label11.Text = "Markue";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.Location = new System.Drawing.Point(24, 210);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(97, 16);
            this.Label10.TabIndex = 143;
            this.Label10.Text = "Departement";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.Location = new System.Drawing.Point(344, 169);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(98, 16);
            this.Label9.TabIndex = 142;
            this.Label9.Text = "Sous Famille";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(344, 134);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(84, 16);
            this.Label8.TabIndex = 141;
            this.Label8.Text = "Referance ";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(26, 254);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(89, 16);
            this.Label7.TabIndex = 140;
            this.Label7.Text = "Fournisseur";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(344, 210);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(57, 16);
            this.Label6.TabIndex = 139;
            this.Label6.Text = "Rayon ";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(25, 173);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(59, 16);
            this.Label5.TabIndex = 138;
            this.Label5.Text = "Famille";
            // 
            // Panel11
            // 
            this.Panel11.BackColor = System.Drawing.Color.White;
            this.Panel11.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel11.Location = new System.Drawing.Point(451, 277);
            this.Panel11.Name = "Panel11";
            this.Panel11.Size = new System.Drawing.Size(200, 1);
            this.Panel11.TabIndex = 137;
            // 
            // Panel10
            // 
            this.Panel10.BackColor = System.Drawing.Color.White;
            this.Panel10.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel10.Location = new System.Drawing.Point(451, 236);
            this.Panel10.Name = "Panel10";
            this.Panel10.Size = new System.Drawing.Size(200, 1);
            this.Panel10.TabIndex = 136;
            // 
            // Panel9
            // 
            this.Panel9.BackColor = System.Drawing.Color.White;
            this.Panel9.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel9.Location = new System.Drawing.Point(453, 194);
            this.Panel9.Name = "Panel9";
            this.Panel9.Size = new System.Drawing.Size(200, 1);
            this.Panel9.TabIndex = 135;
            // 
            // Panel8
            // 
            this.Panel8.BackColor = System.Drawing.Color.White;
            this.Panel8.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel8.Location = new System.Drawing.Point(452, 157);
            this.Panel8.Name = "Panel8";
            this.Panel8.Size = new System.Drawing.Size(200, 1);
            this.Panel8.TabIndex = 134;
            // 
            // Panel7
            // 
            this.Panel7.BackColor = System.Drawing.Color.White;
            this.Panel7.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel7.Location = new System.Drawing.Point(454, 115);
            this.Panel7.Name = "Panel7";
            this.Panel7.Size = new System.Drawing.Size(200, 1);
            this.Panel7.TabIndex = 133;
            // 
            // Panel5
            // 
            this.Panel5.BackColor = System.Drawing.Color.White;
            this.Panel5.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel5.Location = new System.Drawing.Point(134, 277);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(200, 1);
            this.Panel5.TabIndex = 132;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.White;
            this.Panel4.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel4.Location = new System.Drawing.Point(134, 236);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(200, 1);
            this.Panel4.TabIndex = 131;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.White;
            this.Panel3.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel3.Location = new System.Drawing.Point(135, 197);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(200, 1);
            this.Panel3.TabIndex = 130;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.Color.White;
            this.Panel2.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel2.Location = new System.Drawing.Point(135, 156);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(200, 1);
            this.Panel2.TabIndex = 129;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel6.Location = new System.Drawing.Point(134, 115);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(200, 1);
            this.panel6.TabIndex = 128;
            // 
            // textMar
            // 
            this.textMar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textMar.Location = new System.Drawing.Point(465, 252);
            this.textMar.Name = "textMar";
            this.textMar.Size = new System.Drawing.Size(177, 20);
            this.textMar.TabIndex = 127;
            // 
            // textFournisseur
            // 
            this.textFournisseur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFournisseur.Location = new System.Drawing.Point(144, 252);
            this.textFournisseur.Name = "textFournisseur";
            this.textFournisseur.Size = new System.Drawing.Size(178, 20);
            this.textFournisseur.TabIndex = 126;
            // 
            // textRayon
            // 
            this.textRayon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRayon.Location = new System.Drawing.Point(465, 210);
            this.textRayon.Name = "textRayon";
            this.textRayon.Size = new System.Drawing.Size(177, 20);
            this.textRayon.TabIndex = 125;
            // 
            // textDep
            // 
            this.textDep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDep.Location = new System.Drawing.Point(144, 210);
            this.textDep.Name = "textDep";
            this.textDep.Size = new System.Drawing.Size(178, 20);
            this.textDep.TabIndex = 124;
            // 
            // textSFamille
            // 
            this.textSFamille.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSFamille.Location = new System.Drawing.Point(465, 169);
            this.textSFamille.Name = "textSFamille";
            this.textSFamille.Size = new System.Drawing.Size(177, 20);
            this.textSFamille.TabIndex = 123;
            // 
            // textFamille
            // 
            this.textFamille.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textFamille.Location = new System.Drawing.Point(144, 172);
            this.textFamille.Name = "textFamille";
            this.textFamille.Size = new System.Drawing.Size(178, 20);
            this.textFamille.TabIndex = 122;
            // 
            // textRef
            // 
            this.textRef.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textRef.Location = new System.Drawing.Point(465, 130);
            this.textRef.Name = "textRef";
            this.textRef.Size = new System.Drawing.Size(177, 20);
            this.textRef.TabIndex = 121;
            // 
            // textDescri
            // 
            this.textDescri.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textDescri.Location = new System.Drawing.Point(144, 130);
            this.textDescri.Name = "textDescri";
            this.textDescri.Size = new System.Drawing.Size(178, 20);
            this.textDescri.TabIndex = 120;
            // 
            // textCodCat
            // 
            this.textCodCat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCodCat.Location = new System.Drawing.Point(465, 89);
            this.textCodCat.Name = "textCodCat";
            this.textCodCat.Size = new System.Drawing.Size(177, 20);
            this.textCodCat.TabIndex = 8;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(26, 131);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(91, 16);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "Description ";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(26, 90);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(97, 16);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "Code Article ";
            // 
            // textCode
            // 
            this.textCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCode.Location = new System.Drawing.Point(144, 89);
            this.textCode.Name = "textCode";
            this.textCode.Size = new System.Drawing.Size(178, 20);
            this.textCode.TabIndex = 6;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.Black;
            this.Label4.Location = new System.Drawing.Point(244, 24);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(307, 28);
            this.Label4.TabIndex = 123;
            this.Label4.Text = "AJOUTER NOVELLE ARTICLE";
            // 
            // AddArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.Label4);
            this.Name = "AddArticle";
            this.Text = "AddArticle";
            this.Load += new System.EventHandler(this.AddArticle_Load);
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Button ButtonSupprimer;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button ButtonModifier;
        internal System.Windows.Forms.Label codeCat;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        private System.Windows.Forms.Panel Panel11;
        private System.Windows.Forms.Panel Panel10;
        private System.Windows.Forms.Panel Panel9;
        private System.Windows.Forms.Panel Panel8;
        private System.Windows.Forms.Panel Panel7;
        private System.Windows.Forms.Panel Panel5;
        private System.Windows.Forms.Panel Panel4;
        private System.Windows.Forms.Panel Panel3;
        private System.Windows.Forms.Panel Panel2;
        private System.Windows.Forms.Panel panel6;
        internal System.Windows.Forms.TextBox textMar;
        internal System.Windows.Forms.TextBox textFournisseur;
        internal System.Windows.Forms.TextBox textRayon;
        internal System.Windows.Forms.TextBox textDep;
        internal System.Windows.Forms.TextBox textSFamille;
        internal System.Windows.Forms.TextBox textFamille;
        internal System.Windows.Forms.TextBox textRef;
        internal System.Windows.Forms.TextBox textDescri;
        internal System.Windows.Forms.TextBox textCodCat;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox textCode;
        internal System.Windows.Forms.Label Label4;
    }
}