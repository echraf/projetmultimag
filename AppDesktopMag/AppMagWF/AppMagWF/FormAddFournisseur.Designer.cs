﻿namespace AppMagWF
{
    partial class FormAddFournisseur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddFournisseur));
            this.Label1 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.TextBoxNF = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.TBville = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.TBpayes = new System.Windows.Forms.TextBox();
            this.ButtonNouveau = new System.Windows.Forms.Button();
            this.ButtonSupprimer = new System.Windows.Forms.Button();
            this.ButtonModifier = new System.Windows.Forms.Button();
            this.Label3 = new System.Windows.Forms.Label();
            this.ButtonAjouter = new System.Windows.Forms.Button();
            this.TextBoxEmailF = new System.Windows.Forms.TextBox();
            this.TextBoxAdresseF = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.TextBoxTelF = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.TextBoxNomF = new System.Windows.Forms.TextBox();
            this.ButtonPrecedent = new System.Windows.Forms.Button();
            this.ButtonPremier = new System.Windows.Forms.Button();
            this.ButtonSuivant = new System.Windows.Forms.Button();
            this.ButtonDernier = new System.Windows.Forms.Button();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(142, 23);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(271, 28);
            this.Label1.TabIndex = 138;
            this.Label1.Text = "AJOUTER FOURNISSEUR";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.TextBoxNF);
            this.GroupBox1.Controls.Add(this.Label9);
            this.GroupBox1.Controls.Add(this.TBville);
            this.GroupBox1.Controls.Add(this.Label8);
            this.GroupBox1.Controls.Add(this.TBpayes);
            this.GroupBox1.Controls.Add(this.ButtonNouveau);
            this.GroupBox1.Controls.Add(this.ButtonSupprimer);
            this.GroupBox1.Controls.Add(this.ButtonModifier);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.ButtonAjouter);
            this.GroupBox1.Controls.Add(this.TextBoxEmailF);
            this.GroupBox1.Controls.Add(this.TextBoxAdresseF);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.TextBoxTelF);
            this.GroupBox1.Controls.Add(this.Label6);
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Controls.Add(this.TextBoxNomF);
            this.GroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.Location = new System.Drawing.Point(21, 72);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(465, 240);
            this.GroupBox1.TabIndex = 137;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Information Fournisseurs";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.BackColor = System.Drawing.Color.Transparent;
            this.Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(127, 33);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(109, 16);
            this.Label4.TabIndex = 82;
            this.Label4.Text = "N° Fournisseur";
            // 
            // TextBoxNF
            // 
            this.TextBoxNF.Location = new System.Drawing.Point(242, 30);
            this.TextBoxNF.Name = "TextBoxNF";
            this.TextBoxNF.Size = new System.Drawing.Size(100, 22);
            this.TextBoxNF.TabIndex = 81;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.BackColor = System.Drawing.Color.Transparent;
            this.Label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.Location = new System.Drawing.Point(18, 159);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(52, 16);
            this.Label9.TabIndex = 80;
            this.Label9.Text = "Payes";
            // 
            // TBville
            // 
            this.TBville.Location = new System.Drawing.Point(333, 118);
            this.TBville.Name = "TBville";
            this.TBville.Size = new System.Drawing.Size(122, 22);
            this.TBville.TabIndex = 79;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.BackColor = System.Drawing.Color.Transparent;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(241, 118);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(39, 16);
            this.Label8.TabIndex = 77;
            this.Label8.Text = "Ville";
            // 
            // TBpayes
            // 
            this.TBpayes.Location = new System.Drawing.Point(90, 152);
            this.TBpayes.Name = "TBpayes";
            this.TBpayes.Size = new System.Drawing.Size(118, 22);
            this.TBpayes.TabIndex = 78;
            // 
            // ButtonNouveau
            // 
            this.ButtonNouveau.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonNouveau.BackgroundImage")));
            this.ButtonNouveau.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonNouveau.Location = new System.Drawing.Point(283, 191);
            this.ButtonNouveau.Name = "ButtonNouveau";
            this.ButtonNouveau.Size = new System.Drawing.Size(42, 40);
            this.ButtonNouveau.TabIndex = 124;
            this.ButtonNouveau.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonNouveau.UseVisualStyleBackColor = true;
            // 
            // ButtonSupprimer
            // 
            this.ButtonSupprimer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonSupprimer.BackgroundImage")));
            this.ButtonSupprimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonSupprimer.Location = new System.Drawing.Point(166, 189);
            this.ButtonSupprimer.Name = "ButtonSupprimer";
            this.ButtonSupprimer.Size = new System.Drawing.Size(42, 38);
            this.ButtonSupprimer.TabIndex = 123;
            this.ButtonSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonSupprimer.UseVisualStyleBackColor = true;
            // 
            // ButtonModifier
            // 
            this.ButtonModifier.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonModifier.BackgroundImage")));
            this.ButtonModifier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonModifier.Location = new System.Drawing.Point(225, 191);
            this.ButtonModifier.Name = "ButtonModifier";
            this.ButtonModifier.Size = new System.Drawing.Size(42, 40);
            this.ButtonModifier.TabIndex = 122;
            this.ButtonModifier.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonModifier.UseVisualStyleBackColor = true;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.BackColor = System.Drawing.Color.Transparent;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(238, 82);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(87, 16);
            this.Label3.TabIndex = 67;
            this.Label3.Text = "Téléphone ";
            // 
            // ButtonAjouter
            // 
            this.ButtonAjouter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonAjouter.BackgroundImage")));
            this.ButtonAjouter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonAjouter.Location = new System.Drawing.Point(111, 189);
            this.ButtonAjouter.Name = "ButtonAjouter";
            this.ButtonAjouter.Size = new System.Drawing.Size(41, 36);
            this.ButtonAjouter.TabIndex = 121;
            this.ButtonAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonAjouter.UseVisualStyleBackColor = true;
            // 
            // TextBoxEmailF
            // 
            this.TextBoxEmailF.Location = new System.Drawing.Point(333, 156);
            this.TextBoxEmailF.Name = "TextBoxEmailF";
            this.TextBoxEmailF.Size = new System.Drawing.Size(121, 22);
            this.TextBoxEmailF.TabIndex = 76;
            // 
            // TextBoxAdresseF
            // 
            this.TextBoxAdresseF.Location = new System.Drawing.Point(90, 108);
            this.TextBoxAdresseF.Multiline = true;
            this.TextBoxAdresseF.Name = "TextBoxAdresseF";
            this.TextBoxAdresseF.Size = new System.Drawing.Size(118, 22);
            this.TextBoxAdresseF.TabIndex = 75;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(18, 79);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(44, 16);
            this.Label2.TabIndex = 66;
            this.Label2.Text = "Nom ";
            // 
            // TextBoxTelF
            // 
            this.TextBoxTelF.Location = new System.Drawing.Point(333, 79);
            this.TextBoxTelF.Name = "TextBoxTelF";
            this.TextBoxTelF.Size = new System.Drawing.Size(122, 22);
            this.TextBoxTelF.TabIndex = 73;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.BackColor = System.Drawing.Color.Transparent;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(243, 159);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(56, 16);
            this.Label6.TabIndex = 70;
            this.Label6.Text = "E-mail ";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.BackColor = System.Drawing.Color.Transparent;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(18, 117);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(70, 16);
            this.Label5.TabIndex = 69;
            this.Label5.Text = "Adresse ";
            // 
            // TextBoxNomF
            // 
            this.TextBoxNomF.Location = new System.Drawing.Point(90, 76);
            this.TextBoxNomF.Name = "TextBoxNomF";
            this.TextBoxNomF.Size = new System.Drawing.Size(118, 22);
            this.TextBoxNomF.TabIndex = 72;
            // 
            // ButtonPrecedent
            // 
            this.ButtonPrecedent.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonPrecedent.BackgroundImage")));
            this.ButtonPrecedent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonPrecedent.Location = new System.Drawing.Point(131, 318);
            this.ButtonPrecedent.Name = "ButtonPrecedent";
            this.ButtonPrecedent.Size = new System.Drawing.Size(109, 57);
            this.ButtonPrecedent.TabIndex = 136;
            this.ButtonPrecedent.Text = "Précédent";
            this.ButtonPrecedent.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonPrecedent.UseVisualStyleBackColor = true;
            // 
            // ButtonPremier
            // 
            this.ButtonPremier.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonPremier.BackgroundImage")));
            this.ButtonPremier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonPremier.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonPremier.Location = new System.Drawing.Point(20, 318);
            this.ButtonPremier.Name = "ButtonPremier";
            this.ButtonPremier.Size = new System.Drawing.Size(96, 57);
            this.ButtonPremier.TabIndex = 133;
            this.ButtonPremier.Text = "Premier";
            this.ButtonPremier.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonPremier.UseVisualStyleBackColor = true;
            // 
            // ButtonSuivant
            // 
            this.ButtonSuivant.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonSuivant.BackgroundImage")));
            this.ButtonSuivant.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonSuivant.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSuivant.Location = new System.Drawing.Point(256, 318);
            this.ButtonSuivant.Name = "ButtonSuivant";
            this.ButtonSuivant.Size = new System.Drawing.Size(98, 57);
            this.ButtonSuivant.TabIndex = 134;
            this.ButtonSuivant.Text = "Suivant";
            this.ButtonSuivant.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonSuivant.UseVisualStyleBackColor = true;
            // 
            // ButtonDernier
            // 
            this.ButtonDernier.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonDernier.BackgroundImage")));
            this.ButtonDernier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonDernier.Location = new System.Drawing.Point(374, 318);
            this.ButtonDernier.Name = "ButtonDernier";
            this.ButtonDernier.Size = new System.Drawing.Size(104, 57);
            this.ButtonDernier.TabIndex = 135;
            this.ButtonDernier.Text = "Dernier";
            this.ButtonDernier.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonDernier.UseVisualStyleBackColor = true;
            // 
            // FormAddFournisseur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 411);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.ButtonPrecedent);
            this.Controls.Add(this.ButtonPremier);
            this.Controls.Add(this.ButtonSuivant);
            this.Controls.Add(this.ButtonDernier);
            this.Name = "FormAddFournisseur";
            this.Text = "FormAddFournisseur";
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox TextBoxNF;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.TextBox TBville;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox TBpayes;
        internal System.Windows.Forms.Button ButtonNouveau;
        internal System.Windows.Forms.Button ButtonSupprimer;
        internal System.Windows.Forms.Button ButtonModifier;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Button ButtonAjouter;
        internal System.Windows.Forms.TextBox TextBoxEmailF;
        internal System.Windows.Forms.TextBox TextBoxAdresseF;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox TextBoxTelF;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.TextBox TextBoxNomF;
        internal System.Windows.Forms.Button ButtonPrecedent;
        internal System.Windows.Forms.Button ButtonPremier;
        internal System.Windows.Forms.Button ButtonSuivant;
        internal System.Windows.Forms.Button ButtonDernier;
    }
}