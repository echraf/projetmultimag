﻿namespace AppMagWF
{
    partial class UpdateArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateArticle));
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.ComboBox1 = new System.Windows.Forms.ComboBox();
            this.Button4 = new System.Windows.Forms.Button();
            this.Button3 = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.Label3 = new System.Windows.Forms.Label();
            this.ButtonModifier = new System.Windows.Forms.Button();
            this.ButtonSupprimer = new System.Windows.Forms.Button();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Button1 = new System.Windows.Forms.Button();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Panel11 = new System.Windows.Forms.Panel();
            this.Panel10 = new System.Windows.Forms.Panel();
            this.Panel9 = new System.Windows.Forms.Panel();
            this.Panel8 = new System.Windows.Forms.Panel();
            this.Panel7 = new System.Windows.Forms.Panel();
            this.Panel5 = new System.Windows.Forms.Panel();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.txtobs = new System.Windows.Forms.TextBox();
            this.txtfour = new System.Windows.Forms.TextBox();
            this.txtrayon = new System.Windows.Forms.TextBox();
            this.txtdep = new System.Windows.Forms.TextBox();
            this.txtsfamille = new System.Windows.Forms.TextBox();
            this.txtfamille = new System.Windows.Forms.TextBox();
            this.txtref = new System.Windows.Forms.TextBox();
            this.txtdescri = new System.Windows.Forms.TextBox();
            this.categorie = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.GroupBox1.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.Panel1);
            this.GroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.Location = new System.Drawing.Point(72, 57);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(657, 380);
            this.GroupBox1.TabIndex = 127;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Informations D\'article";
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.SystemColors.GrayText;
            this.Panel1.Controls.Add(this.ComboBox1);
            this.Panel1.Controls.Add(this.Button4);
            this.Panel1.Controls.Add(this.Button3);
            this.Panel1.Controls.Add(this.Button2);
            this.Panel1.Controls.Add(this.Label3);
            this.Panel1.Controls.Add(this.ButtonModifier);
            this.Panel1.Controls.Add(this.ButtonSupprimer);
            this.Panel1.Controls.Add(this.Label11);
            this.Panel1.Controls.Add(this.Label10);
            this.Panel1.Controls.Add(this.Label9);
            this.Panel1.Controls.Add(this.Button1);
            this.Panel1.Controls.Add(this.Label8);
            this.Panel1.Controls.Add(this.Label7);
            this.Panel1.Controls.Add(this.Label6);
            this.Panel1.Controls.Add(this.Label5);
            this.Panel1.Controls.Add(this.Panel11);
            this.Panel1.Controls.Add(this.Panel10);
            this.Panel1.Controls.Add(this.Panel9);
            this.Panel1.Controls.Add(this.Panel8);
            this.Panel1.Controls.Add(this.Panel7);
            this.Panel1.Controls.Add(this.Panel5);
            this.Panel1.Controls.Add(this.Panel4);
            this.Panel1.Controls.Add(this.Panel3);
            this.Panel1.Controls.Add(this.Panel2);
            this.Panel1.Controls.Add(this.txtobs);
            this.Panel1.Controls.Add(this.txtfour);
            this.Panel1.Controls.Add(this.txtrayon);
            this.Panel1.Controls.Add(this.txtdep);
            this.Panel1.Controls.Add(this.txtsfamille);
            this.Panel1.Controls.Add(this.txtfamille);
            this.Panel1.Controls.Add(this.txtref);
            this.Panel1.Controls.Add(this.txtdescri);
            this.Panel1.Controls.Add(this.categorie);
            this.Panel1.Controls.Add(this.Label2);
            this.Panel1.Controls.Add(this.Label1);
            this.Panel1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Panel1.Location = new System.Drawing.Point(6, 19);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(645, 355);
            this.Panel1.TabIndex = 123;
            // 
            // ComboBox1
            // 
            this.ComboBox1.FormattingEnabled = true;
            this.ComboBox1.Location = new System.Drawing.Point(186, 36);
            this.ComboBox1.Name = "ComboBox1";
            this.ComboBox1.Size = new System.Drawing.Size(129, 24);
            this.ComboBox1.TabIndex = 152;
            // 
            // Button4
            // 
            this.Button4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Button4.Location = new System.Drawing.Point(340, 309);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(100, 36);
            this.Button4.TabIndex = 151;
            this.Button4.Text = "Enregistrer";
            this.Button4.UseVisualStyleBackColor = true;
            // 
            // Button3
            // 
            this.Button3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Button3.Location = new System.Drawing.Point(446, 303);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(100, 36);
            this.Button3.TabIndex = 150;
            this.Button3.Text = "Enregistrer";
            this.Button3.UseVisualStyleBackColor = true;
            // 
            // Button2
            // 
            this.Button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button2.BackgroundImage")));
            this.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Button2.Location = new System.Drawing.Point(292, 297);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(42, 42);
            this.Button2.TabIndex = 149;
            this.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button2.UseVisualStyleBackColor = true;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label3.Location = new System.Drawing.Point(344, 82);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(80, 16);
            this.Label3.TabIndex = 145;
            this.Label3.Text = "Categorie ";
            // 
            // ButtonModifier
            // 
            this.ButtonModifier.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ButtonModifier.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonModifier.BackgroundImage")));
            this.ButtonModifier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonModifier.Location = new System.Drawing.Point(175, 285);
            this.ButtonModifier.Name = "ButtonModifier";
            this.ButtonModifier.Size = new System.Drawing.Size(107, 65);
            this.ButtonModifier.TabIndex = 147;
            this.ButtonModifier.Text = "Enregistrer";
            this.ButtonModifier.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonModifier.UseVisualStyleBackColor = true;
            // 
            // ButtonSupprimer
            // 
            this.ButtonSupprimer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonSupprimer.BackgroundImage")));
            this.ButtonSupprimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonSupprimer.Location = new System.Drawing.Point(115, 301);
            this.ButtonSupprimer.Name = "ButtonSupprimer";
            this.ButtonSupprimer.Size = new System.Drawing.Size(42, 38);
            this.ButtonSupprimer.TabIndex = 148;
            this.ButtonSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButtonSupprimer.UseVisualStyleBackColor = true;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label11.Location = new System.Drawing.Point(344, 244);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(39, 16);
            this.Label11.TabIndex = 144;
            this.Label11.Text = "OBS";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label10.Location = new System.Drawing.Point(32, 192);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(97, 16);
            this.Label10.TabIndex = 143;
            this.Label10.Text = "Departement";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label9.Location = new System.Drawing.Point(344, 158);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(98, 16);
            this.Label9.TabIndex = 142;
            this.Label9.Text = "Sous Famille";
            // 
            // Button1
            // 
            this.Button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Button1.BackgroundImage")));
            this.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Button1.Location = new System.Drawing.Point(55, 297);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(41, 36);
            this.Button1.TabIndex = 146;
            this.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button1.UseVisualStyleBackColor = true;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label8.Location = new System.Drawing.Point(344, 123);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(84, 16);
            this.Label8.TabIndex = 141;
            this.Label8.Text = "Referance ";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label7.Location = new System.Drawing.Point(37, 251);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(89, 16);
            this.Label7.TabIndex = 140;
            this.Label7.Text = "Fournisseur";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label6.Location = new System.Drawing.Point(344, 199);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(57, 16);
            this.Label6.TabIndex = 139;
            this.Label6.Text = "Rayon ";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label5.Location = new System.Drawing.Point(37, 135);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(59, 16);
            this.Label5.TabIndex = 138;
            this.Label5.Text = "Famille";
            // 
            // Panel11
            // 
            this.Panel11.BackColor = System.Drawing.Color.White;
            this.Panel11.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel11.Location = new System.Drawing.Point(432, 266);
            this.Panel11.Name = "Panel11";
            this.Panel11.Size = new System.Drawing.Size(200, 1);
            this.Panel11.TabIndex = 137;
            // 
            // Panel10
            // 
            this.Panel10.BackColor = System.Drawing.Color.White;
            this.Panel10.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel10.Location = new System.Drawing.Point(432, 225);
            this.Panel10.Name = "Panel10";
            this.Panel10.Size = new System.Drawing.Size(200, 1);
            this.Panel10.TabIndex = 136;
            // 
            // Panel9
            // 
            this.Panel9.BackColor = System.Drawing.Color.White;
            this.Panel9.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel9.Location = new System.Drawing.Point(434, 183);
            this.Panel9.Name = "Panel9";
            this.Panel9.Size = new System.Drawing.Size(200, 1);
            this.Panel9.TabIndex = 135;
            // 
            // Panel8
            // 
            this.Panel8.BackColor = System.Drawing.Color.White;
            this.Panel8.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel8.Location = new System.Drawing.Point(433, 146);
            this.Panel8.Name = "Panel8";
            this.Panel8.Size = new System.Drawing.Size(200, 1);
            this.Panel8.TabIndex = 134;
            // 
            // Panel7
            // 
            this.Panel7.BackColor = System.Drawing.Color.White;
            this.Panel7.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel7.Location = new System.Drawing.Point(435, 102);
            this.Panel7.Name = "Panel7";
            this.Panel7.Size = new System.Drawing.Size(200, 1);
            this.Panel7.TabIndex = 133;
            // 
            // Panel5
            // 
            this.Panel5.BackColor = System.Drawing.Color.White;
            this.Panel5.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel5.Location = new System.Drawing.Point(137, 267);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(200, 1);
            this.Panel5.TabIndex = 132;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.White;
            this.Panel4.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel4.Location = new System.Drawing.Point(137, 216);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(200, 1);
            this.Panel4.TabIndex = 131;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.White;
            this.Panel3.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel3.Location = new System.Drawing.Point(138, 150);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(200, 1);
            this.Panel3.TabIndex = 130;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.Color.White;
            this.Panel2.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.Panel2.Location = new System.Drawing.Point(137, 106);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(200, 1);
            this.Panel2.TabIndex = 129;
            // 
            // txtobs
            // 
            this.txtobs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtobs.Location = new System.Drawing.Point(446, 241);
            this.txtobs.Name = "txtobs";
            this.txtobs.Size = new System.Drawing.Size(177, 22);
            this.txtobs.TabIndex = 127;
            // 
            // txtfour
            // 
            this.txtfour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfour.Location = new System.Drawing.Point(147, 242);
            this.txtfour.Name = "txtfour";
            this.txtfour.Size = new System.Drawing.Size(178, 22);
            this.txtfour.TabIndex = 126;
            // 
            // txtrayon
            // 
            this.txtrayon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrayon.Location = new System.Drawing.Point(446, 199);
            this.txtrayon.Name = "txtrayon";
            this.txtrayon.Size = new System.Drawing.Size(177, 22);
            this.txtrayon.TabIndex = 125;
            // 
            // txtdep
            // 
            this.txtdep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdep.Location = new System.Drawing.Point(147, 190);
            this.txtdep.Name = "txtdep";
            this.txtdep.Size = new System.Drawing.Size(178, 22);
            this.txtdep.TabIndex = 124;
            // 
            // txtsfamille
            // 
            this.txtsfamille.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsfamille.Location = new System.Drawing.Point(446, 158);
            this.txtsfamille.Name = "txtsfamille";
            this.txtsfamille.Size = new System.Drawing.Size(177, 22);
            this.txtsfamille.TabIndex = 123;
            // 
            // txtfamille
            // 
            this.txtfamille.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtfamille.Location = new System.Drawing.Point(147, 125);
            this.txtfamille.Name = "txtfamille";
            this.txtfamille.Size = new System.Drawing.Size(178, 22);
            this.txtfamille.TabIndex = 122;
            // 
            // txtref
            // 
            this.txtref.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtref.Location = new System.Drawing.Point(446, 119);
            this.txtref.Name = "txtref";
            this.txtref.Size = new System.Drawing.Size(177, 22);
            this.txtref.TabIndex = 121;
            // 
            // txtdescri
            // 
            this.txtdescri.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdescri.Location = new System.Drawing.Point(146, 80);
            this.txtdescri.Name = "txtdescri";
            this.txtdescri.Size = new System.Drawing.Size(178, 22);
            this.txtdescri.TabIndex = 120;
            // 
            // categorie
            // 
            this.categorie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.categorie.Location = new System.Drawing.Point(446, 76);
            this.categorie.Name = "categorie";
            this.categorie.Size = new System.Drawing.Size(177, 22);
            this.categorie.TabIndex = 8;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label2.Location = new System.Drawing.Point(32, 82);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(91, 16);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "Description ";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Label1.Location = new System.Drawing.Point(60, 39);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(97, 16);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "Code Article ";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.Black;
            this.Label4.Location = new System.Drawing.Point(259, 13);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(213, 28);
            this.Label4.TabIndex = 126;
            this.Label4.Text = "Modifier un Article";
            // 
            // UpdateArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.Label4);
            this.Name = "UpdateArticle";
            this.Text = "UpdateArticle";
            this.GroupBox1.ResumeLayout(false);
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.ComboBox ComboBox1;
        internal System.Windows.Forms.Button Button4;
        internal System.Windows.Forms.Button Button3;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Button ButtonModifier;
        internal System.Windows.Forms.Button ButtonSupprimer;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        private System.Windows.Forms.Panel Panel11;
        private System.Windows.Forms.Panel Panel10;
        private System.Windows.Forms.Panel Panel9;
        private System.Windows.Forms.Panel Panel8;
        private System.Windows.Forms.Panel Panel7;
        private System.Windows.Forms.Panel Panel5;
        private System.Windows.Forms.Panel Panel4;
        private System.Windows.Forms.Panel Panel3;
        private System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.TextBox txtobs;
        internal System.Windows.Forms.TextBox txtfour;
        internal System.Windows.Forms.TextBox txtrayon;
        internal System.Windows.Forms.TextBox txtdep;
        internal System.Windows.Forms.TextBox txtsfamille;
        internal System.Windows.Forms.TextBox txtfamille;
        internal System.Windows.Forms.TextBox txtref;
        internal System.Windows.Forms.TextBox txtdescri;
        internal System.Windows.Forms.TextBox categorie;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label4;
    }
}