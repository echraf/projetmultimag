﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class entrees
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.ButtonSuivant = New System.Windows.Forms.Button()
        Me.gratuit = New System.Windows.Forms.CheckBox()
        Me.bc = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.menu = New System.Windows.Forms.Panel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnSon = New System.Windows.Forms.Button()
        Me.btnActualise = New System.Windows.Forms.Button()
        Me.btnhelp = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel2.SuspendLayout()
        Me.menu.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.ButtonSuivant)
        Me.Panel2.Controls.Add(Me.gratuit)
        Me.Panel2.Controls.Add(Me.bc)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(13, 72)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(222, 181)
        Me.Panel2.TabIndex = 17
        '
        'ButtonSuivant
        '
        Me.ButtonSuivant.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ButtonSuivant.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ButtonSuivant.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSuivant.Location = New System.Drawing.Point(61, 129)
        Me.ButtonSuivant.Name = "ButtonSuivant"
        Me.ButtonSuivant.Size = New System.Drawing.Size(108, 29)
        Me.ButtonSuivant.TabIndex = 163
        Me.ButtonSuivant.Text = "Suivant"
        Me.ButtonSuivant.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ButtonSuivant.UseVisualStyleBackColor = False
        '
        'gratuit
        '
        Me.gratuit.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gratuit.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.gratuit.Location = New System.Drawing.Point(84, 82)
        Me.gratuit.Name = "gratuit"
        Me.gratuit.Size = New System.Drawing.Size(124, 21)
        Me.gratuit.TabIndex = 7
        Me.gratuit.Text = "BE Gratuit"
        '
        'bc
        '
        Me.bc.Location = New System.Drawing.Point(44, 51)
        Me.bc.Name = "bc"
        Me.bc.Size = New System.Drawing.Size(131, 20)
        Me.bc.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(15, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(204, 21)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Numéro Bande Commande"
        '
        'menu
        '
        Me.menu.BackColor = System.Drawing.Color.SeaGreen
        Me.menu.Controls.Add(Me.Button4)
        Me.menu.Controls.Add(Me.Button3)
        Me.menu.Controls.Add(Me.Button1)
        Me.menu.Controls.Add(Me.btnSon)
        Me.menu.Controls.Add(Me.btnActualise)
        Me.menu.Controls.Add(Me.btnhelp)
        Me.menu.Location = New System.Drawing.Point(-3, 32)
        Me.menu.Name = "menu"
        Me.menu.Size = New System.Drawing.Size(82, 176)
        Me.menu.TabIndex = 21
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button4.Location = New System.Drawing.Point(3, 142)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(77, 31)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "Quitter"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button3.Location = New System.Drawing.Point(3, 58)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(77, 29)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "M.Sortie"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button1.Location = New System.Drawing.Point(3, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(76, 29)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Actualiser"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnSon
        '
        Me.btnSon.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSon.Location = New System.Drawing.Point(3, 31)
        Me.btnSon.Name = "btnSon"
        Me.btnSon.Size = New System.Drawing.Size(77, 28)
        Me.btnSon.TabIndex = 1
        Me.btnSon.Text = "Aide"
        Me.btnSon.UseVisualStyleBackColor = False
        '
        'btnActualise
        '
        Me.btnActualise.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnActualise.Location = New System.Drawing.Point(4, 86)
        Me.btnActualise.Name = "btnActualise"
        Me.btnActualise.Size = New System.Drawing.Size(76, 29)
        Me.btnActualise.TabIndex = 2
        Me.btnActualise.Text = "M.Tranfert"
        Me.btnActualise.UseVisualStyleBackColor = False
        '
        'btnhelp
        '
        Me.btnhelp.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnhelp.Location = New System.Drawing.Point(3, 114)
        Me.btnhelp.Name = "btnhelp"
        Me.btnhelp.Size = New System.Drawing.Size(77, 29)
        Me.btnhelp.TabIndex = 3
        Me.btnhelp.Text = "Déconnecter"
        Me.btnhelp.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.SeaGreen
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Button2)
        Me.Panel3.Location = New System.Drawing.Point(-1, -1)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(239, 34)
        Me.Panel3.TabIndex = 109
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label1.Location = New System.Drawing.Point(43, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(154, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Mouvement d'Entrie ...."
        '
        'Button2
        '
        Me.Button2.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.Bars_hamburger_list_menu_navigation_options_1285
        Me.Button2.Location = New System.Drawing.Point(2, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(36, 29)
        Me.Button2.TabIndex = 106
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.SeaGreen
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Location = New System.Drawing.Point(-1, 270)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(239, 20)
        Me.Panel4.TabIndex = 110
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(13, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Date :"
        '
        'entrees
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(238, 290)
        Me.ControlBox = False
        Me.Controls.Add(Me.menu)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Name = "entrees"
        Me.Text = "Mouvements D'entrées ..."
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.menu.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents bc As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents gratuit As System.Windows.Forms.CheckBox
    Private WithEvents menu As Panel
    Private WithEvents btnSon As Button
    Private WithEvents btnActualise As Button
    Private WithEvents btnhelp As Button
    Private WithEvents Button1 As Button
    Private WithEvents Button3 As Button
    Private WithEvents Button4 As Button
    Friend WithEvents ButtonSuivant As Button
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label1 As Label
    Private WithEvents Button2 As Button
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label3 As Label
End Class
