﻿Imports System.Data.SqlClient
Imports System.Threading.Thread
Imports System.Data.SqlServerCe
Imports System.IO
Public Class Attention

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Application.Exit()
    End Sub

    Private Sub Attention_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        nbr_fich.Text = nombre_fichiers_Non_ENV
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Select Case Interfaces
            Case 1 : Form0.Show()
            Case 2 : Form_0.Show()
            Case 3 : y = 34 : visible_import = False : hamza.Show()
        End Select
        Me.Hide()
    End Sub
    Private Sub extraire_zone_rayon(ByVal fichierr, ByRef rayyonn, ByRef zonnee)
        Dim start As Integer
        Dim pos As Integer
        Dim endd As Integer
        Dim rayon_zone As String

        rayyonn = ""
        zonnee = ""

        If InStr(fichierr, "Inv") = 0 Then Exit Sub

        start = InStr(fichierr, "#") + 1
        endd = InStrRev(fichierr, "#")
        rayon_zone = Mid(fichierr, start, endd - start)
        pos = InStr(rayon_zone, "-") - 1
        rayyonn = Mid(rayon_zone, 1, pos)
        zonnee = Mid(rayon_zone, pos + 2)

    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim fichier As String
        Dim fich As String
        Dim buffer As String
        Dim code As String
        Dim qte As String
        Dim libel As String
        Dim indice As Integer = 1
        Dim terminer As Boolean
        Dim zonne As String
        Dim rayyon As String

        ProgressBar.Maximum = nombre_fichiers_Non_ENV
        ProgressBar.Visible = True
        ProgressBar.Value = 0
        ProgressBar.Refresh()
        connexion_begin()
        '----------------------------------------ouvrire la connexion vers Article Sdf file ---------------------------------------------------------'
        Connexion_Article_locale = New SqlCeConnection("Data Source=" + path_application + "\\Articles.sdf")
        Try
            If Connexion_Article_locale.State <> ConnectionState.Open Then Connexion_Article_locale.Open()
        Catch ex As Exception
            MsgBox("prob connexion Articles sdf  file --> " & ex.Message)
        End Try
        '-----------------------------------------------------------------------------------'
        For Each fichier In dirs
            extraire_zone_rayon(fichier, rayyon, zonne)
            fich = Replace(Mid(fichier, InStrRev(fichier, "\") + 1), "_NE.txt", "")
            indice = 1
            '------------------------ lire fichier be_mobile.ini -------------------'
            Try
                Dim objReader As New System.IO.StreamReader(fichier)
                Do While objReader.Peek() <> -1
                    buffer = objReader.ReadLine()
                    If String.IsNullOrEmpty(buffer) Then Continue Do
                    code = get_code(buffer)
                    qte = get_qte(buffer)
                    libel = cherche_article2(code)

                    Dim req_insert_server_mobile As String = "insert into " & Table_lignes_pointage & " (nom_file, indice, code_article, lib_article, qte_article) values ('" & fich & "'," & indice & ",'" & code & "','" & libel & "'," & val2(CDbl(qte)) & ")"
                    Dim cmd_serveur_mobile As New SqlCommand(req_insert_server_mobile, Connexion_serveur_mobile)
                    Try
                        cmd_serveur_mobile.ExecuteNonQuery()
                        terminer = True
                    Catch ex As Exception
                        MsgBox("prob isertion dans serveur --> " & ex.Message)
                        terminer = False
                    End Try
                    indice = indice + 1
                Loop

                objReader.Close()
                If terminer = True Then
                    File.Move(fichier, path_application & "\BONS\" & fich & ".txt")
                    '--Seulmement pour ramzi--'
                    If Interfaces = 2 And (rayyon <> "") Then maj_etat_zone("pointée", Trim(zonne), Trim(rayyon))
                End If

            Catch ex As Exception
                MsgBox("Err lecture fichier NE " & ex.Message)
            End Try

            If ProgressBar.Value < ProgressBar.Maximum And terminer Then
                ProgressBar.Value = ProgressBar.Value + 1
                Sleep(200)
                ProgressBar.Refresh()
            End If

        Next fichier

        connexion_end()
        Connexion_Article_locale.Close()
        If terminer = False Then
            MsgBox("Serveur non détecté", MsgBoxStyle.Critical)
            Exit Sub
        End If

        MsgBox("Fiches de pointages envoyées avec succès", MsgBoxStyle.Information)
        Button2_Click(Nothing, Nothing)
    End Sub
    Public Function get_code(ByVal buffer As String) As String
        Dim start As Integer
        Dim endd As Integer
        On Error GoTo fin

        start = 1
        endd = InStr(start, buffer, "/") - 1
        get_code = Trim(Mid(buffer, 1, endd))

        Exit Function
fin:    MsgBox("la structure du fichier non env est erronée")
    End Function

    Public Function get_qte(ByVal buffer As String) As String
        Dim start As Integer
        Dim endd As Integer
        Dim longeur As Integer
        On Error GoTo fin

        start = InStr(1, buffer, "/") + 1
        endd = Len(buffer) + 1
        longeur = endd - start

        get_qte = Trim(Mid(buffer, start, longeur))

        Exit Function
fin:    MsgBox("la structure du fichier non env est erronée")
    End Function
    Public Function cherche_article2(ByVal code As String) As String
        Dim req As String = "select " & Nom_libelle & " from articles Where " & Nom_code & "= '" & code & "'"
        Dim cmd_serveur As New SqlCeCommand(req, Connexion_Article_locale)
        Dim rdr_serveur As SqlCeDataReader
        cherche_article2 = "Non enregistré"
        '-------------------------------------- lecture lib article ----------------------------------------'
        Try
            rdr_serveur = cmd_serveur.ExecuteReader()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            If rdr_serveur.Read() Then
                If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(0).ToString) Then
                    cherche_article2 = rdr_serveur.GetString(0).ToString
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '---------------------------------------------------------------------------------------------------'

        rdr_serveur.Close()
    End Function
End Class