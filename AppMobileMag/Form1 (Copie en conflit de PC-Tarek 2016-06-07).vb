﻿'Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlServerCe
'Imports System.Net
Imports System.IO
Public Class Form1

    Dim indice As Integer = 0
    Dim validé As Boolean = False

    Dim ds As DataSet = New DataSet()
    '------------------------------------- ouvrire fichier txt de saisie---------------------------'
    'Dim nom_file As String = prefixe & Date.Now.ToString("dd-MM-yyyy") & "_" & TimeOfDay.ToString("hh-mm-ss") & "_" & suffixe
    Dim nom_file As String = prefixe & Date.Now.ToString("yyyy-MM-dd") & "_" & TimeOfDay.ToString("hh-mm-ss") & "_" & suffixe
    Dim path_file As String = path_application & "\Bons\" & nom_file & ".txt"
    Dim indice_datagrid As Integer
    Dim code_modif As String
    Dim qte_modif As Double

    Private Sub code_article_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles code_article.GotFocus
        code_article.BackColor = Color.Aqua
        code_article.Select(0, Len(code_article.Text))
        code_article.Focus()

    End Sub

    Private Sub code_article_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles code_article.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then

            Dim lib_art As String
            Dim codesup As String
            Dim qte As Double = 1


            If code_article.Text = "" Then Exit Sub

            code_origine = ""
            code_origine = cherche_code_origine(Trim(code_article.Text))
            If code_origine = code_origine_precedent Then beeper(400)
            '------------------------------------------------------'
            If operation = "CT" And verifier_etat_article(code_article.Text) = "1" Then
                MsgBox("Article déjà controlé", MsgBoxStyle.Information)
                lib_article.Text = ""
                code_article.Focus()
                Exit Sub
            End If
            '-------------------------------------------------------'

            quantite_par_code = 1
            code_sup = ""                                'variable globale contenant le code sup de l'article en cours
            'code_origine = code_article.Text             'variable globale contenant le code org de l'article en cours

            lib_art = cherche_article(Trim(code_article.Text))

            If Not String.IsNullOrEmpty(lib_art) Then
                lib_article.Text = lib_art
            Else
                codesup = cherche_article_sup(Trim(code_article.Text), qte)
                If Not String.IsNullOrEmpty(codesup) Then
                    lib_art = cherche_article(codesup)
                    quantite_par_code = qte
                Else
                    lib_art = "Non enregistré"
                End If
            End If

            lib_article.Text = lib_art
            code_origine_precedent = code_origine
            qte_article.Focus()
            Application.DoEvents()
            If Not etoiles_existes() Then ordre_modif.Text = nb_records_sdf() + 1

        End If

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call init_application()
    End Sub
    Public Sub init_application()

        validé = False
        indice = 0
        '----------------------------------------ouvrire la connexion vers Article Sdf file ---------------------------------------------------------'

        Connexion_Article_locale = New SqlCeConnection("Data Source=" + path_application + "\\Articles.sdf")

        Try
            If Connexion_Article_locale.State <> ConnectionState.Open Then Connexion_Article_locale.Open()
        Catch ex As Exception
            MsgBox("prob connexion Articles sdf  file --> " & ex.Message)
        End Try

        '--------------------------------connexion vers sdf local----------------------------------------------------'
        If Trim(code_article.Text) <> "init" Then
            Try
                connectionUser = New SqlCeConnection("Data Source=" + path_application + "\\base_bidon.sdf")

                If connectionUser.State <> ConnectionState.Open Then connectionUser.Open()
            Catch ex As Exception
                MsgBox("prob connexion sdf local file --> " & ex.Message)
            End Try
        End If

        '-------------------------- creation de la dataset  pour les mouvements ----------------------'
        Dim sql As String = "SELECT indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice"
        Dim cmd2 As SqlCeCommand = New SqlCeCommand(sql, connectionUser)
        Dim da As New SqlCeDataAdapter(cmd2)
        ds.Clear()
        da.Fill(ds)
        DataGrid1.DataSource = ds.Tables(0)

        '-----si jamais la table est pleine alors le programme c'est planté et puis lancé de nouveau danc il faut mettra à jour le champs nom_file ---------------------------'
        Dim req As String = "update  lignes_be_mobile set nom_file = '" & nom_file & "'"
        Dim cmd As New SqlCeCommand(req, connectionUser)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("prob de update de nom file dans sdf local file --> " & ex.Message)
        End Try

        cmd.Dispose()

        '---------------------------creation style pour datagrid --------------------------------------------------'

        Dim tableStyle As New DataGridTableStyle
        tableStyle.MappingName = ds.Tables(0).TableName
        DataGrid1.TableStyles.Add(tableStyle)
        tableStyle.GridColumnStyles("indice").HeaderText = "N°"
        tableStyle.GridColumnStyles("indice").Width = 22
        tableStyle.GridColumnStyles("code_article").HeaderText = "Code Article"
        tableStyle.GridColumnStyles("code_article").Width = 40 '85
        tableStyle.GridColumnStyles("lib_article").HeaderText = "Libellé Article"
        tableStyle.GridColumnStyles("lib_article").Width = 130  '85
        tableStyle.GridColumnStyles("qte_article").HeaderText = "Qte"
        tableStyle.GridColumnStyles("qte_article").Width = 40
        DataGrid1.PreferredRowHeight = 23


        '--------------------------finalement on peut commencer-------------------------------------------------'
        DataGrid1.Refresh()

        '---tester si l'application à planté pendant la modification d'un article ----'

        If etoiles_existes() Then
            ordre_modif.Text = indice_plantage()
            DataGrid1.CurrentRowIndex = Val(ordre_modif.Text) - 1
            DataGrid1.Select(Val(ordre_modif.Text) - 1)
            indice_datagrid = Val(ordre_modif.Text)
            code_article.Text = DataGrid1(DataGrid1.CurrentRowIndex, 1)
            lib_article.Text = DataGrid1(DataGrid1.CurrentRowIndex, 2)
            qte_article.Text = DataGrid1(DataGrid1.CurrentRowIndex, 3)
            code_article.Focus()
            code_article.SelectAll()
        Else
            ordre_modif.Text = nb_records_sdf()
            code_article.Text = ""
            code_article.Focus()
        End If
        '-------------------------------------------------------'
        code_origine_precedent = ""
        'valide.BackColor = Color.Green
    End Sub
    Protected Overrides Sub Finalize()
        Try

            If Connexion_serveur_mobile.State <> Data.ConnectionState.Closed Then Connexion_serveur_mobile.Close()
            Connexion_serveur_mobile.Dispose()
            If Connexion_Article_locale.State <> Data.ConnectionState.Closed Then Connexion_Article_locale.Close()
            Connexion_Article_locale.Dispose()


            If connectionUser.State <> Data.ConnectionState.Closed Then connectionUser.Close()
            connectionUser.Dispose()

        Catch ex As Exception
            MsgBox("prob fermeture application --> " & ex.Message)
        End Try
    End Sub
    Private Sub quitter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitter.Click

        Dim rep As Integer
        DataGrid1.Refresh()
        If DataGrid1.VisibleRowCount > 0 And validé = False Then
            rep = MsgBox("Quitter sans validation du Pointage en cours (le pointage ne sera pas perdu) ", MsgBoxStyle.YesNo, "Attention")
            If rep = 7 Then

                Exit Sub
            End If
        End If

        Application.Exit()

    End Sub

    Private Sub qte_article_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles qte_article.GotFocus
        qte_article.BackColor = Color.Aqua
    End Sub

    Private Sub qte_article_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles qte_article.KeyPress


        If e.KeyChar = Convert.ToChar(13) Then
            Dim qte As String = ""
            Dim qte_par_lot As Double = 1

            If UCase(Mid(Trim(qte_article.Text), 1, 1)) = "E" Then ' vérifier c'est un code par lot '
                qte = Mid(Trim(qte_article.Text), 2)
                qte_par_lot = get_qte_par_lot(code_article.Text)
            Else
                qte = qte_article.Text
            End If
            '----------------------------------------------------------------------'

            If Not IsNumeric(Val(Trim(qte))) Then
                MsgBox("Quatité non valide")
                qte_article.Text = ""
                qte_article.Focus()
                Exit Sub
            End If

            qte_article.Text = Trim(qte)
            Application.DoEvents()

            Dim etoiles As Boolean = etoiles_existes()

            If etoiles Then '---procédure d'éxcéption------'


                If Val(val2(qte_article.Text)) = 0 Then 'tester la qte pour savoir si c'est une operation de modif ou supp 

                    '----------------------supprimer la ligne en ***** dans sdf ---------------------------------'
                    Dim req3 As String = "delete from  lignes_be_mobile where code_article = '****'"
                    Dim cmd3 As New SqlCeCommand(req3, connectionUser)

                    Try
                        cmd3.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob delete ligne contenat des ****  dans sdf local file --> " & ex.Message)
                    End Try

                    cmd3.Dispose()
                    Application.DoEvents()
                    '----------------------mise a jour des indices après supp de la ligne en ***** dans sdf ---------------------------------'
                    Dim req4 As String = "update  lignes_be_mobile set indice = indice - 1 where indice > " & indice_datagrid
                    Dim cmd4 As New SqlCeCommand(req4, connectionUser)

                    Try
                        cmd4.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob de decrimenation des indices apres supp des ****  dans sdf local file --> " & ex.Message)
                    End Try

                    cmd4.Dispose()
                    Application.DoEvents()
                Else
                    '----------------------tester si le Mr à changer l'emplacement de l'enregistrement ---------------------------------'
                    indice = nb_records_sdf()
                    If Val(ordre_modif.Text) <> indice_datagrid Then ' il ya vraiment un changement d'emplacement  '

                        If Val(ordre_modif.Text) > indice Or Val(ordre_modif.Text) <= 0 Then 'si le mr introduit un indice > au nombre des mouvements
                            MsgBox("ordre <=0 ou  > nombre de mouvements", MsgBoxStyle.Critical)
                            Exit Sub
                        End If

                        Dim req4 As String
                        If Val(ordre_modif.Text) < indice_datagrid Then
                            req4 = "update  lignes_be_mobile set indice = indice + 1 where indice between " & Val(ordre_modif.Text) & " and " & (indice_datagrid - 1)
                        Else
                            req4 = "update  lignes_be_mobile set indice = indice - 1 where indice between " & (indice_datagrid + 1) & " and " & (Val(ordre_modif.Text))
                        End If

                        Dim cmd4 As New SqlCeCommand(req4, connectionUser)

                        Try
                            cmd4.ExecuteNonQuery()
                        Catch ex As Exception
                            MsgBox("prob de M.A.J des  indices après changement d'ordre  --> " & ex.Message)
                        End Try

                        cmd4.Dispose()
                        Application.DoEvents()
                    End If
                    '----------------------modifier la ligne en ***** dans sdf ---------------------------------'
                    Dim qtes2 As Double
                    qtes2 = CDbl(qte_article.Text) * quantite_par_code * qte_par_lot
                    Dim req3 As String = "update  lignes_be_mobile set indice = '" & Val(ordre_modif.Text) & "', code_article = '" & code_origine & "', lib_article ='" & lib_article.Text & "', qte_article = " & val2(qtes2.ToString) & " where code_article = '****' ;"
                    Dim cmd3 As New SqlCeCommand(req3, connectionUser)

                    Try
                        cmd3.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob update ligne contenat des ****  dans sdf local file --> " & ex.Message)
                    End Try
                    cmd3.Dispose()
                    Application.DoEvents()

                End If

            Else '---procédure d'insertion ou normale------'
                indice = nb_records_sdf()
                If Val(ordre_modif.Text) <> (nb_records_sdf() + 1) Then  '---- procédure d'insertion ----'

                    If Val(ordre_modif.Text) > indice Or Val(ordre_modif.Text) <= 0 Then
                        MsgBox("ordre <= 0 ou  > nombre de mouvements", MsgBoxStyle.Critical)
                        Exit Sub
                    End If

                    '------------------ décallage des indices après ajout ------------------'
                    Dim req4 As String = "update  lignes_be_mobile set indice = indice + 1 where indice >= " & Val(ordre_modif.Text)
                    Dim cmd4 As New SqlCeCommand(req4, connectionUser)

                    Try
                        cmd4.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob d'incrimenation des indices apres décallage des mouvements  dans sdf local file --> " & ex.Message)
                    End Try

                    cmd4.Dispose()
                    Application.DoEvents()
                    '----------------- insertion avec l'indice choisi ---------------'
                    Dim qtes1 As Double
                    qtes1 = CDbl(qte_article.Text) * quantite_par_code * qte_par_lot
                    Dim req As String = "insert into lignes_be_mobile (nom_file, indice, code_article, lib_article, qte_article,code_sup) values ('" & nom_file & "'," & Val(ordre_modif.Text) & ",'" & code_origine & "','" & lib_article.Text & "'," & val2(qtes1.ToString) & ",'" & code_sup & "')"
                    Dim cmd As New SqlCeCommand(req, connectionUser)

                    Try
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob insertion spéciale dans sdf file --> " & ex.Message)
                    End Try

                    cmd.Dispose()
                    Application.DoEvents()
                Else 'procédure  normale 100%

                    '---------- vérifier si la quantité est valide -----------------------------------------------'
                    Dim adapt_qte = Val(val2(qte_article.Text))
                    If adapt_qte <= 0 Or Not IsNumeric(adapt_qte) Then
                        MsgBox("Quantité non valide")
                        Exit Sub
                    End If
                    '----------------------ajouter nouvelle ligne dans sdf ---------------------------------'
                    Dim qtes As Double
                    indice = nb_records_sdf() + 1
                    qtes = CDbl(qte_article.Text) * quantite_par_code * qte_par_lot
                    Dim req As String = "insert into lignes_be_mobile (nom_file, indice, code_article, lib_article, qte_article,code_sup) values ('" & nom_file & "'," & indice & ",'" & code_origine & "','" & lib_article.Text & "'," & val2(qtes.ToString) & ",'" & code_sup & "')"
                    Dim cmd As New SqlCeCommand(req, connectionUser)

                    Try
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob insertion sdf file --> " & ex.Message)
                    End Try

                    cmd.Dispose()
                    Application.DoEvents()
                End If
            End If

            '----------------------- refrech datagrid --------------------------'
            Dim cmd2 As SqlCeCommand = New SqlCeCommand("SELECT indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice", connectionUser)
            Dim da As New SqlCeDataAdapter(cmd2)
            ds.Clear()
            da.Fill(ds)
            DataGrid1.Refresh()
            cmd2.Dispose()
            Application.DoEvents()
            '------------------------- init interface --------------------------'
            code_article.Text = ""
            lib_article.Text = ""
            qte_article.Text = ""
            code_origine = ""
            code_article.Focus()
            ordre_modif.Text = nb_records_sdf()
            '--------------------------------------------------------------------'*
            If Val(ordre_modif.Text) = 0 Then Exit Sub
			DataGrid1.Select(Val(ordre_modif.Text) - 1)
            DataGrid1.CurrentRowIndex = Val(ordre_modif.Text) - 1
            Application.DoEvents()
        End If
        Application.DoEvents()
    End Sub

    Private Sub qte_article_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles qte_article.LostFocus
        qte_article.BackColor = Color.White
    End Sub

    Private Sub code_article_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles code_article.LostFocus
        code_article.BackColor = Color.White
    End Sub

    Private Sub import_bases_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles import_bases.Click   'ce bouton joue le role d'imporation de la base
        Dim Value As Integer = 0
        connexion_begin()
        Dim nbr_articles As Integer
        nbr_articles = nb_records_article_serveur()
        '------------ bare de progression --------------------'
        Value = nbr_articles + nb_records_codesup()

        If nbr_articles <= 0 Then
            MsgBox("table Article serveur est vide !!!")
            Exit Sub
        End If

        ProgressBar.Maximum = Value
        ProgressBar.Value = 0
        ProgressBar.Visible = True

        '------------------------------------------------------'

        Dim req_read As String = "select CODART, DESART, etat, art1, qt1, qt_lot from " & Nom_table & "," & Nom_table_opt & " where CODART = CODE "
        Dim cmd_read_articles As SqlCommand = New SqlCommand(req_read, Connexion_serveur_mobile)
        Dim record_reader As SqlDataReader

        Dim etape1 As Boolean = False
        Dim etape2 As Boolean
        Dim progress As Integer = 0

        '----------- suppression de l'ancienne liste articles -------------'
        Dim req_delete_artucle_local As String = "delete from articles"
        Dim cmd_delete_article As New SqlCeCommand(req_delete_artucle_local, Connexion_Article_locale)

        Try
            cmd_delete_article.ExecuteNonQuery()
            req_delete_artucle_local = Nothing
            cmd_delete_article.Dispose()
        Catch ex As Exception
            MsgBox("problème suppression l'ancienne liste articles local --> " & ex.Message)
        End Try

        '----------- lecture liste des articles dans le serveur -------------'
        Try
            record_reader = cmd_read_articles.ExecuteReader()
            etape1 = True
        Catch ex As Exception
            MsgBox("problème lecture liste des articles" & ex.Message)
        End Try

        '----------- ecriture liste des articles dans base locale -------------'
        Dim cmd_write_articles As SqlCeCommand

        Do While record_reader.Read()
            etape2 = False
            Dim req_write As String = "insert into Articles (CODART, DESART,etat,art1,qt1,qt_lot) values ('" & record_reader.GetString(0) & "','" & record_reader.GetString(1) & "','" & record_reader.GetString(2) & "','" & record_reader.GetString(3) & "'," & val2(record_reader.GetSqlDouble(4).ToString) & "," & val2(record_reader.GetSqlDouble(5).ToString) & ")"
            cmd_write_articles = New SqlCeCommand(req_write, Connexion_Article_locale)
            Try
                cmd_write_articles.ExecuteNonQuery()
                progress = progress + 1
                ProgressBar.Value = progress
                etape2 = True
            Catch ex As Exception
                MsgBox("problème insertion articles dans base article locale" & ex.Message)
                MsgBox(req_write)
            End Try

        Loop

        record_reader.Close()

        '##################################### importation code_sup #############################################
        Dim req_read_sup As String = "select CODE, code_2, typ, qt  from " & nom_table_sup
        Dim cmd_read_sup As SqlCommand = New SqlCommand(req_read_sup, Connexion_serveur_mobile)
        Dim read_sup As SqlDataReader

        Dim etape3 As Boolean = False
        Dim etape4 As Boolean

        '----------- suppression de l'ancienne code_sup -------------'
        Dim req_delete_sup_local As String = "delete from code_sup"
        Dim cmd_delete_sup As New SqlCeCommand(req_delete_sup_local, Connexion_Article_locale)

        Try
            cmd_delete_sup.ExecuteNonQuery()
            req_delete_sup_local = Nothing
            cmd_delete_sup.Dispose()
        Catch ex As Exception
            MsgBox("problème suppression de l'ancienne code_sup --> " & ex.Message)
        End Try

        '----------- lecture liste code_sup depuis le serveur -------------'
        Try
            read_sup = cmd_read_sup.ExecuteReader()
            etape3 = True
        Catch ex As Exception
            MsgBox("problème lecture code_sup depuis le serveur" & ex.Message)
        End Try

        '----------- ecriture liste des code sup dans base locale -------------'
        Dim cmd_write_sup As SqlCeCommand

        Do While read_sup.Read()
            etape4 = False
            Dim req_write_sup As String = "insert into code_sup (CODE, code_2, typ, qt) values ('" & read_sup.GetString(0) & "','" & read_sup.GetString(1) & "','" & read_sup.GetString(2) & "'," & read_sup.GetSqlValue(3).ToString & ")"
            cmd_write_sup = New SqlCeCommand(req_write_sup, Connexion_Article_locale)
            Try
                cmd_write_sup.ExecuteNonQuery()
                progress = progress + 1
                ProgressBar.Value = progress
                etape4 = True
            Catch ex As Exception
                MsgBox("problème insertion articles dans base article locale" & ex.Message)
            End Try

        Loop

        read_sup.Close()


        connexion_end()
        '##########################################################################################
        ProgressBar.Visible = False
        If etape1 And etape2 And etape3 And etape4 Then
            MsgBox("importation [" & nbr_articles & "] Articles", MsgBoxStyle.Information)
            code_article.Focus()
        End If


    End Sub


    Private Sub valide_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles valide.Click

        connexion_begin()

        If nb_records_sdf() = 0 Then
            MsgBox("impossible: la table de mouvments est vide", MsgBoxStyle.Critical)
            code_article.Focus()
            Exit Sub
        End If

        If etoiles_existes() Then
            MsgBox("il y a déjà une ligne en cours de modification!!!", MsgBoxStyle.Critical)
            Exit Sub
        End If

        '------------------------------------------------------------------------------------------'
        Dim req_read_sdf As String = "SELECT nom_file, indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice"
        Dim cmd_read_sdf As New SqlCeCommand(req_read_sdf, connectionUser)

        Dim cmd_begin_tr As New SqlCommand("begin transaction;", Connexion_serveur_mobile)
        Dim cmd_commit_tr As New SqlCommand("commit transaction;", Connexion_serveur_mobile)
        Dim cmd_roll_tr As New SqlCommand("rollback transaction;", Connexion_serveur_mobile)

        Dim sdf_data_reader As SqlCeDataReader
        Dim etape1 As Boolean = True
        Dim etape2 As Boolean
        Dim saisie_file_obj As New System.IO.StreamWriter(path_file)
        '---------- debut transaction ------------------'
        Try
            cmd_begin_tr.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '--------------------------------------------------'
        sdf_data_reader = cmd_read_sdf.ExecuteReader()

        Do While sdf_data_reader.Read()

            Dim req_insert_server_mobile As String = "insert into " & Table_lignes_pointage & " (nom_file, indice, code_article, lib_article, qte_article) values ('" & sdf_data_reader.GetString(0).ToString & "'," & Val(sdf_data_reader.GetInt32(1)) & ",'" & sdf_data_reader.GetString(2).ToString & "','" & sdf_data_reader.GetString(3).ToString & "'," & val2(sdf_data_reader.GetSqlDouble(4).ToString) & ")"
            Dim cmd_serveur_mobile As New SqlCommand(req_insert_server_mobile, Connexion_serveur_mobile)

            '---------Ajouter lignes dans sql server mobile ----------------------'
            Try
                If etape1 = True Then cmd_serveur_mobile.ExecuteNonQuery()
            Catch ex As Exception
                etape1 = False
                MsgBox("Le poinatge sera enregister localement pour une future importation")
            End Try
            '------------Ajouter lignes dans file txt local ----------------------'
            Try
                saisie_file_obj.Write(sdf_data_reader.GetString(2).ToString & "/" & val2(sdf_data_reader.GetSqlDouble(4).ToString) & vbNewLine)
                etape2 = True
            Catch ex As Exception
                MsgBox("Erreur d'enregistrement dans le fichier txt local " & ex.Message)
                etape2 = False
            End Try

            req_insert_server_mobile = Nothing
            cmd_serveur_mobile.Dispose()
        Loop

        sdf_data_reader.Close()
        '----------------------si opéartion insersion dans serveur est totalement complete then commit else rollback -------------------------------------'

        If etape1 Then
            '-- la ligne suivante est Seulmement pour ramzi--'
            If Interfaces = 2 And (operation = "invmag" Or operation = "invdepot") Then maj_etat_zone("pointée", Trim(zone_var), Trim(rayon_var))
            '---------- commit transaction ------------------'
            Try
                cmd_commit_tr.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            '--------------------------------------------------'
        Else
            '---------- rollback transaction ------------------'
            Try
                cmd_roll_tr.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            '--------------------------------------------------'
        End If

        '------------------------------------- terminer application -----------------------------'
        If etape2 Then
            Dim path2 As String
            path2 = Mid(path_file, 1, Len(path_file) - 4) & "_NE.txt"
            validé = True
            saisie_file_obj.Close()
            Try
                If etape1 = False Then File.Move(path_file, path2)
                MsgBox("Oppération terminée avec succé", MsgBoxStyle.Information)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        End If
        '--------------------------- Nettoyer interface et base locale ----------------------------'
        connexion_end()
        Call Button1_Click(Nothing, Nothing)
        Call quitter_Click(Nothing, Nothing)
    End Sub

    Private Sub DataGrid1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGrid1.DoubleClick

        '------------------------ Afficher les stat de pointage pour l'article sélectionné ---------------------------'
        If DataGrid1.CurrentCell.ColumnNumber = 3 Then
            affiche_frequence_pointage(DataGrid1(DataGrid1.CurrentRowIndex, 1))
            Exit Sub
        End If
        '-------------------- tester si un autre produit est en cours de modif -----------------------'
        If etoiles_existes() Then
            MsgBox("un autre produit est en cours de modification, terminer la transaction précédente")
            Exit Sub
        End If
        '----------------------------------------------------------------------------------------------'
        indice_datagrid = DataGrid1(DataGrid1.CurrentRowIndex, 0)
        code_modif = cherche_codesup(indice_datagrid)
        If code_modif = "" Then code_modif = DataGrid1(DataGrid1.CurrentRowIndex, 1)
        quantite_par_code = cherche_qte_par_code(code_modif)
        qte_modif = DataGrid1(DataGrid1.CurrentRowIndex, 3) / quantite_par_code
        code_article.Text = code_modif
        qte_article.Text = qte_modif
        lib_article.Text = DataGrid1(DataGrid1.CurrentRowIndex, 2)
        ordre_modif.Text = indice_datagrid
        code_origine = cherche_code_origine(Trim(code_article.Text))
        '---------------- replacer le code et le lib par des ****  dans le sdf -----------------------'

        Dim req3 As String = "update  lignes_be_mobile set code_article = '****' where indice =  " & indice_datagrid
        Dim cmd3 As New SqlCeCommand(req3, connectionUser)

        Try
            cmd3.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("prob modif code par des ****  dans sdf local file --> " & ex.Message)
        End Try

        cmd3.Dispose()
        req3 = Nothing

        '----------------------- refrech de la datagrid --------------------------'
        Dim cmd2 As SqlCeCommand = New SqlCeCommand("SELECT indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice", connectionUser)
        Dim da As New SqlCeDataAdapter(cmd2)
        ds.Clear()
        da.Fill(ds)
        DataGrid1.Refresh()
        DataGrid1.Select(indice_datagrid - 1)
        DataGrid1.CurrentRowIndex = indice_datagrid - 1
        '--------------------------------------------------------------------------'
        cmd2.Dispose()
        code_article.Focus()
        code_article.SelectAll()
    End Sub

    

    Function etoiles_existes() As Boolean
        Dim req As String = "select * from lignes_be_mobile where code_article =  '****' "
        Dim cmd As New SqlCeCommand(req, connectionUser)
        Dim etoiles_reader As SqlCeDataReader
        etoiles_existes = False

        Try
            etoiles_reader = cmd.ExecuteReader()
            If etoiles_reader.Read Then etoiles_existes = True
            etoiles_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob seection nombre des ****  dans sdf local file --> " & ex.Message)
        End Try


    End Function
    



    Private Sub ordre_modif_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ordre_modif.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then
            qte_article.Focus()
            qte_article.SelectAll()
        End If
    End Sub

    Function indice_plantage() As Integer
        Dim req As String = "select indice from lignes_be_mobile where code_article = '****' "
        Dim cmd As New SqlCeCommand(req, connectionUser)
        Dim nbr_reader As SqlCeDataReader
        indice_plantage = 0

        Try
            nbr_reader = cmd.ExecuteReader()

            If nbr_reader.Read Then indice_plantage = nbr_reader.GetSqlInt32(0)
            nbr_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob de recherche d'indice plantage --> " & ex.Message)
        End Try

    End Function

    Function nb_records_article_serveur() As Integer
        Dim req As String = "select count(CODART) from " & Nom_table
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim nbr_reader As SqlDataReader
        nb_records_article_serveur = 0

        Try
            nbr_reader = cmd.ExecuteReader()

            If nbr_reader.Read Then nb_records_article_serveur = nbr_reader.GetSqlInt32(0)
            nbr_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob de comptage des lignes du la table article --> " & ex.Message)
        End Try

    End Function

    Function nb_records_codesup() As Integer
        Dim req As String = "select count(CODE) from " & nom_table_sup
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim nbr_reader As SqlDataReader
        nb_records_codesup = 0

        Try
            nbr_reader = cmd.ExecuteReader()

            If nbr_reader.Read Then nb_records_codesup = nbr_reader.GetSqlInt32(0)
            nbr_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob de comptage des lignes du la table code_supp --> " & ex.Message)
        End Try

    End Function

    'Public Function cherche_article(ByVal code As String) As String

    '    Dim req As String = "select " & Nom_libelle & " from articles Where " & Nom_code & "= '" & code & "'"
    '    Dim cmd_serveur As New SqlCeCommand(req, Connexion_Article_locale)
    '    Dim rdr_serveur As SqlCeDataReader
    '    cherche_article = ""
    '    '-------------------------------------- lecture lib article ----------------------------------------'
    '    Try
    '        rdr_serveur = cmd_serveur.ExecuteReader()
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    '    Try
    '        If rdr_serveur.Read() Then
    '            If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(0).ToString) Then
    '                cherche_article = rdr_serveur.GetString(0).ToString
    '                code_origine = code
    '            End If

    '        End If

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    '    '---------------------------------------------------------------------------------------------------'

    '    rdr_serveur.Close()
    '    rdr_serveur.Dispose()


    'End Function

    'Public Function cherche_article_sup(ByVal code As String, ByRef qte As Double) As String

    '    Dim req As String = "select  CODE,qt from code_sup Where code_2 = '" & code & "'"
    '    Dim cmd_serveur As New SqlCeCommand(req, Connexion_Article_locale)
    '    Dim rdr_serveur As SqlCeDataReader
    '    cherche_article_sup = ""
    '    '-------------------------------------- lecture lib article ----------------------------------------'
    '    Try
    '        rdr_serveur = cmd_serveur.ExecuteReader()
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    '    Try
    '        If rdr_serveur.Read() Then
    '            If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(0).ToString) Then
    '                cherche_article_sup = rdr_serveur.GetString(0).ToString
    '                code_sup = code
    '                If Val(CDbl(rdr_serveur.GetFloat(1))) <> 0 Then qte = CDbl(rdr_serveur.GetFloat(1))

    '            End If

    '        End If

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    '    '---------------------------------------------------------------------------------------------------'

    '    rdr_serveur.Close()
    '    rdr_serveur.Dispose()


    'End Function

    'Public Function cherche_qte_par_code(ByVal code As String) As Double
    '    Dim qte As Double

    '    If String.IsNullOrEmpty(cherche_article_sup(code, qte)) Then
    '        cherche_qte_par_code = 1
    '    Else
    '        cherche_qte_par_code = qte
    '    End If

    'End Function

    Private Sub affiche_frequence_pointage(ByVal code As String)
        Dim req As String = "select  indice,qte_article,code_sup from lignes_be_mobile Where code_article = '" & code & "'"
        Dim cmd_serveur As New SqlCeCommand(req, connectionUser)
        Dim rdr_serveur As SqlCeDataReader
        Dim message As String = "Lignes [" & code & "]" & vbNewLine
        Dim somme As String = 0
        '-------------------------------------- lecture lib article ----------------------------------------'
        Try
            rdr_serveur = cmd_serveur.ExecuteReader()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '----------------------------------------------------------------------------------------------------'
        Try
            Do While rdr_serveur.Read()
                If Val(rdr_serveur.GetInt32(0)) <> 0 Then
                    If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(2)) Then
                        message = message & Val(rdr_serveur.GetInt32(0)) & ": --> " & rdr_serveur.GetSqlDouble(1).ToString & " (Code+)" & vbNewLine
                    Else
                        message = message & Val(rdr_serveur.GetInt32(0)) & ": --> " & rdr_serveur.GetSqlDouble(1).ToString & vbNewLine
                    End If
                    somme = somme + Val(val2(rdr_serveur.GetSqlDouble(1)))

                End If
            Loop

            message = message & "Total pointé: " & somme
            MsgBox(message)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '---------------------------------------------------------------------------------------------------'

        rdr_serveur.Close()
        rdr_serveur.Dispose()

    End Sub

    Function cherche_codesup(ByVal indice As Integer) As String
        Dim req As String = "select code_sup from lignes_be_mobile where indice =" & indice
        Dim cmd As New SqlCeCommand(req, connectionUser)
        Dim nbr_reader As SqlCeDataReader
        cherche_codesup = ""

        Try
            nbr_reader = cmd.ExecuteReader()

            If nbr_reader.Read Then cherche_codesup = nbr_reader.GetSqlString(0)
            nbr_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob de recherche code_supp dans table bidon --> " & ex.Message)
        End Try

    End Function

    Private Sub send_saisie_file(ByVal file As String, ByVal ip_serv As String)

        '------------- connexion vers le serveur ---------------------------------------'

        'Dim MonTCPClient As TcpClient
        'Dim MonFlux As NetworkStream
        'Dim MonWriter As StreamWriter

        'Try
        '    MonTCPClient = New TcpClient(ip_serv, 5050)
        '    MonFlux = MonTCPClient.GetStream()
        '    MonWriter = New StreamWriter(MonFlux)
        'Catch ex As Exception
        '    MsgBox("Problème de connexion avec le serveur : " & ex.Message)
        'End Try

        ''-------------- debut d'envoi du fichier ----------------------------------------'
        'Try
        '    Dim objReader As New System.IO.StreamReader(path_file)
        '    Dim Buffer As String
        '    Dim MesBytes As Byte()

        '    '------- envoie du non du fichier ver le serveur ----------'
        '    MonWriter.WriteLine(file)
        '    MonWriter.Flush()
        '    '----------envoie du corps du fichier ----------------------'

        '    Do While objReader.Peek() <> -1
        '        Buffer = objReader.ReadLine()
        '        If String.IsNullOrEmpty(Buffer) Then Continue Do
        '        MonWriter.WriteLine(Buffer)
        '        MonWriter.Flush()
        '    Loop

        '    objReader.Close()
        '    '------- envoie du signe de terminaison ----------'
        '    MonWriter.WriteLine("fin transmission")
        '    MonWriter.Flush()
        '    '-----------------------------------------------------------'


        '    MonWriter.Close()
        '    MonWriter.Dispose()
        '    MonFlux.Close()
        '    MonFlux.Dispose()
        '    MonTCPClient.Close()
        '    Buffer = Nothing
        '    MesBytes = Nothing
        '    MsgBox("Le fichier est transmit vers le serveur", MsgBoxStyle.Information)

        'Catch ex As Exception
        '    MsgBox("Problème d'envoi du fichier : " & ex.Message)
        'End Try
        ''-------------- fin d'envoi du fichier ----------------------------------------'

    End Sub

    Private Sub send_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        '----------- les trois lignes suivantes pour la transmision vers un serveur d'écoute ----------'
        valide_Click("send_file", Nothing) 'valider le travail sans quitter 
        send_saisie_file(nom_file, ip_sauv) 'envoyer fichier vers serveur
        Call quitter_Click(Nothing, Nothing) 'terminer application
        '---------------------------------- Fin trois lignes -------------------------------------------'
    End Sub

    Private Function verifier_etat_article(ByVal code_article) As String
        Dim etat_article As String = "0"
        Dim req As String = "select etat from articles where CODART = '" & code_article & "'"
        Dim cmd As New SqlCeCommand(req, Connexion_Article_locale)
        Dim test_article_reader As SqlCeDataReader

        Try
            test_article_reader = cmd.ExecuteReader()

            If test_article_reader.Read Then

                If Not String.IsNullOrEmpty(test_article_reader.GetString(0).ToString) Then etat_article = test_article_reader.GetString(0).ToString

            Else '---- c'est un code supp -------------'

                Dim req2 As String = "select etat from articles where CODART = '" & code_origine & "'"
                Dim cmd2 As New SqlCeCommand(req2, Connexion_Article_locale)
                Dim test_article_reader2 As SqlCeDataReader

                test_article_reader2 = cmd2.ExecuteReader()

                If test_article_reader2.Read Then
                    If Not String.IsNullOrEmpty(test_article_reader2.GetString(0).ToString) Then etat_article = test_article_reader2.GetString(0).ToString
                End If

                cmd2.Dispose()
            End If


            test_article_reader.Close()
            cmd.Dispose()


            Return etat_article

        Catch ex As Exception
            MsgBox("prob verif etat article --> " & ex.Message)
        End Try

    End Function

    Private Function get_qte_par_lot(ByVal code_article) As Double

        Dim req As String = "select qt_lot from Articles where CODART = '" & code_article & "'"
        Dim cmd As New SqlCeCommand(req, Connexion_Article_locale)
        Dim test_article_reader As SqlCeDataReader
        get_qte_par_lot = 1

        Try
            test_article_reader = cmd.ExecuteReader()

            If test_article_reader.Read Then
                If Val(CDbl(test_article_reader.GetValue(0))) <> 0 Then get_qte_par_lot = CDbl(test_article_reader.GetValue(0))
            Else '---- c'est un code supp -------------'

                Dim req2 As String = "select qt_lot from Articles where CODART = '" & code_origine & "'"
                Dim cmd2 As New SqlCeCommand(req2, Connexion_Article_locale)
                Dim test_article_reader2 As SqlCeDataReader

                test_article_reader2 = cmd2.ExecuteReader()

                If test_article_reader2.Read Then
                    If Val(CDbl(test_article_reader2.GetValue(0))) <> 0 Then get_qte_par_lot = CDbl(test_article_reader2.GetValue(0))
                End If

                cmd2.Dispose()
            End If


                test_article_reader.Close()
                cmd.Dispose()

        Catch ex As Exception
            MsgBox("prob select qt1 par lot --> " & ex.Message)
        End Try
    End Function

    Public Function cherche_code_origine(ByVal code As String) As String

        Dim req As String = "select  CODE from code_sup Where code_2 = '" & code & "'"
        Dim cmd_serveur As New SqlCeCommand(req, Connexion_Article_locale)
        Dim rdr_serveur As SqlCeDataReader
        cherche_code_origine = code
        '-------------------------------------- lecture lib article ----------------------------------------'
        Try
            rdr_serveur = cmd_serveur.ExecuteReader()
        
            If rdr_serveur.Read() Then
                If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(0).ToString) Then
                    cherche_code_origine = rdr_serveur.GetString(0).ToString
                End If

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '---------------------------------------------------------------------------------------------------'

        rdr_serveur.Close()
        rdr_serveur.Dispose()


    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim rep As Integer

        '---------- tester si le bouton été cliquer ou pas -------------------------------------'
        Try
            If sender <> Nothing Then
                rep = MsgBox("êtes vous sûr de vouloir supprimer le pointage en cours ", MsgBoxStyle.YesNo, "Attention")
                If rep = 7 Then
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            rep = MsgBox("êtes vous sûr de vouloir supprimer le pointage en cours ", MsgBoxStyle.YesNo, "Attention")
            If rep = 7 Then
                Exit Sub
            End If
        End Try
        '----------------------------- supprimer la base sdf locale ------------------------------'
        Dim req As String = "delete from lignes_be_mobile"
        Dim cmd As New SqlCeCommand(req, connectionUser)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("prob delete from sdf file --> " & ex.Message)
        End Try

        cmd.Dispose()

        '----------------------- refrech datagrid --------------------------'
        Dim cmd2 As SqlCeCommand = New SqlCeCommand("SELECT indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice", connectionUser)
        Dim da As New SqlCeDataAdapter(cmd2)
        ds.Clear()
        da.Fill(ds)
        DataGrid1.Refresh()
        cmd2.Dispose()
        '-----------------------------------------------------------------------------------------'
        ordre_modif.Text = nb_records_sdf()
        code_article.Text = ""
        qte_article.Text = ""
        lib_article.Text = ""
        indice = 0
        code_article.Focus()
    End Sub

    
End Class
