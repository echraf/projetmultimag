﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class Transferts
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Mg2 = New System.Windows.Forms.ComboBox()
        Me.Mg1 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnmenu = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.menu = New System.Windows.Forms.Panel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnSon = New System.Windows.Forms.Button()
        Me.btnActualise = New System.Windows.Forms.Button()
        Me.btnhelp = New System.Windows.Forms.Button()
        Me.ButtonSuivant = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.menu.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.ButtonSuivant)
        Me.Panel1.Controls.Add(Me.Mg2)
        Me.Panel1.Controls.Add(Me.Mg1)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(14, 81)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(214, 151)
        Me.Panel1.TabIndex = 15
        '
        'Mg2
        '
        Me.Mg2.Location = New System.Drawing.Point(86, 71)
        Me.Mg2.Name = "Mg2"
        Me.Mg2.Size = New System.Drawing.Size(122, 21)
        Me.Mg2.TabIndex = 24
        '
        'Mg1
        '
        Me.Mg1.Location = New System.Drawing.Point(86, 29)
        Me.Mg1.Name = "Mg1"
        Me.Mg1.Size = New System.Drawing.Size(122, 21)
        Me.Mg1.TabIndex = 23
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(2, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(105, 21)
        Me.Label3.TabIndex = 104
        Me.Label3.Text = "Mg Destina :"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(5, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(102, 21)
        Me.Label2.TabIndex = 105
        Me.Label2.Text = "Mg Source :"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.SeaGreen
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.btnmenu)
        Me.Panel3.Location = New System.Drawing.Point(1, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(237, 33)
        Me.Panel3.TabIndex = 108
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label1.Location = New System.Drawing.Point(39, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(180, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Mouvement de Transfert ...."
        '
        'btnmenu
        '
        Me.btnmenu.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.Bars_hamburger_list_menu_navigation_options_1284
        Me.btnmenu.Location = New System.Drawing.Point(1, 2)
        Me.btnmenu.Name = "btnmenu"
        Me.btnmenu.Size = New System.Drawing.Size(36, 30)
        Me.btnmenu.TabIndex = 106
        Me.btnmenu.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.SeaGreen
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Location = New System.Drawing.Point(-1, 270)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(239, 20)
        Me.Panel4.TabIndex = 112
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(13, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Date :"
        '
        'menu
        '
        Me.menu.BackColor = System.Drawing.Color.SeaGreen
        Me.menu.Controls.Add(Me.Button4)
        Me.menu.Controls.Add(Me.Button3)
        Me.menu.Controls.Add(Me.Button1)
        Me.menu.Controls.Add(Me.btnSon)
        Me.menu.Controls.Add(Me.btnActualise)
        Me.menu.Controls.Add(Me.btnhelp)
        Me.menu.ForeColor = System.Drawing.Color.Black
        Me.menu.Location = New System.Drawing.Point(1, 33)
        Me.menu.Name = "menu"
        Me.menu.Size = New System.Drawing.Size(82, 176)
        Me.menu.TabIndex = 22
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button4.Location = New System.Drawing.Point(3, 142)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(77, 31)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "Quitter"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button3.Location = New System.Drawing.Point(3, 58)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(77, 29)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "M.Entrée"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button1.Location = New System.Drawing.Point(3, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(76, 29)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Actualiser"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnSon
        '
        Me.btnSon.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSon.Location = New System.Drawing.Point(3, 31)
        Me.btnSon.Name = "btnSon"
        Me.btnSon.Size = New System.Drawing.Size(77, 28)
        Me.btnSon.TabIndex = 1
        Me.btnSon.Text = "Aide"
        Me.btnSon.UseVisualStyleBackColor = False
        '
        'btnActualise
        '
        Me.btnActualise.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnActualise.Location = New System.Drawing.Point(4, 86)
        Me.btnActualise.Name = "btnActualise"
        Me.btnActualise.Size = New System.Drawing.Size(76, 29)
        Me.btnActualise.TabIndex = 2
        Me.btnActualise.Text = "M.Sortie"
        Me.btnActualise.UseVisualStyleBackColor = False
        '
        'btnhelp
        '
        Me.btnhelp.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnhelp.Location = New System.Drawing.Point(3, 114)
        Me.btnhelp.Name = "btnhelp"
        Me.btnhelp.Size = New System.Drawing.Size(77, 29)
        Me.btnhelp.TabIndex = 3
        Me.btnhelp.Text = "Déconnecter"
        Me.btnhelp.UseVisualStyleBackColor = False
        '
        'ButtonSuivant
        '
        Me.ButtonSuivant.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ButtonSuivant.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ButtonSuivant.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSuivant.ForeColor = System.Drawing.Color.Black
        Me.ButtonSuivant.Location = New System.Drawing.Point(53, 111)
        Me.ButtonSuivant.Name = "ButtonSuivant"
        Me.ButtonSuivant.Size = New System.Drawing.Size(108, 29)
        Me.ButtonSuivant.TabIndex = 164
        Me.ButtonSuivant.Text = "Suivant"
        Me.ButtonSuivant.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ButtonSuivant.UseVisualStyleBackColor = False
        '
        'Transferts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(238, 290)
        Me.ControlBox = False
        Me.Controls.Add(Me.menu)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.ForeColor = System.Drawing.Color.White
        Me.Name = "Transferts"
        Me.Text = "Mouvements De Transferts"
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.menu.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Mg2 As System.Windows.Forms.ComboBox
    Friend WithEvents Mg1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Private WithEvents btnmenu As Button
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label4 As Label
    Private WithEvents menu As Panel
    Private WithEvents Button4 As Button
    Private WithEvents Button3 As Button
    Private WithEvents Button1 As Button
    Private WithEvents btnSon As Button
    Private WithEvents btnActualise As Button
    Private WithEvents btnhelp As Button
    Friend WithEvents ButtonSuivant As Button
End Class
