﻿Imports System.Data.SqlClient
Imports System.Data.SqlServerCe
Imports System.IO
Public Class hamza
    Private Sub Update_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Update.Click
        Dim Value As Integer = 0
        connexion_begin()
        Dim nbr_articles As Integer
        nbr_articles = nb_records_article_serveur()
        '------------ bare de progression --------------------'
        Value = nbr_articles + nb_records_codesup()

        If nbr_articles <= 0 Then
            MsgBox("table Article serveur est vide !!!")
            Exit Sub
        End If

        ProgressBar.Maximum = Value
        ProgressBar.Value = 0
        ProgressBar.Visible = True

        '------------------------------------------------------'

        Dim req_read As String = "select CODART, DESART, etat, art1, qt1, qt_lot from " & Nom_table & "," & Nom_table_opt & " where CODART = CODE "
        Dim cmd_read_articles As SqlCommand = New SqlCommand(req_read, Connexion_serveur_mobile)
        Dim record_reader As SqlDataReader

        Dim etape1 As Boolean = False
        Dim etape2 As Boolean
        Dim progress As Integer = 0

        '----------- suppression de l'ancienne liste articles -------------'
        Dim req_delete_artucle_local As String = "delete from articles"
        Dim cmd_delete_article As New SqlCeCommand(req_delete_artucle_local, Connexion_Article_locale)

        Try
            cmd_delete_article.ExecuteNonQuery()
            req_delete_artucle_local = Nothing
            cmd_delete_article.Dispose()
        Catch ex As Exception
            MsgBox("problème suppression l'ancienne liste articles local --> " & ex.Message)
        End Try

        '----------- lecture liste des articles dans le serveur -------------'
        Try
            record_reader = cmd_read_articles.ExecuteReader()
            etape1 = True
        Catch ex As Exception
            MsgBox("problème lecture liste des articles" & ex.Message)
        End Try

        '----------- ecriture liste des articles dans base locale -------------'
        Dim cmd_write_articles As SqlCeCommand

        Do While record_reader.Read()
            etape2 = False
            Dim req_write As String = "insert into Articles (CODART, DESART,etat,art1,qt1,qt_lot) values ('" & record_reader.GetString(0) & "','" & record_reader.GetString(1) & "','" & record_reader.GetString(2) & "','" & record_reader.GetString(3) & "'," & val2(record_reader.GetSqlDouble(4).ToString) & "," & val2(record_reader.GetSqlDouble(5).ToString) & ")"
            cmd_write_articles = New SqlCeCommand(req_write, Connexion_Article_locale)
            Try
                cmd_write_articles.ExecuteNonQuery()
                progress = progress + 1
                ProgressBar.Value = progress
                etape2 = True
            Catch ex As Exception
                MsgBox("problème insertion articles dans base article locale" & ex.Message)
                MsgBox(req_write)
            End Try

        Loop

        record_reader.Close()

        '##################################### importation code_sup #############################################
        Dim req_read_sup As String = "select CODE, code_2, typ, qt  from " & nom_table_sup
        Dim cmd_read_sup As SqlCommand = New SqlCommand(req_read_sup, Connexion_serveur_mobile)
        Dim read_sup As SqlDataReader

        Dim etape3 As Boolean = False
        Dim etape4 As Boolean

        '----------- suppression de l'ancienne code_sup -------------'
        Dim req_delete_sup_local As String = "delete from code_sup"
        Dim cmd_delete_sup As New SqlCeCommand(req_delete_sup_local, Connexion_Article_locale)

        Try
            cmd_delete_sup.ExecuteNonQuery()
            req_delete_sup_local = Nothing
            cmd_delete_sup.Dispose()
        Catch ex As Exception
            MsgBox("problème suppression de l'ancienne code_sup --> " & ex.Message)
        End Try

        '----------- lecture liste code_sup depuis le serveur -------------'
        Try
            read_sup = cmd_read_sup.ExecuteReader()
            etape3 = True
        Catch ex As Exception
            MsgBox("problème lecture code_sup depuis le serveur" & ex.Message)
        End Try

        '----------- ecriture liste des code sup dans base locale -------------'
        Dim cmd_write_sup As SqlCeCommand

        Do While read_sup.Read()
            etape4 = False
            Dim req_write_sup As String = "insert into code_sup (CODE, code_2, typ, qt) values ('" & read_sup.GetString(0) & "','" & read_sup.GetString(1) & "','" & read_sup.GetString(2) & "'," & read_sup.GetSqlValue(3).ToString & ")"
            cmd_write_sup = New SqlCeCommand(req_write_sup, Connexion_Article_locale)
            Try
                cmd_write_sup.ExecuteNonQuery()
                progress = progress + 1
                ProgressBar.Value = progress
                etape4 = True
            Catch ex As Exception
                MsgBox("problème insertion articles dans base article locale" & ex.Message)
            End Try

        Loop

        read_sup.Close()


        connexion_end()
        '##########################################################################################
        ProgressBar.Visible = False
        If etape1 And etape2 And etape3 And etape4 Then
            MsgBox("importation [" & nbr_articles & "] Articles", MsgBoxStyle.Information)
        End If


    End Sub

    Private Sub Pointage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Pointage.Click
        prefixe = "BE_"
        suffixe = bc.Text
        Form1.Show()
        Form1.code_article.Focus()
        Me.Hide()
    End Sub

    Private Sub MAHDIA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MAHDIA.Click
        Application.Exit()
    End Sub

    Function nb_records_article_serveur() As Integer
        Dim req As String = "select count(CODART) from " & Nom_table
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim nbr_reader As SqlDataReader
        nb_records_article_serveur = 0

        Try
            nbr_reader = cmd.ExecuteReader()

            If nbr_reader.Read Then nb_records_article_serveur = nbr_reader.GetSqlInt32(0)
            nbr_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob de comptage des lignes du la table article --> " & ex.Message)
        End Try

    End Function

    Function nb_records_codesup() As Integer
        Dim req As String = "select count(CODE) from " & nom_table_sup
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim nbr_reader As SqlDataReader
        nb_records_codesup = 0

        Try
            nbr_reader = cmd.ExecuteReader()

            If nbr_reader.Read Then nb_records_codesup = nbr_reader.GetSqlInt32(0)
            nbr_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob de comptage des lignes du la table code_supp --> " & ex.Message)
        End Try

    End Function

    Private Sub hamza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '----------------------------------------ouvrire la connexion vers Article Sdf file ---------------------------------------------------------'

        Connexion_Article_locale = New SqlCeConnection("Data Source=" + path_application + "\\Articles.sdf")

        Try
            If Connexion_Article_locale.State <> ConnectionState.Open Then Connexion_Article_locale.Open()
        Catch ex As Exception
            MsgBox("prob connexion Articles sdf  file --> " & ex.Message)
        End Try
        '-------------------------------------------------------------------------------------'
        bc.Focus()

    End Sub

    Private Sub bc_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles bc.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then
            Pointage_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub bc_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bc.TextChanged

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        '------ Test if online is trueeee ------------------'
        If Not connexion_begin_test() Then
            MsgBox("Cette page fonctionne seulement en mode connecté. vérifier votre connexion au serveur", MsgBoxStyle.Information, "Serveur non joignable !!!")
            Exit Sub
        End If

        Tickets.Show()
        Me.Hide()
    End Sub
End Class