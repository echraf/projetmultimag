﻿'Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlServerCe
Public Class Inventaires

    Private Sub Inventaires_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Button1.Click
        controleur.Text = operateur
        Dim datejour As Date
        datejour = Date.Now.Date
        dateT.Text = datejour
        menu.Visible = False
        menu.Enabled = False
        connexion_begin()
        If Connexion_serveur_mobile.State = Data.ConnectionState.Open Then
            cahrger_rayons()
            connexion_end()
        Else
            rayon.Items.Add("Rayonxxx")
            rayon.Text = rayon.Items.Item(0).ToString
            zone.Items.Add("Zonexxx")
            zone.Text = zone.Items.Item(0).ToString
            rayon.Enabled = False
            zone.Enabled = False
            'quitter.Enabled = False
        End If
        magazin.Focus()
    End Sub
    Private Sub cahrger_rayons()
        Dim req As String = "select RAY_LIB from rayon"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim rayon_reader As SqlDataReader

        Try
            rayon_reader = cmd.ExecuteReader()
            rayon.Items.Clear()

            Do While rayon_reader.Read
                If Not String.IsNullOrEmpty(rayon_reader.GetString(0).ToString) Then
                    rayon.Items.Add(rayon_reader.GetString(0).ToString)
                End If
            Loop

            If rayon.Items.Count > 0 Then
                rayon.Text = rayon.Items.Item(0).ToString
                rayon.Focus()
                rayon.SelectAll()
            End If

            rayon_reader.Close()
            cmd.Dispose()

        Catch ex As Exception
            MsgBox("prob chargement liste rayons --> " & ex.Message)
        End Try
    End Sub

    Private Sub rayon_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rayon.SelectedIndexChanged
        If rayon.Text = "Rayonxxx" Then Exit Sub
        Dim req As String = "select zone,etat from " & nom_table_zone & " where rayon = '" & rayon.Text & "' and etat <> 'pointée'  and etat <> 'pointage en cours'"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim zone_reader As SqlDataReader

        connexion_begin()

        Try
            zone_reader = cmd.ExecuteReader()

            zone.Items.Clear()

            Do While zone_reader.Read
                If Not String.IsNullOrEmpty(zone_reader.GetString(0).ToString) Then
                    zone.Items.Add(zone_reader.GetString(0).ToString)
                End If
            Loop

            If zone.Items.Count > 0 Then
                zone.Text = zone.Items.Item(0).ToString
                zone.Focus()
                zone.SelectAll()
            End If

            zone_reader.Close()
            cmd.Dispose()

        Catch ex As Exception
            MsgBox("prob chargement liste zones --> " & ex.Message)
        End Try

        connexion_end()

        rayon.Focus()
    End Sub

    Private Sub valid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSuivant.Click, Button2.Click
        Dim oper As String = ""  '----opérateur chargé par l'opéartion -------'

        If magazin.Checked = True Then
            If Not String.IsNullOrEmpty(operateur) Then
                prefixe = "Inv_magazin_" & operateur & "#" & rayon.Text & "-" & zone.Text & "#"

            Else
                prefixe = "Inv_magazin#" & rayon.Text & "-" & zone.Text & "#"

            End If

            If String.IsNullOrEmpty(rayon.Text) Or String.IsNullOrEmpty(zone.Text) Then
                MsgBox("Entrer rayon et zone", MsgBoxStyle.Critical)
                Exit Sub
            End If

            operation = "invmag"
        End If

        If depot.Checked = True Then
            If Not String.IsNullOrEmpty(operateur) Then
                prefixe = "Inv_depot_" & operateur & "#" & rayon.Text & "-" & zone.Text & "#"
            Else
                prefixe = "Inv_depot#" & rayon.Text & "-" & zone.Text & "#"
            End If
            If String.IsNullOrEmpty(rayon.Text) Or String.IsNullOrEmpty(zone.Text) Then
                MsgBox("Entrer rayon et zone", MsgBoxStyle.Critical)
                Exit Sub
            End If

            operation = "invdepot"
        End If
        '---------------------------------------------------------------'
        'prefixe = Replace(prefixe, " ", "_")
        '---------------------------------------------------------------'
        If rayon.Enabled = False Then GoTo suite
        '---------------------------------------------------------------'
        If operation = "invmag" Or operation = "invdepot" Then
            connexion_begin()
            Select Case etat_pointage(oper, Trim(zone.Text), Trim(rayon.Text))
                Case "pointée"
                    MsgBox("Zone déjà pointée par " & oper, MsgBoxStyle.Critical)
                    Exit Sub
                Case "pointage en cours"
                    MsgBox("Zone en cours de pointage par " & oper, MsgBoxStyle.Critical)
                    Exit Sub
            End Select
            maj_etat_zone("pointage en cours", Trim(zone.Text), Trim(rayon.Text))
            connexion_end()
        End If
        '-------------------------------------------------------------------------'
        rayon_var = rayon.Text
        zone_var = zone.Text
suite:  Tickets.Show()
        Tickets.code_article.Focus()
        Me.Hide()

    End Sub

    ' Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Form_0.Show()
    'Me.Hide()
    ' End Sub
    Private Sub quitter_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        connexion_begin()
        If Connexion_serveur_mobile.State = Data.ConnectionState.Open Then
            rayon.Enabled = True
            zone.Enabled = True
            cahrger_rayons()
            connexion_end()
        Else
            rayon.Enabled = False
            zone.Enabled = False
            rayon.Items.Add("Rayonxxx")
            rayon.Text = rayon.Items.Item(0).ToString
            zone.Items.Add("Zonexxx")
            zone.Text = zone.Items.Item(0).ToString

            'quitter.Enabled = False
        End If
        magazin.Focus()

    End Sub

    '   Private Sub op_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles op.GotFocus
    '      op.BackColor = Color.Aqua
    '     op.Select(0, Len(op.Text))
    '    op.Focus()
    '    End Sub

    'Private Sub op_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles op.LostFocus
    '   op.BackColor = Color.White
    'End Sub

    Private Sub btnmenu_Click(sender As Object, e As EventArgs) Handles btnmenu.Click
        If (menu.Visible = False) Then 'New Size(0, 176)) Then
            menu.Visible = True
            menu.Enabled = True
            'Panel1.Size = New Size(82, 176)
        Else
            ' Panel1.Size = New Size(0, 176)
            menu.Visible = False
            menu.Enabled = False
        End If
    End Sub

    Private Sub fermerMenu(sender As Object, e As EventArgs) Handles MyBase.Click
        menu.Visible = False
        menu.Enabled = False
    End Sub



    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub



    Private Sub Deconnecte(sender As Object, e As EventArgs) Handles btnhelp.Click
        operateur = ""
        Form0.Show()
        Me.Hide()
    End Sub
End Class