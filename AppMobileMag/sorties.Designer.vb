﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class sorties
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.usage = New System.Windows.Forms.RadioButton()
        Me.retour = New System.Windows.Forms.RadioButton()
        Me.casse = New System.Windows.Forms.RadioButton()
        Me.perime = New System.Windows.Forms.RadioButton()
        Me.op = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnmenu = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.menu = New System.Windows.Forms.Panel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnSon = New System.Windows.Forms.Button()
        Me.btnActualise = New System.Windows.Forms.Button()
        Me.btnhelp = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.menu.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.usage)
        Me.Panel1.Controls.Add(Me.retour)
        Me.Panel1.Controls.Add(Me.casse)
        Me.Panel1.Controls.Add(Me.perime)
        Me.Panel1.Location = New System.Drawing.Point(15, 61)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(211, 201)
        Me.Panel1.TabIndex = 14
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Button2.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(54, 161)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(108, 29)
        Me.Button2.TabIndex = 166
        Me.Button2.Text = "Suivant"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(33, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(147, 17)
        Me.Label1.TabIndex = 165
        Me.Label1.Text = "Choisir le type de sortie"
        '
        'usage
        '
        Me.usage.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.usage.Location = New System.Drawing.Point(61, 125)
        Me.usage.Name = "usage"
        Me.usage.Size = New System.Drawing.Size(134, 19)
        Me.usage.TabIndex = 17
        Me.usage.Text = "Usage Magazin"
        '
        'retour
        '
        Me.retour.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.retour.Location = New System.Drawing.Point(61, 65)
        Me.retour.Name = "retour"
        Me.retour.Size = New System.Drawing.Size(77, 19)
        Me.retour.TabIndex = 16
        Me.retour.Text = "Retour"
        '
        'casse
        '
        Me.casse.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.casse.Location = New System.Drawing.Point(61, 96)
        Me.casse.Name = "casse"
        Me.casse.Size = New System.Drawing.Size(95, 19)
        Me.casse.TabIndex = 15
        Me.casse.Text = "Casse"
        '
        'perime
        '
        Me.perime.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.perime.Location = New System.Drawing.Point(61, 34)
        Me.perime.Name = "perime"
        Me.perime.Size = New System.Drawing.Size(77, 19)
        Me.perime.TabIndex = 13
        Me.perime.Text = "Périmé"
        '
        'op
        '
        Me.op.Location = New System.Drawing.Point(88, 38)
        Me.op.Name = "op"
        Me.op.Size = New System.Drawing.Size(122, 20)
        Me.op.TabIndex = 7
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.SeaGreen
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.btnmenu)
        Me.Panel3.Location = New System.Drawing.Point(-1, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(239, 32)
        Me.Panel3.TabIndex = 110
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label3.Location = New System.Drawing.Point(43, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(161, 17)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Mouvement de Sortie ...."
        '
        'btnmenu
        '
        Me.btnmenu.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.Bars_hamburger_list_menu_navigation_options_1283
        Me.btnmenu.Location = New System.Drawing.Point(2, 1)
        Me.btnmenu.Name = "btnmenu"
        Me.btnmenu.Size = New System.Drawing.Size(35, 29)
        Me.btnmenu.TabIndex = 22
        Me.btnmenu.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.SeaGreen
        Me.Panel4.Controls.Add(Me.Label2)
        Me.Panel4.Location = New System.Drawing.Point(-1, 270)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(239, 20)
        Me.Panel4.TabIndex = 111
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(13, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Date :"
        '
        'menu
        '
        Me.menu.BackColor = System.Drawing.Color.SeaGreen
        Me.menu.Controls.Add(Me.Button4)
        Me.menu.Controls.Add(Me.Button3)
        Me.menu.Controls.Add(Me.Button1)
        Me.menu.Controls.Add(Me.btnSon)
        Me.menu.Controls.Add(Me.btnActualise)
        Me.menu.Controls.Add(Me.btnhelp)
        Me.menu.Location = New System.Drawing.Point(0, 32)
        Me.menu.Name = "menu"
        Me.menu.Size = New System.Drawing.Size(82, 176)
        Me.menu.TabIndex = 22
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button4.Location = New System.Drawing.Point(3, 142)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(77, 31)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "Quitter"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button3.Location = New System.Drawing.Point(3, 58)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(77, 29)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "M.Entrée"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button1.Location = New System.Drawing.Point(3, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(76, 29)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Actualiser"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnSon
        '
        Me.btnSon.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSon.Location = New System.Drawing.Point(3, 31)
        Me.btnSon.Name = "btnSon"
        Me.btnSon.Size = New System.Drawing.Size(77, 28)
        Me.btnSon.TabIndex = 1
        Me.btnSon.Text = "Aide"
        Me.btnSon.UseVisualStyleBackColor = False
        '
        'btnActualise
        '
        Me.btnActualise.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnActualise.Location = New System.Drawing.Point(4, 86)
        Me.btnActualise.Name = "btnActualise"
        Me.btnActualise.Size = New System.Drawing.Size(76, 29)
        Me.btnActualise.TabIndex = 2
        Me.btnActualise.Text = "M.Tranfert"
        Me.btnActualise.UseVisualStyleBackColor = False
        '
        'btnhelp
        '
        Me.btnhelp.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnhelp.Location = New System.Drawing.Point(3, 114)
        Me.btnhelp.Name = "btnhelp"
        Me.btnhelp.Size = New System.Drawing.Size(77, 29)
        Me.btnhelp.TabIndex = 3
        Me.btnhelp.Text = "Déconnecter"
        Me.btnhelp.UseVisualStyleBackColor = False
        '
        'sorties
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(238, 290)
        Me.ControlBox = False
        Me.Controls.Add(Me.menu)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.op)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "sorties"
        Me.Text = "Mouvements De Sorties ..."
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.menu.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents op As System.Windows.Forms.TextBox
    Friend WithEvents usage As System.Windows.Forms.RadioButton
    Friend WithEvents retour As System.Windows.Forms.RadioButton
    Friend WithEvents casse As System.Windows.Forms.RadioButton
    Friend WithEvents perime As System.Windows.Forms.RadioButton
    Private WithEvents btnmenu As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label2 As Label
    Private WithEvents menu As Panel
    Private WithEvents Button4 As Button
    Private WithEvents Button3 As Button
    Private WithEvents Button1 As Button
    Private WithEvents btnSon As Button
    Private WithEvents btnActualise As Button
    Private WithEvents btnhelp As Button
End Class
