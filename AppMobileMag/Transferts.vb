﻿'Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlServerCe
Public Class Transferts

    Private Sub valid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If Mg1.Text = Mg2.Text Then
            MsgBox("Erreur :Mg_source = Mg_Destination !!!!")
            Exit Sub
        End If
        '--------------------------------------------------'
        'operateur = op.Text
        prefixe = "TR_"
        operation = "TR"

        If operateur <> "" Then
            suffixe = operateur & "_" & Mg1.Text & "_" & Mg2.Text
        Else
            suffixe = Mg1.Text & "_" & Mg2.Text
        End If

        '------------------------------------------------'
        prefixe = Replace(prefixe, " ", "_")
        '------------------------------------------------'
        Form1.Show()
        Form1.code_article.Focus()
        Me.Hide()
    End Sub

    Private Sub Transferts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Button1.AutoSizeChanged
        ' Panel1.Size = New Size(0, 176)
        menu.Visible = False
        menu.Enabled = False
        connexion_begin()
        If Connexion_serveur_mobile.State = Data.ConnectionState.Open Then
            cahrger_Magasion(Mg1)
            cahrger_Magasion(Mg2)
            connexion_end()
        Else
            Mg1.Items.Add("Mg_Source")
            Mg1.Text = Mg1.Items.Item(0).ToString
            Mg2.Items.Add("Mg_Dest")
            Mg2.Text = Mg2.Items.Item(0).ToString
        End If

        ' op.Focus()
    End Sub
    Private Sub cahrger_Magasion(ByVal dest As ComboBox)
        Dim req As String = "select [désignation] from MAGASIN"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim dest_reader As SqlDataReader


        Try
            dest_reader = cmd.ExecuteReader()

            dest.Items.Clear()

            Do While dest_reader.Read
                If Not String.IsNullOrEmpty(dest_reader.GetString(0).ToString) Then
                    dest.Items.Add(dest_reader.GetString(0).ToString)
                End If
            Loop

            If dest.Items.Count > 0 Then
                dest.Text = dest.Items.Item(0).ToString
                dest.Focus()
                dest.SelectAll()
            End If

            dest_reader.Close()
            cmd.Dispose()

        Catch ex As Exception
            MsgBox("prob chargement liste magazin --> " & ex.Message)
        End Try



    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form_0.Show()
        Me.Hide()
    End Sub

    Private Sub quitter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        connexion_begin()
        If Connexion_serveur_mobile.State = Data.ConnectionState.Open Then
            cahrger_Magasion(Mg1)
            cahrger_Magasion(Mg2)
            connexion_end()
        Else
            Mg1.Items.Add("Mg_Source")
            Mg1.Text = Mg1.Items.Item(0).ToString
            Mg2.Items.Add("Mg_Dest")
            Mg2.Text = Mg2.Items.Item(0).ToString
        End If

        ' op.Focus()
    End Sub

    ' Private Sub op_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles op.GotFocus
    '    op.BackColor = Color.Aqua
    '   op.Select(0, Len(op.Text))
    '  op.Focus()
    'End Sub

    'Private Sub op_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles op.LostFocus
    '   op.BackColor = Color.White
    'End Sub


    Private Sub Panel1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Panel1.GotFocus

    End Sub

    Private Sub fermerMenu(sender As Object, e As EventArgs) Handles MyBase.Click
        menu.Visible = False
        menu.Enabled = False
    End Sub

    Private Sub btnmenu_Click(sender As Object, e As EventArgs) Handles btnmenu.Click
        If (menu.Visible = False) Then 'New Size(0, 176)) Then
            menu.Visible = True
            menu.Enabled = True
            'Panel1.Size = New Size(82, 176)
        Else
            ' Panel1.Size = New Size(0, 176)
            menu.Visible = False
            menu.Enabled = False
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        sorties.Show()
        Me.Hide()
    End Sub

    Private Sub btnActualise_Click(sender As Object, e As EventArgs) Handles btnActualise.Click
        entrees.Show()
        Me.Hide()
    End Sub

    Private Sub btnhelp_Click(sender As Object, e As EventArgs) Handles btnhelp.Click
        operateur = ""
        Form0.Show()
        Me.Hide()
    End Sub
End Class