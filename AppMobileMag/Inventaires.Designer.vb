﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Inventaires
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Inventaires))
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rayon = New System.Windows.Forms.ComboBox()
        Me.zone = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.depot = New System.Windows.Forms.RadioButton()
        Me.magazin = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.controleur = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.menu = New System.Windows.Forms.Panel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnSon = New System.Windows.Forms.Button()
        Me.btnhelp = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btnmenu = New System.Windows.Forms.Button()
        Me.ButtonSuivant = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dateT = New System.Windows.Forms.Label()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.menu.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.Button2)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.ButtonSuivant)
        Me.Panel3.Controls.Add(Me.rayon)
        Me.Panel3.Controls.Add(Me.zone)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Panel3.Location = New System.Drawing.Point(19, 99)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(202, 151)
        Me.Panel3.TabIndex = 14
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(3, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 13)
        Me.Label2.TabIndex = 166
        Me.Label2.Text = "Choisir le rayon et la Zone "
        '
        'rayon
        '
        Me.rayon.Location = New System.Drawing.Point(55, 41)
        Me.rayon.Name = "rayon"
        Me.rayon.Size = New System.Drawing.Size(137, 21)
        Me.rayon.TabIndex = 24
        '
        'zone
        '
        Me.zone.Location = New System.Drawing.Point(54, 79)
        Me.zone.Name = "zone"
        Me.zone.Size = New System.Drawing.Size(138, 21)
        Me.zone.TabIndex = 19
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(12, 80)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 21)
        Me.Label5.TabIndex = 104
        Me.Label5.Text = "Zone:"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(7, 42)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 21)
        Me.Label4.TabIndex = 105
        Me.Label4.Text = "Rayon:"
        '
        'depot
        '
        Me.depot.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.depot.ForeColor = System.Drawing.Color.Black
        Me.depot.Location = New System.Drawing.Point(131, 5)
        Me.depot.Name = "depot"
        Me.depot.Size = New System.Drawing.Size(64, 19)
        Me.depot.TabIndex = 14
        Me.depot.Text = "Dépôt"
        '
        'magazin
        '
        Me.magazin.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.magazin.ForeColor = System.Drawing.Color.Black
        Me.magazin.Location = New System.Drawing.Point(34, 5)
        Me.magazin.Name = "magazin"
        Me.magazin.Size = New System.Drawing.Size(106, 19)
        Me.magazin.TabIndex = 12
        Me.magazin.Text = "Magazin"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.SteelBlue
        Me.Panel2.Controls.Add(Me.controleur)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.btnmenu)
        Me.Panel2.Location = New System.Drawing.Point(1, -1)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(237, 33)
        Me.Panel2.TabIndex = 107
        '
        'controleur
        '
        Me.controleur.AutoSize = True
        Me.controleur.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.controleur.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.controleur.Location = New System.Drawing.Point(165, 7)
        Me.controleur.Name = "controleur"
        Me.controleur.Size = New System.Drawing.Size(23, 15)
        Me.controleur.TabIndex = 23
        Me.controleur.Text = "...."
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label1.Location = New System.Drawing.Point(38, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Page Controle : Mr"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel4.Controls.Add(Me.depot)
        Me.Panel4.Controls.Add(Me.magazin)
        Me.Panel4.Location = New System.Drawing.Point(22, 70)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(198, 39)
        Me.Panel4.TabIndex = 108
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.SteelBlue
        Me.Panel5.Controls.Add(Me.dateT)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Location = New System.Drawing.Point(1, 274)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(237, 16)
        Me.Panel5.TabIndex = 109
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Maroon
        Me.Label3.Location = New System.Drawing.Point(140, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 110
        Me.Label3.Text = "Data"
        '
        'menu
        '
        Me.menu.BackColor = System.Drawing.Color.SteelBlue
        Me.menu.Controls.Add(Me.Button4)
        Me.menu.Controls.Add(Me.Button1)
        Me.menu.Controls.Add(Me.btnSon)
        Me.menu.Controls.Add(Me.btnhelp)
        Me.menu.Location = New System.Drawing.Point(1, 32)
        Me.menu.Name = "menu"
        Me.menu.Size = New System.Drawing.Size(82, 115)
        Me.menu.TabIndex = 167
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button4.Location = New System.Drawing.Point(3, 86)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(77, 28)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "Quitter"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button1.Location = New System.Drawing.Point(3, 1)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(76, 29)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Actualiser"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnSon
        '
        Me.btnSon.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSon.Location = New System.Drawing.Point(3, 30)
        Me.btnSon.Name = "btnSon"
        Me.btnSon.Size = New System.Drawing.Size(77, 28)
        Me.btnSon.TabIndex = 1
        Me.btnSon.Text = "Aide"
        Me.btnSon.UseVisualStyleBackColor = False
        '
        'btnhelp
        '
        Me.btnhelp.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnhelp.Location = New System.Drawing.Point(3, 57)
        Me.btnhelp.Name = "btnhelp"
        Me.btnhelp.Size = New System.Drawing.Size(77, 29)
        Me.btnhelp.TabIndex = 3
        Me.btnhelp.Text = "Déconnecter"
        Me.btnhelp.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button2.Font = New System.Drawing.Font("Microsoft YaHei UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(57, 110)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(96, 33)
        Me.Button2.TabIndex = 167
        Me.Button2.Text = "Valider"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'btnmenu
        '
        Me.btnmenu.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.menu
        Me.btnmenu.Location = New System.Drawing.Point(0, 0)
        Me.btnmenu.Name = "btnmenu"
        Me.btnmenu.Size = New System.Drawing.Size(35, 33)
        Me.btnmenu.TabIndex = 22
        Me.btnmenu.UseVisualStyleBackColor = True
        '
        'ButtonSuivant
        '
        Me.ButtonSuivant.BackColor = System.Drawing.Color.Transparent
        Me.ButtonSuivant.BackgroundImage = CType(resources.GetObject("ButtonSuivant.BackgroundImage"), System.Drawing.Image)
        Me.ButtonSuivant.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ButtonSuivant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSuivant.Location = New System.Drawing.Point(206, 107)
        Me.ButtonSuivant.Name = "ButtonSuivant"
        Me.ButtonSuivant.Size = New System.Drawing.Size(28, 31)
        Me.ButtonSuivant.TabIndex = 165
        Me.ButtonSuivant.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ButtonSuivant.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(185, 270)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(0, 13)
        Me.Label6.TabIndex = 168
        '
        'dateT
        '
        Me.dateT.AutoSize = True
        Me.dateT.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dateT.ForeColor = System.Drawing.Color.Maroon
        Me.dateT.Location = New System.Drawing.Point(170, 0)
        Me.dateT.Name = "dateT"
        Me.dateT.Size = New System.Drawing.Size(40, 13)
        Me.dateT.TabIndex = 111
        Me.dateT.Text = "Label7"
        '
        'Inventaires
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(238, 290)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.menu)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel4)
        Me.Name = "Inventaires"
        Me.Text = "Contrôles et Inventaires"
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.menu.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents rayon As System.Windows.Forms.ComboBox
    Friend WithEvents zone As System.Windows.Forms.ComboBox
    Friend WithEvents depot As System.Windows.Forms.RadioButton
    Friend WithEvents magazin As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Private WithEvents btnmenu As Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents ButtonSuivant As Button
    Friend WithEvents controleur As Label
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Label3 As Label
    Private WithEvents menu As Panel
    Private WithEvents Button4 As Button
    Private WithEvents Button1 As Button
    Private WithEvents btnSon As Button
    Private WithEvents btnhelp As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents dateT As Label
End Class
