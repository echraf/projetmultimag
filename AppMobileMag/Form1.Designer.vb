﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.ordre_modif = New System.Windows.Forms.TextBox()
        Me.qte_article = New System.Windows.Forms.TextBox()
        Me.code_article = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.valide = New System.Windows.Forms.Button()
        Me.DataGrid1 = New System.Windows.Forms.DataGrid()
        Me.quitter = New System.Windows.Forms.Button()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.import_bases = New System.Windows.Forms.Button()
        Me.panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lib_article = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btnSon = New System.Windows.Forms.Button()
        Me.btnActualise = New System.Windows.Forms.Button()
        Me.btnhelp = New System.Windows.Forms.Button()
        Me.btnmenu = New System.Windows.Forms.Button()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ordre_modif
        '
        Me.ordre_modif.BackColor = System.Drawing.Color.Gray
        resources.ApplyResources(Me.ordre_modif, "ordre_modif")
        Me.ordre_modif.ForeColor = System.Drawing.Color.White
        Me.ordre_modif.Name = "ordre_modif"
        '
        'qte_article
        '
        resources.ApplyResources(Me.qte_article, "qte_article")
        Me.qte_article.Name = "qte_article"
        '
        'code_article
        '
        resources.ApplyResources(Me.code_article, "code_article")
        Me.code_article.Name = "code_article"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Name = "Label2"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Name = "Label1"
        '
        'valide
        '
        Me.valide.BackColor = System.Drawing.Color.WhiteSmoke
        resources.ApplyResources(Me.valide, "valide")
        Me.valide.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.valide.Name = "valide"
        Me.valide.UseVisualStyleBackColor = False
        '
        'DataGrid1
        '
        Me.DataGrid1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DataGrid1.CaptionVisible = False
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        resources.ApplyResources(Me.DataGrid1, "DataGrid1")
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.RowHeadersVisible = False
        Me.DataGrid1.TabStop = False
        '
        'quitter
        '
        Me.quitter.BackColor = System.Drawing.Color.WhiteSmoke
        resources.ApplyResources(Me.quitter, "quitter")
        Me.quitter.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.quitter.Name = "quitter"
        Me.quitter.UseVisualStyleBackColor = False
        '
        'ProgressBar
        '
        resources.ApplyResources(Me.ProgressBar, "ProgressBar")
        Me.ProgressBar.ForeColor = System.Drawing.Color.SeaGreen
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Value = 70
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.WhiteSmoke
        resources.ApplyResources(Me.Button1, "Button1")
        Me.Button1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button1.Name = "Button1"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'import_bases
        '
        Me.import_bases.BackColor = System.Drawing.Color.WhiteSmoke
        resources.ApplyResources(Me.import_bases, "import_bases")
        Me.import_bases.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.import_bases.Name = "import_bases"
        Me.import_bases.UseVisualStyleBackColor = False
        '
        'panel1
        '
        Me.panel1.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.panel1.Controls.Add(Me.Button4)
        Me.panel1.Controls.Add(Me.Button3)
        Me.panel1.Controls.Add(Me.Button2)
        Me.panel1.Controls.Add(Me.btnSon)
        Me.panel1.Controls.Add(Me.btnActualise)
        Me.panel1.Controls.Add(Me.btnhelp)
        resources.ApplyResources(Me.panel1, "panel1")
        Me.panel1.Name = "panel1"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.SeaGreen
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.Button5)
        resources.ApplyResources(Me.Panel3, "Panel3")
        Me.Panel3.Name = "Panel3"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.Name = "Label3"
        '
        'lib_article
        '
        resources.ApplyResources(Me.lib_article, "lib_article")
        Me.lib_article.Name = "lib_article"
        '
        'Button5
        '
        Me.Button5.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.Bars_hamburger_list_menu_navigation_options_1282
        resources.ApplyResources(Me.Button5, "Button5")
        Me.Button5.Name = "Button5"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.f
        resources.ApplyResources(Me.Button4, "Button4")
        Me.Button4.Name = "Button4"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.images2
        resources.ApplyResources(Me.Button3, "Button3")
        Me.Button3.Name = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources._500_F_35104677_mtVghtwUo6UYnEdnSbqweEtF7cX1XG3A
        resources.ApplyResources(Me.Button2, "Button2")
        Me.Button2.Name = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'btnSon
        '
        Me.btnSon.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.téléchargement3
        resources.ApplyResources(Me.btnSon, "btnSon")
        Me.btnSon.Name = "btnSon"
        Me.btnSon.UseVisualStyleBackColor = True
        '
        'btnActualise
        '
        Me.btnActualise.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.téléchargement1
        resources.ApplyResources(Me.btnActualise, "btnActualise")
        Me.btnActualise.Name = "btnActualise"
        Me.btnActualise.UseVisualStyleBackColor = True
        '
        'btnhelp
        '
        Me.btnhelp.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.téléchargement4
        resources.ApplyResources(Me.btnhelp, "btnhelp")
        Me.btnhelp.Name = "btnhelp"
        Me.btnhelp.UseVisualStyleBackColor = True
        '
        'btnmenu
        '
        Me.btnmenu.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.menu
        resources.ApplyResources(Me.btnmenu, "btnmenu")
        Me.btnmenu.Name = "btnmenu"
        Me.btnmenu.UseVisualStyleBackColor = True
        '
        'Form1
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ControlBox = False
        Me.Controls.Add(Me.lib_article)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.panel1)
        Me.Controls.Add(Me.btnmenu)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.qte_article)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ordre_modif)
        Me.Controls.Add(Me.code_article)
        Me.Controls.Add(Me.valide)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.import_bases)
        Me.Controls.Add(Me.quitter)
        Me.Controls.Add(Me.DataGrid1)
        Me.ForeColor = System.Drawing.Color.Black
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form1"
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents code_article As System.Windows.Forms.TextBox
    Friend WithEvents valide As System.Windows.Forms.Button
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents qte_article As System.Windows.Forms.TextBox
    Friend WithEvents quitter As System.Windows.Forms.Button
    Friend WithEvents ordre_modif As System.Windows.Forms.TextBox
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents import_bases As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents panel1 As Panel
    Private WithEvents Button3 As Button
    Private WithEvents Button2 As Button
    Private WithEvents btnSon As Button
    Private WithEvents btnActualise As Button
    Private WithEvents btnhelp As Button
    Private WithEvents btnmenu As Button
    Private WithEvents Button4 As Button
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label3 As Label
    Private WithEvents Button5 As Button
    Friend WithEvents lib_article As Label
End Class
