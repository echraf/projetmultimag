﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class Tickets
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.code_article = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.prix = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Annuler_obs = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.obs = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.affichage = New System.Windows.Forms.CheckBox()
        Me.Annuler_tickets = New System.Windows.Forms.Button()
        Me.valide = New System.Windows.Forms.Button()
        Me.remq = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.nbr_tickets = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.DataGrid1 = New System.Windows.Forms.DataGrid()
        Me.lib_article = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.controleur = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnmenu = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dateT = New System.Windows.Forms.Label()
        Me.menu = New System.Windows.Forms.Panel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btnSon = New System.Windows.Forms.Button()
        Me.btnhelp = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.menu.SuspendLayout()
        Me.SuspendLayout()
        '
        'code_article
        '
        Me.code_article.BackColor = System.Drawing.Color.White
        Me.code_article.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.code_article.Location = New System.Drawing.Point(82, 39)
        Me.code_article.Name = "code_article"
        Me.code_article.Size = New System.Drawing.Size(134, 13)
        Me.code_article.TabIndex = 12
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(20, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 22)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Code   :"
        '
        'prix
        '
        Me.prix.BackColor = System.Drawing.Color.Transparent
        Me.prix.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.prix.ForeColor = System.Drawing.Color.Red
        Me.prix.Location = New System.Drawing.Point(123, 63)
        Me.prix.Name = "prix"
        Me.prix.Size = New System.Drawing.Size(82, 27)
        Me.prix.TabIndex = 18
        Me.prix.Text = "<Prix>"
        Me.prix.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(-1, 97)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(239, 178)
        Me.TabControl1.TabIndex = 17
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.CheckBox3)
        Me.TabPage1.Controls.Add(Me.CheckBox2)
        Me.TabPage1.Controls.Add(Me.CheckBox1)
        Me.TabPage1.Controls.Add(Me.Annuler_obs)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.obs)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Location = New System.Drawing.Point(4, 26)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(231, 148)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Reclamations "
        '
        'CheckBox3
        '
        Me.CheckBox3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.ForeColor = System.Drawing.Color.Black
        Me.CheckBox3.Location = New System.Drawing.Point(12, 48)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(178, 20)
        Me.CheckBox3.TabIndex = 37
        Me.CheckBox3.Text = "Manque de Ticket"
        '
        'CheckBox2
        '
        Me.CheckBox2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.ForeColor = System.Drawing.Color.Black
        Me.CheckBox2.Location = New System.Drawing.Point(12, 28)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(178, 20)
        Me.CheckBox2.TabIndex = 36
        Me.CheckBox2.Text = "Défaut de rangement"
        '
        'CheckBox1
        '
        Me.CheckBox1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.Black
        Me.CheckBox1.Location = New System.Drawing.Point(12, 8)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(134, 20)
        Me.CheckBox1.TabIndex = 35
        Me.CheckBox1.Text = "Prix Incorrect"
        '
        'Annuler_obs
        '
        Me.Annuler_obs.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Annuler_obs.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Annuler_obs.ForeColor = System.Drawing.Color.Black
        Me.Annuler_obs.Location = New System.Drawing.Point(38, 116)
        Me.Annuler_obs.Name = "Annuler_obs"
        Me.Annuler_obs.Size = New System.Drawing.Size(78, 28)
        Me.Annuler_obs.TabIndex = 28
        Me.Annuler_obs.Text = "&Annuler"
        Me.Annuler_obs.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(128, 116)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(78, 28)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "&Envoyer"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'obs
        '
        Me.obs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.obs.Location = New System.Drawing.Point(55, 69)
        Me.obs.Multiline = True
        Me.obs.Name = "obs"
        Me.obs.Size = New System.Drawing.Size(168, 41)
        Me.obs.TabIndex = 16
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(9, 69)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 23)
        Me.Label4.TabIndex = 29
        Me.Label4.Text = "Détail"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage2.Controls.Add(Me.Panel3)
        Me.TabPage2.Controls.Add(Me.affichage)
        Me.TabPage2.Controls.Add(Me.Annuler_tickets)
        Me.TabPage2.Controls.Add(Me.valide)
        Me.TabPage2.Controls.Add(Me.remq)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.nbr_tickets)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Location = New System.Drawing.Point(4, 26)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(231, 148)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Tickets "
        '
        'affichage
        '
        Me.affichage.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.affichage.ForeColor = System.Drawing.Color.Blue
        Me.affichage.Location = New System.Drawing.Point(8, 8)
        Me.affichage.Name = "affichage"
        Me.affichage.Size = New System.Drawing.Size(100, 23)
        Me.affichage.TabIndex = 35
        Me.affichage.Text = "Affichage"
        Me.affichage.Visible = False
        '
        'Annuler_tickets
        '
        Me.Annuler_tickets.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Annuler_tickets.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Annuler_tickets.ForeColor = System.Drawing.Color.Black
        Me.Annuler_tickets.Location = New System.Drawing.Point(26, 117)
        Me.Annuler_tickets.Name = "Annuler_tickets"
        Me.Annuler_tickets.Size = New System.Drawing.Size(82, 25)
        Me.Annuler_tickets.TabIndex = 27
        Me.Annuler_tickets.Text = "&Annuler"
        Me.Annuler_tickets.UseVisualStyleBackColor = False
        '
        'valide
        '
        Me.valide.BackColor = System.Drawing.Color.WhiteSmoke
        Me.valide.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.valide.ForeColor = System.Drawing.Color.Black
        Me.valide.Location = New System.Drawing.Point(112, 117)
        Me.valide.Name = "valide"
        Me.valide.Size = New System.Drawing.Size(80, 25)
        Me.valide.TabIndex = 24
        Me.valide.Text = "Envoyer"
        Me.valide.UseVisualStyleBackColor = False
        '
        'remq
        '
        Me.remq.BackColor = System.Drawing.Color.White
        Me.remq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.remq.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.remq.ForeColor = System.Drawing.Color.Red
        Me.remq.Location = New System.Drawing.Point(54, 64)
        Me.remq.Multiline = True
        Me.remq.Name = "remq"
        Me.remq.Size = New System.Drawing.Size(168, 41)
        Me.remq.TabIndex = 21
        Me.remq.TabStop = False
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(4, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 23)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Détail"
        '
        'nbr_tickets
        '
        Me.nbr_tickets.BackColor = System.Drawing.Color.WhiteSmoke
        Me.nbr_tickets.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.nbr_tickets.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nbr_tickets.Location = New System.Drawing.Point(134, 24)
        Me.nbr_tickets.Name = "nbr_tickets"
        Me.nbr_tickets.Size = New System.Drawing.Size(88, 18)
        Me.nbr_tickets.TabIndex = 20
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(46, 29)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(103, 23)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Nbr Tickets   :"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.DataGrid1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 26)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(231, 148)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Liste"
        '
        'DataGrid1
        '
        Me.DataGrid1.AllowDrop = True
        Me.DataGrid1.BackgroundColor = System.Drawing.Color.LightGray
        Me.DataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DataGrid1.CaptionBackColor = System.Drawing.Color.Red
        Me.DataGrid1.CaptionForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.DataGrid1.CaptionVisible = False
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.FlatMode = True
        Me.DataGrid1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(3, 3)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.ParentRowsVisible = False
        Me.DataGrid1.RowHeadersVisible = False
        Me.DataGrid1.RowHeaderWidth = 0
        Me.DataGrid1.Size = New System.Drawing.Size(225, 143)
        Me.DataGrid1.TabIndex = 7
        Me.DataGrid1.TabStop = False
        '
        'lib_article
        '
        Me.lib_article.AutoSize = True
        Me.lib_article.BackColor = System.Drawing.Color.Transparent
        Me.lib_article.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lib_article.ForeColor = System.Drawing.Color.Black
        Me.lib_article.Location = New System.Drawing.Point(18, 66)
        Me.lib_article.Name = "lib_article"
        Me.lib_article.Size = New System.Drawing.Size(51, 20)
        Me.lib_article.TabIndex = 20
        Me.lib_article.Text = "Label1"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.SteelBlue
        Me.Panel2.Controls.Add(Me.controleur)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.btnmenu)
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(239, 33)
        Me.Panel2.TabIndex = 108
        '
        'controleur
        '
        Me.controleur.AutoSize = True
        Me.controleur.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.controleur.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.controleur.Location = New System.Drawing.Point(149, 7)
        Me.controleur.Name = "controleur"
        Me.controleur.Size = New System.Drawing.Size(19, 15)
        Me.controleur.TabIndex = 23
        Me.controleur.Text = "...."
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label1.Location = New System.Drawing.Point(38, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Page Controle : Mr"
        '
        'btnmenu
        '
        Me.btnmenu.BackgroundImage = Global.Poinatge_PC.My.Resources.Resources.menu
        Me.btnmenu.Location = New System.Drawing.Point(0, 0)
        Me.btnmenu.Name = "btnmenu"
        Me.btnmenu.Size = New System.Drawing.Size(35, 30)
        Me.btnmenu.TabIndex = 22
        Me.btnmenu.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Black
        Me.Panel1.Location = New System.Drawing.Point(88, 54)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(117, 1)
        Me.Panel1.TabIndex = 109
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.SteelBlue
        Me.Panel5.Controls.Add(Me.dateT)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Location = New System.Drawing.Point(1, 274)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(237, 16)
        Me.Panel5.TabIndex = 110
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Maroon
        Me.Label3.Location = New System.Drawing.Point(127, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 110
        Me.Label3.Text = "Data"
        '
        'dateT
        '
        Me.dateT.AutoSize = True
        Me.dateT.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dateT.ForeColor = System.Drawing.Color.Maroon
        Me.dateT.Location = New System.Drawing.Point(160, 0)
        Me.dateT.Name = "dateT"
        Me.dateT.Size = New System.Drawing.Size(0, 13)
        Me.dateT.TabIndex = 111
        '
        'menu
        '
        Me.menu.BackColor = System.Drawing.Color.SteelBlue
        Me.menu.Controls.Add(Me.Button4)
        Me.menu.Controls.Add(Me.Button2)
        Me.menu.Controls.Add(Me.btnSon)
        Me.menu.Controls.Add(Me.btnhelp)
        Me.menu.Location = New System.Drawing.Point(0, 30)
        Me.menu.Name = "menu"
        Me.menu.Size = New System.Drawing.Size(82, 115)
        Me.menu.TabIndex = 168
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button4.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(3, 86)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(77, 28)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "Quitter"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button2.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(3, 1)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(76, 29)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "Actualiser"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'btnSon
        '
        Me.btnSon.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSon.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSon.ForeColor = System.Drawing.Color.Black
        Me.btnSon.Location = New System.Drawing.Point(3, 30)
        Me.btnSon.Name = "btnSon"
        Me.btnSon.Size = New System.Drawing.Size(77, 28)
        Me.btnSon.TabIndex = 1
        Me.btnSon.Text = "Aide"
        Me.btnSon.UseVisualStyleBackColor = False
        '
        'btnhelp
        '
        Me.btnhelp.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnhelp.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnhelp.ForeColor = System.Drawing.Color.Black
        Me.btnhelp.Location = New System.Drawing.Point(3, 57)
        Me.btnhelp.Name = "btnhelp"
        Me.btnhelp.Size = New System.Drawing.Size(77, 29)
        Me.btnhelp.TabIndex = 3
        Me.btnhelp.Text = "Déconnecter"
        Me.btnhelp.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Black
        Me.Panel3.Location = New System.Drawing.Point(137, 44)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(70, 1)
        Me.Panel3.TabIndex = 110
        '
        'Tickets
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(238, 290)
        Me.Controls.Add(Me.menu)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.lib_article)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.prix)
        Me.Controls.Add(Me.code_article)
        Me.Controls.Add(Me.Label2)
        Me.ForeColor = System.Drawing.Color.Wheat
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Tickets"
        Me.Text = "Prix et Tickets..."
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.menu.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents code_article As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents prix As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents obs As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents remq As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents nbr_tickets As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents valide As System.Windows.Forms.Button
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents Annuler_tickets As System.Windows.Forms.Button
    Friend WithEvents Annuler_obs As System.Windows.Forms.Button
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents affichage As System.Windows.Forms.CheckBox
    Friend WithEvents lib_article As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents controleur As Label
    Friend WithEvents Label1 As Label
    Private WithEvents btnmenu As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents dateT As Label
    Private WithEvents menu As Panel
    Private WithEvents Button4 As Button
    Private WithEvents Button2 As Button
    Private WithEvents btnSon As Button
    Private WithEvents btnhelp As Button
    Friend WithEvents Panel3 As Panel
End Class
