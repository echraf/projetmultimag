﻿'Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlServerCe
Public Class start_form

    Private Sub recep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles recep.Click
        If Me.recep.Checked = True Then
            avoir.Checked = False
            bl.Checked = False
            perime.Checked = False
            retour.Checked = False
            magazin.Checked = False
            depot.Checked = False
            Stock.Checked = False
            prefixe = "BE_"
            suffixe = "BC_" & bc.Text
            bc.Focus()

        End If
    End Sub

    Private Sub avoir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles avoir.Click
        If Me.avoir.Checked = True Then
            recep.Checked = False
            bl.Checked = False
            perime.Checked = False
            retour.Checked = False
            magazin.Checked = False
            depot.Checked = False
            Stock.Checked = False
            sortie.Focus()
            prefixe = "AV_"
            suffixe = "Avoir_" & sortie.Text
            operation = "Bs"
        End If
    End Sub

    Private Sub perime_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles perime.Click
        If Me.perime.Checked = True Then
            recep.Checked = False
            avoir.Checked = False
            bl.Checked = False
            retour.Checked = False
            magazin.Checked = False
            depot.Checked = False
            Stock.Checked = False
            sortie.Focus()
            prefixe = "BL_"
            suffixe = "perime_" & sortie.Text
            operation = "Bs"
        End If
    End Sub

    Private Sub bl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bl.Click
        If Me.bl.Checked = True Then
            recep.Checked = False
            avoir.Checked = False
            perime.Checked = False
            retour.Checked = False
            magazin.Checked = False
            depot.Checked = False
            Stock.Checked = False
            sortie.Focus()
            prefixe = "BL_"
            suffixe = sortie.Text
            operation = "Bs"
        End If
    End Sub

    Private Sub retour_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles retour.Click
        If Me.retour.Checked = True Then
            recep.Checked = False
            avoir.Checked = False
            bl.Checked = False
            perime.Checked = False
            magazin.Checked = False
            depot.Checked = False
            Stock.Checked = False
            sortie.Focus()
            prefixe = "BL_"
            suffixe = "Retour_frs_" & sortie.Text
            operation = "Bs"
        End If
    End Sub


    Private Sub magazin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles magazin.Click
        If Me.magazin.Checked = True Then
            recep.Checked = False
            avoir.Checked = False
            bl.Checked = False
            perime.Checked = False
            retour.Checked = False
            depot.Checked = False
            Stock.Checked = False
            prefixe = "IM_"
            suffixe = inv.Text
            operation = "inv"
        End If
    End Sub

    Private Sub depot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles depot.Click
        If Me.depot.Checked = True Then
            recep.Checked = False
            avoir.Checked = False
            bl.Checked = False
            perime.Checked = False
            retour.Checked = False
            magazin.Checked = False
            Stock.Checked = False
            prefixe = "ID_"
            suffixe = inv.Text
            operation = "inv"
        End If
    End Sub
    Private Sub Stock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Stock.Click
        If Me.Stock.Checked = True Then
            recep.Checked = False
            avoir.Checked = False
            bl.Checked = False
            perime.Checked = False
            retour.Checked = False
            magazin.Checked = False
            depot.Checked = False
            Stockk.Focus()
            prefixe = "CT_"
            suffixe = "côntrol_" & Stockk.Text
            operation = "CT"
        End If
    End Sub
    Private Sub valid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles valid.Click
        Dim op As String = ""

        '--------------- Construction prefixe doc ---------------------'
        If recep.Checked = True Then
            prefixe = "BE_"
            suffixe = "BC_" & bc.Text
            operation = "Be"
        End If

        If avoir.Checked = True Then
            prefixe = "AV_"
            suffixe = "Avoir_" & sortie.Text
            operation = "Bs"
        End If

        If perime.Checked = True Then
            prefixe = "BL_"
            suffixe = "perime_" & sortie.Text
            operation = "Bs"
        End If

        If bl.Checked = True Then
            prefixe = "BL_"
            suffixe = sortie.Text
            operation = "Bs"
        End If

        If retour.Checked = True Then
            prefixe = "BL_"
            suffixe = "Retour_Frs_" & sortie.Text
            operation = "Bs"
        End If

        If magazin.Checked = True Then
            prefixe = "IM_"
            suffixe = inv.Text
            operation = "invmag"
        End If

        If depot.Checked = True Then
            prefixe = "ID_"
            suffixe = inv.Text

            operation = "invdepot"
        End If

        If Stock.Checked = True Then
            prefixe = "CT_"
            suffixe = "côntrol_" & stockk.Text
            operation = "CT"
        End If
        '----------------------------------------------------------------'

        If operation = "" Then
            MsgBox("il faut choisir un type de document pointage", MsgBoxStyle.Critical)
            Exit Sub
        End If

        Form1.Show()
        Form1.code_article.Focus()
        Me.Hide()
    End Sub
    Private Sub start_form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim buffer As String
        Dim champ As String
        Dim valeur As String

        '------------------------ lire fichier be_mobile.ini -------------------'
        Try
            Dim FILE_NAME As String = path_application & chemin_ini
            Dim objReader As New System.IO.StreamReader(FILE_NAME)

            Do While objReader.Peek() <> -1

                buffer = objReader.ReadLine()

                If String.IsNullOrEmpty(buffer) Then Continue Do

                champ = get_champ(buffer)
                valeur = get_valeur(buffer)

                Select Case champ

                    Case "chemin_base" : chemin_base = valeur
                    Case "ip_sauv" : ip_sauv = valeur
                    Case "Nom_catalog" : Nom_catalog = valeur
                    Case "Nom_table_sup" : nom_table_sup = valeur
                    Case "Nom_table_zone" : nom_table_zone = valeur
                    Case "Nom_table_opt" : Nom_table_opt = valeur
                    Case "Table_lignes_pointage" : Table_lignes_pointage = valeur
                    Case "Nom_table" : Nom_table = valeur
                    Case "Nom_code" : Nom_code = valeur
                    Case "Nom_libelle" : Nom_libelle = valeur

                End Select
            Loop

            objReader.Close()
        Catch ex As Exception
            MsgBox("fichier ini de pointage est introuvable : " & ex.Message)
            Form0.Show()
            Me.Hide()
        End Try
        recep.Checked = True
        recep_Click(Nothing, Nothing)
        bc.Focus()

    End Sub

    Private Sub bc_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles bc.GotFocus
        bc.SelectAll()
    End Sub

    Private Sub bc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles bc.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then

            If String.IsNullOrEmpty(bc.Text) Then
                Exit Sub
            Else
                bc.SelectAll()
                valid_Click(Nothing, Nothing)
            End If
        End If

    End Sub

    Private Sub bc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bc.TextChanged
        If recep.Checked = False Then
            recep.Checked = True
        End If
        sortie.Text = ""
        inv.Text = ""
    End Sub

    Private Sub sortie_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles sortie.GotFocus
        sortie.SelectAll()
    End Sub

    Private Sub sortie_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles sortie.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then

            If String.IsNullOrEmpty(sortie.Text) Then
                Exit Sub
            Else
                sortie.SelectAll()
                valid_Click(Nothing, Nothing)
            End If
        End If
    End Sub

    Private Sub sortie_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sortie.TextChanged
        If retour.Checked = False And bl.Checked = False And avoir.Checked = False And perime.Checked = False Then
            retour.Checked = True
            retour_Click(Nothing, Nothing)
        End If
        bc.Text = ""
        inv.Text = ""
    End Sub

    Private Sub inv_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles inv.GotFocus
        inv.SelectAll()
    End Sub

    Private Sub inv_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles inv.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then

            If String.IsNullOrEmpty(inv.Text) Then
                Exit Sub
            Else
                inv.SelectAll()
                valid_Click(Nothing, Nothing)
            End If
        End If
    End Sub

    Private Sub inv_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles inv.TextChanged
        If magazin.Checked = False And depot.Checked = False Then
            magazin.Checked = True
            magazin_Click(Nothing, Nothing)
        End If
        sortie.Text = ""
        bc.Text = ""
    End Sub

    Private Sub stockk_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles stockk.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then

            If String.IsNullOrEmpty(stockk.Text) Then
                Exit Sub
            Else
                stockk.SelectAll()
                valid_Click(Nothing, Nothing)
            End If
        End If
    End Sub


    Private Sub stockk_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stockk.TextChanged

    End Sub

    Private Sub Stock_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Stock.CheckedChanged

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        '------ Test if online is trueeee ------------------'
        If Not connexion_begin_test() Then
            MsgBox("Cette page fonctionne seulement en mode connecté. vérifier votre connexion au serveur", MsgBoxStyle.Information, "Serveur non joignable !!!")
            Exit Sub
        End If

        Tickets.Show()
        Me.Hide()
    End Sub
End Class