﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Inventaires
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Inventaires))
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.quitter = New System.Windows.Forms.Button
        Me.rayon = New System.Windows.Forms.ComboBox
        Me.zone = New System.Windows.Forms.ComboBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.depot = New System.Windows.Forms.RadioButton
        Me.magazin = New System.Windows.Forms.RadioButton
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.op = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.valid = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Panel3.Controls.Add(Me.quitter)
        Me.Panel3.Controls.Add(Me.rayon)
        Me.Panel3.Controls.Add(Me.zone)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.op)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(232, 135)
        '
        'quitter
        '
        Me.quitter.Location = New System.Drawing.Point(199, 107)
        Me.quitter.Name = "quitter"
        Me.quitter.Size = New System.Drawing.Size(27, 23)
        Me.quitter.TabIndex = 102
        Me.quitter.Text = "&#"
        '
        'rayon
        '
        Me.rayon.Location = New System.Drawing.Point(55, 73)
        Me.rayon.Name = "rayon"
        Me.rayon.Size = New System.Drawing.Size(174, 23)
        Me.rayon.TabIndex = 24
        '
        'zone
        '
        Me.zone.Location = New System.Drawing.Point(56, 107)
        Me.zone.Name = "zone"
        Me.zone.Size = New System.Drawing.Size(141, 23)
        Me.zone.TabIndex = 19
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Green
        Me.Panel4.Controls.Add(Me.depot)
        Me.Panel4.Controls.Add(Me.magazin)
        Me.Panel4.Location = New System.Drawing.Point(3, 35)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(226, 27)
        '
        'depot
        '
        Me.depot.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.depot.ForeColor = System.Drawing.Color.White
        Me.depot.Location = New System.Drawing.Point(149, 5)
        Me.depot.Name = "depot"
        Me.depot.Size = New System.Drawing.Size(74, 19)
        Me.depot.TabIndex = 14
        Me.depot.Text = "Dépôt"
        '
        'magazin
        '
        Me.magazin.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.magazin.ForeColor = System.Drawing.Color.White
        Me.magazin.Location = New System.Drawing.Point(10, 5)
        Me.magazin.Name = "magazin"
        Me.magazin.Size = New System.Drawing.Size(95, 19)
        Me.magazin.TabIndex = 12
        Me.magazin.Text = "Magazin"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(6, 111)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 21)
        Me.Label5.Text = "Zone:"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(4, 78)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 21)
        Me.Label4.Text = "Rayon:"
        '
        'op
        '
        Me.op.Location = New System.Drawing.Point(99, 3)
        Me.op.Name = "op"
        Me.op.Size = New System.Drawing.Size(130, 23)
        Me.op.TabIndex = 11
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(4, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 21)
        Me.Label3.Text = "Opérateur :"
        '
        'valid
        '
        Me.valid.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.valid.Location = New System.Drawing.Point(163, 250)
        Me.valid.Name = "valid"
        Me.valid.Size = New System.Drawing.Size(72, 25)
        Me.valid.TabIndex = 9
        Me.valid.Text = "&Valider"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(3, 139)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(232, 148)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.Location = New System.Drawing.Point(163, 219)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 25)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "&Accueil"
        '
        'Inventaires
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(238, 290)
        Me.ControlBox = False
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.valid)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "Inventaires"
        Me.Text = "Contrôles et Inventaires"
        Me.Panel3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents rayon As System.Windows.Forms.ComboBox
    Friend WithEvents zone As System.Windows.Forms.ComboBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents depot As System.Windows.Forms.RadioButton
    Friend WithEvents magazin As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents op As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents valid As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents quitter As System.Windows.Forms.Button
End Class
