﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class start_form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.retour = New System.Windows.Forms.RadioButton
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.bc = New System.Windows.Forms.TextBox
        Me.recep = New System.Windows.Forms.RadioButton
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.depot = New System.Windows.Forms.RadioButton
        Me.magazin = New System.Windows.Forms.RadioButton
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.avoir = New System.Windows.Forms.RadioButton
        Me.bl = New System.Windows.Forms.RadioButton
        Me.perime = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.sortie = New System.Windows.Forms.TextBox
        Me.inv = New System.Windows.Forms.TextBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.valid = New System.Windows.Forms.Button
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.stockk = New System.Windows.Forms.TextBox
        Me.Stock = New System.Windows.Forms.RadioButton
        Me.Button6 = New System.Windows.Forms.Button
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'retour
        '
        Me.retour.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.retour.Location = New System.Drawing.Point(118, 35)
        Me.retour.Name = "retour"
        Me.retour.Size = New System.Drawing.Size(91, 19)
        Me.retour.TabIndex = 10
        Me.retour.Text = "Retour Fr"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.bc)
        Me.Panel2.Controls.Add(Me.recep)
        Me.Panel2.Location = New System.Drawing.Point(5, 5)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(228, 38)
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(117, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 21)
        Me.Label2.Text = "Bc:"
        '
        'bc
        '
        Me.bc.Location = New System.Drawing.Point(149, 6)
        Me.bc.Name = "bc"
        Me.bc.Size = New System.Drawing.Size(73, 23)
        Me.bc.TabIndex = 6
        '
        'recep
        '
        Me.recep.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.recep.Location = New System.Drawing.Point(5, 9)
        Me.recep.Name = "recep"
        Me.recep.Size = New System.Drawing.Size(95, 19)
        Me.recep.TabIndex = 4
        Me.recep.Text = "Réception"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Green
        Me.Panel4.Controls.Add(Me.depot)
        Me.Panel4.Controls.Add(Me.magazin)
        Me.Panel4.Location = New System.Drawing.Point(3, 35)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(215, 27)
        '
        'depot
        '
        Me.depot.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.depot.ForeColor = System.Drawing.Color.White
        Me.depot.Location = New System.Drawing.Point(117, 6)
        Me.depot.Name = "depot"
        Me.depot.Size = New System.Drawing.Size(95, 19)
        Me.depot.TabIndex = 14
        Me.depot.Text = "Dépôt"
        '
        'magazin
        '
        Me.magazin.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.magazin.ForeColor = System.Drawing.Color.White
        Me.magazin.Location = New System.Drawing.Point(10, 6)
        Me.magazin.Name = "magazin"
        Me.magazin.Size = New System.Drawing.Size(95, 19)
        Me.magazin.TabIndex = 12
        Me.magazin.Text = "Magazin"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Panel5.Controls.Add(Me.retour)
        Me.Panel5.Controls.Add(Me.avoir)
        Me.Panel5.Controls.Add(Me.bl)
        Me.Panel5.Controls.Add(Me.perime)
        Me.Panel5.Location = New System.Drawing.Point(3, 30)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(218, 59)
        '
        'avoir
        '
        Me.avoir.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.avoir.Location = New System.Drawing.Point(4, 5)
        Me.avoir.Name = "avoir"
        Me.avoir.Size = New System.Drawing.Size(95, 19)
        Me.avoir.TabIndex = 9
        Me.avoir.Text = "Avoir"
        '
        'bl
        '
        Me.bl.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.bl.Location = New System.Drawing.Point(4, 35)
        Me.bl.Name = "bl"
        Me.bl.Size = New System.Drawing.Size(79, 19)
        Me.bl.TabIndex = 8
        Me.bl.Text = "BL"
        '
        'perime
        '
        Me.perime.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.perime.Location = New System.Drawing.Point(118, 5)
        Me.perime.Name = "perime"
        Me.perime.Size = New System.Drawing.Size(91, 19)
        Me.perime.TabIndex = 7
        Me.perime.Text = "Périmé"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(92, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 21)
        Me.Label1.Text = "Sortie:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.sortie)
        Me.Panel1.Location = New System.Drawing.Point(7, 48)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(226, 92)
        '
        'sortie
        '
        Me.sortie.Location = New System.Drawing.Point(148, 4)
        Me.sortie.Name = "sortie"
        Me.sortie.Size = New System.Drawing.Size(73, 23)
        Me.sortie.TabIndex = 7
        '
        'inv
        '
        Me.inv.Location = New System.Drawing.Point(146, 3)
        Me.inv.Name = "inv"
        Me.inv.Size = New System.Drawing.Size(73, 23)
        Me.inv.TabIndex = 11
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Panel3.Controls.Add(Me.Button6)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.inv)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.valid)
        Me.Panel3.Location = New System.Drawing.Point(8, 188)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(225, 97)
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(56, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 21)
        Me.Label3.Text = "Inventaire :"
        '
        'valid
        '
        Me.valid.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.valid.Location = New System.Drawing.Point(147, 68)
        Me.valid.Name = "valid"
        Me.valid.Size = New System.Drawing.Size(72, 25)
        Me.valid.TabIndex = 8
        Me.valid.Text = "&Valider"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Panel6.Controls.Add(Me.stockk)
        Me.Panel6.Controls.Add(Me.Stock)
        Me.Panel6.Location = New System.Drawing.Point(8, 145)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(225, 38)
        '
        'stockk
        '
        Me.stockk.Location = New System.Drawing.Point(80, 6)
        Me.stockk.Name = "stockk"
        Me.stockk.Size = New System.Drawing.Size(138, 23)
        Me.stockk.TabIndex = 6
        '
        'Stock
        '
        Me.Stock.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Stock.Location = New System.Drawing.Point(5, 9)
        Me.Stock.Name = "Stock"
        Me.Stock.Size = New System.Drawing.Size(95, 19)
        Me.Stock.TabIndex = 4
        Me.Stock.Text = "Stock"
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Button6.Location = New System.Drawing.Point(3, 68)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(118, 25)
        Me.Button6.TabIndex = 13
        Me.Button6.Text = "Tickets et Prix"
        '
        'start_form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(238, 290)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "start_form"
        Me.Text = "Avant de commencer..."
        Me.Panel2.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents retour As System.Windows.Forms.RadioButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents bc As System.Windows.Forms.TextBox
    Friend WithEvents recep As System.Windows.Forms.RadioButton
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents depot As System.Windows.Forms.RadioButton
    Friend WithEvents magazin As System.Windows.Forms.RadioButton
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents avoir As System.Windows.Forms.RadioButton
    Friend WithEvents bl As System.Windows.Forms.RadioButton
    Friend WithEvents perime As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents sortie As System.Windows.Forms.TextBox
    Friend WithEvents inv As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents valid As System.Windows.Forms.Button
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents stockk As System.Windows.Forms.TextBox
    Friend WithEvents Stock As System.Windows.Forms.RadioButton
    Friend WithEvents Button6 As System.Windows.Forms.Button
End Class
