﻿
Public Class Form_0

    Private Sub Form_0_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        Application.Exit()
    End Sub

    Private Sub Form_0_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim buffer As String
        Dim champ As String
        Dim valeur As String

        '------------------------ lire fichier be_mobile.ini -------------------'
        Try
            Dim FILE_NAME As String = path_application & "\pointage_Mobile.ini"
            Dim objReader As New System.IO.StreamReader(FILE_NAME)

            Do While objReader.Peek() <> -1

                buffer = objReader.ReadLine()

                If String.IsNullOrEmpty(buffer) Then Continue Do

                champ = get_champ(buffer)
                valeur = get_valeur(buffer)

                Select Case champ

                    Case "chemin_base" : chemin_base = valeur
                    Case "ip_sauv" : ip_sauv = valeur
                    Case "Nom_catalog" : Nom_catalog = valeur
                    Case "Nom_table_sup" : nom_table_sup = valeur
                    Case "Nom_table_zone" : nom_table_zone = valeur
                    Case "Nom_table_opt" : Nom_table_opt = valeur
                    Case "Table_lignes_pointage" : Table_lignes_pointage = valeur
                    Case "Nom_table" : Nom_table = valeur
                    Case "Nom_code" : Nom_code = valeur
                    Case "Nom_libelle" : Nom_libelle = valeur

                End Select
            Loop

            objReader.Close()
        Catch ex As Exception
            MsgBox("Fichier pointage_Mibile.ini erroné ou introuvable : " & ex.Message)
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        entrees.Show()
        entrees.bc.Focus()
        Me.Hide()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        sorties.Show()
        sorties.op.Focus()
        Me.Hide()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Transferts.Show()
        Transferts.op.Focus()
        Me.Hide()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Inventaires.Show()
        Inventaires.op.Focus()
        Me.Hide()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Application.Exit()
    End Sub

    
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        '------ Test if online is trueeee ------------------'
        If Not connexion_begin_test() Then
            MsgBox("Cette page fonctionne seulement en mode connecté. vérifier votre connexion au serveur", MsgBoxStyle.Information, "Serveur non joignable !!!")
            Exit Sub
        End If

        Tickets.Show()
        Me.Hide()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            Dim rep As String
            rep = InputBox("Donner le mot de passe  ", "Mot de passe Opérateur...")
            If rep = "dollars" Then
                Button1.Enabled = True
                Button2.Enabled = True
                Button3.Enabled = True
                Button4.Enabled = True
            Else
                CheckBox1.Checked = False
                Exit Sub
            End If
        Else
            Button1.Enabled = False
            Button2.Enabled = False
            Button3.Enabled = False
            Button4.Enabled = False
        End If
    End Sub
End Class