﻿Public Class sorties

    Private Sub op_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles op.GotFocus
        op.BackColor = Color.Aqua
        op.Select(0, Len(op.Text))
        op.Focus()
    End Sub

    Private Sub sortie_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles op.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then

            If String.IsNullOrEmpty(op.Text) Then
                Exit Sub
            Else
                op.SelectAll()
                valid_Click(Nothing, Nothing)
            End If
        End If
    End Sub

    Private Sub valid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles valid.Click
        If casse.Checked = True Then
            prefixe = "BL_"
            suffixe = "Casse_" & op.Text
            operation = "Bs"
        End If

        If perime.Checked = True Then
            prefixe = "BL_"
            suffixe = "perime_" & op.Text
            operation = "Bs"
        End If

        If vol.Checked = True Then
            prefixe = "BL_"
            suffixe = "Vol_" & op.Text
            operation = "Bs"
        End If

        If retour.Checked = True Then
            prefixe = "BL_"
            suffixe = "Retour_Frs_" & op.Text
            operation = "Bs"
        End If

        If usage.Checked = True Then
            prefixe = "BL_"
            suffixe = "Usage_Mg_" & op.Text
            operation = "Bs"
        End If
        '------------------------------------------------'
        prefixe = Replace(prefixe, " ", "_")
        '-------------------------------------------------'
        Me.Hide()
        Form1.Show()
        ' Form1.code_article.Focus()


    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Form_0.Show()
        Me.Hide()
    End Sub

    Private Sub op_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles op.LostFocus
        op.BackColor = Color.White
    End Sub

    Private Sub sorties_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        retour.Checked = True
    End Sub
End Class