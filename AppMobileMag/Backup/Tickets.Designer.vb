﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Tickets
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lib_article = New System.Windows.Forms.TextBox
        Me.code_article = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.prix = New System.Windows.Forms.Label
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.CheckBox3 = New System.Windows.Forms.CheckBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.Annuler_obs = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.obs = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.Annuler_tickets = New System.Windows.Forms.Button
        Me.valide = New System.Windows.Forms.Button
        Me.remq = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.nbr_tickets = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.DataGrid1 = New System.Windows.Forms.DataGrid
        Me.affichage = New System.Windows.Forms.CheckBox
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lib_article
        '
        Me.lib_article.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lib_article.ForeColor = System.Drawing.Color.Red
        Me.lib_article.Location = New System.Drawing.Point(62, 38)
        Me.lib_article.Name = "lib_article"
        Me.lib_article.ReadOnly = True
        Me.lib_article.Size = New System.Drawing.Size(173, 20)
        Me.lib_article.TabIndex = 13
        Me.lib_article.TabStop = False
        '
        'code_article
        '
        Me.code_article.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.code_article.Location = New System.Drawing.Point(62, 9)
        Me.code_article.Name = "code_article"
        Me.code_article.Size = New System.Drawing.Size(173, 20)
        Me.code_article.TabIndex = 12
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(0, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 23)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Code   :"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(0, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Libellé :"
        '
        'prix
        '
        Me.prix.Font = New System.Drawing.Font("Andalus", 24.0!, System.Drawing.FontStyle.Bold)
        Me.prix.ForeColor = System.Drawing.Color.Red
        Me.prix.Location = New System.Drawing.Point(3, 64)
        Me.prix.Name = "prix"
        Me.prix.Size = New System.Drawing.Size(230, 45)
        Me.prix.TabIndex = 18
        Me.prix.Text = "<Prix>"
        Me.prix.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.TabControl1.Location = New System.Drawing.Point(0, 112)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(239, 178)
        Me.TabControl1.TabIndex = 17
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.CheckBox3)
        Me.TabPage1.Controls.Add(Me.CheckBox2)
        Me.TabPage1.Controls.Add(Me.CheckBox1)
        Me.TabPage1.Controls.Add(Me.Annuler_obs)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.obs)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(231, 149)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Reclamations "
        '
        'CheckBox3
        '
        Me.CheckBox3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.ForeColor = System.Drawing.Color.Blue
        Me.CheckBox3.Location = New System.Drawing.Point(7, 43)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(178, 20)
        Me.CheckBox3.TabIndex = 37
        Me.CheckBox3.Text = "Manque de Ticket"
        '
        'CheckBox2
        '
        Me.CheckBox2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.ForeColor = System.Drawing.Color.Blue
        Me.CheckBox2.Location = New System.Drawing.Point(7, 23)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(178, 20)
        Me.CheckBox2.TabIndex = 36
        Me.CheckBox2.Text = "Défaut de rangement"
        '
        'CheckBox1
        '
        Me.CheckBox1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.Blue
        Me.CheckBox1.Location = New System.Drawing.Point(7, 3)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(134, 20)
        Me.CheckBox1.TabIndex = 35
        Me.CheckBox1.Text = "Prix Incorrect"
        '
        'Annuler_obs
        '
        Me.Annuler_obs.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Annuler_obs.ForeColor = System.Drawing.Color.Red
        Me.Annuler_obs.Location = New System.Drawing.Point(69, 118)
        Me.Annuler_obs.Name = "Annuler_obs"
        Me.Annuler_obs.Size = New System.Drawing.Size(78, 23)
        Me.Annuler_obs.TabIndex = 28
        Me.Annuler_obs.Text = "&Annuler"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Silver
        Me.Button1.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.Location = New System.Drawing.Point(153, 116)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(76, 25)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "&Envoyer"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'obs
        '
        Me.obs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.obs.Location = New System.Drawing.Point(58, 69)
        Me.obs.Multiline = True
        Me.obs.Name = "obs"
        Me.obs.Size = New System.Drawing.Size(170, 41)
        Me.obs.TabIndex = 16
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(4, 69)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 23)
        Me.Label4.TabIndex = 29
        Me.Label4.Text = "OBS   :"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.affichage)
        Me.TabPage2.Controls.Add(Me.Annuler_tickets)
        Me.TabPage2.Controls.Add(Me.valide)
        Me.TabPage2.Controls.Add(Me.remq)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.nbr_tickets)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(231, 149)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Tickets "
        '
        'Annuler_tickets
        '
        Me.Annuler_tickets.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Annuler_tickets.ForeColor = System.Drawing.Color.Red
        Me.Annuler_tickets.Location = New System.Drawing.Point(41, 111)
        Me.Annuler_tickets.Name = "Annuler_tickets"
        Me.Annuler_tickets.Size = New System.Drawing.Size(78, 23)
        Me.Annuler_tickets.TabIndex = 27
        Me.Annuler_tickets.Text = "&Annuler"
        '
        'valide
        '
        Me.valide.BackColor = System.Drawing.Color.Silver
        Me.valide.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.valide.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.valide.Location = New System.Drawing.Point(125, 110)
        Me.valide.Name = "valide"
        Me.valide.Size = New System.Drawing.Size(103, 25)
        Me.valide.TabIndex = 24
        Me.valide.Text = "&Impression"
        Me.valide.UseVisualStyleBackColor = False
        '
        'remq
        '
        Me.remq.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.remq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.remq.ForeColor = System.Drawing.Color.Red
        Me.remq.Location = New System.Drawing.Point(100, 67)
        Me.remq.Name = "remq"
        Me.remq.Size = New System.Drawing.Size(128, 24)
        Me.remq.TabIndex = 21
        Me.remq.TabStop = False
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(-3, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 23)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Remarques   :"
        '
        'nbr_tickets
        '
        Me.nbr_tickets.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nbr_tickets.Location = New System.Drawing.Point(100, 37)
        Me.nbr_tickets.Name = "nbr_tickets"
        Me.nbr_tickets.Size = New System.Drawing.Size(128, 24)
        Me.nbr_tickets.TabIndex = 20
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(0, 37)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(103, 23)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Nbr Tickets   :"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.DataGrid1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 25)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(231, 149)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Liste"
        '
        'DataGrid1
        '
        Me.DataGrid1.AllowDrop = True
        Me.DataGrid1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DataGrid1.CaptionBackColor = System.Drawing.Color.Red
        Me.DataGrid1.CaptionVisible = False
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.FlatMode = True
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(3, 3)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.ParentRowsVisible = False
        Me.DataGrid1.RowHeadersVisible = False
        Me.DataGrid1.RowHeaderWidth = 0
        Me.DataGrid1.Size = New System.Drawing.Size(225, 143)
        Me.DataGrid1.TabIndex = 7
        Me.DataGrid1.TabStop = False
        '
        'affichage
        '
        Me.affichage.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.affichage.ForeColor = System.Drawing.Color.Blue
        Me.affichage.Location = New System.Drawing.Point(8, 8)
        Me.affichage.Name = "affichage"
        Me.affichage.Size = New System.Drawing.Size(100, 23)
        Me.affichage.TabIndex = 35
        Me.affichage.Text = "Affichage"
        '
        'Tickets
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackColor = System.Drawing.Color.Wheat
        Me.ClientSize = New System.Drawing.Size(238, 290)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.prix)
        Me.Controls.Add(Me.lib_article)
        Me.Controls.Add(Me.code_article)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.ForeColor = System.Drawing.Color.Wheat
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Tickets"
        Me.Text = "Prix et Tickets..."
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lib_article As System.Windows.Forms.TextBox
    Friend WithEvents code_article As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents prix As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents obs As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents remq As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents nbr_tickets As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents valide As System.Windows.Forms.Button
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents Annuler_tickets As System.Windows.Forms.Button
    Friend WithEvents Annuler_obs As System.Windows.Forms.Button
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents affichage As System.Windows.Forms.CheckBox
End Class
