﻿Imports System.Data.SqlClient
Imports System.Data.SqlServerCe
Imports System.Media
Imports System.IO



Module Module1
    Public online As Boolean = True
    Public chemin_base As String
    Public chemin_ini As String
    Public prefixe As String
    Public suffixe As String
    Public Nom_catalog As String
    Public Interfaces As Integer
    Public Nom_table As String
    Public nom_table_sup As String
    Public visible_import As Boolean = True
    Public y As Integer = 66
    Public nom_table_zone As String
    Public Nom_table_opt As String
    Public Nom_code As String
    Public Table_lignes_pointage As String
    Public operation As String = ""
    Public ip_sauv As String
    Public code_sup As String
    Public operateur As String = ""
    Public code_origine As String
    Public code_origine_precedent As String
    Public quantite_par_code As Double
    Public Nom_libelle As String
    Public rayon_var = ""
    Public zone_var = ""
    Public dirs As String()
    Public Connexion_serveur_mobile As New SqlConnection()
    Public Connexion_test As New SqlConnection()
    Public path_application As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)
    Public Connexion_Article_locale As New SqlCeConnection()
    Public connectionUser As New SqlCeConnection()
    Public nombre_fichiers_Non_ENV As Integer
    
    Sub Main()
        Dim buffer As String
        Dim champ As String
        Dim valeur As String
        Dim f3 As New hamza
        Dim f2 As New Form_0
        Dim f1 As New Form0
        Dim Att As New Attention

        path_application = Replace(path_application, "file:\", "")

        '------------------------ lire fichier be_mobile.ini -------------------'
        Try
            Dim FILE_NAME As String = path_application & "\pointage_Mobile.ini"
            Dim objReader As New System.IO.StreamReader(FILE_NAME)

            Do While objReader.Peek() <> -1

                buffer = objReader.ReadLine()

                If String.IsNullOrEmpty(buffer) Then Continue Do

                champ = get_champ(buffer)
                valeur = get_valeur(buffer)

                Select Case champ

                    Case "chemin_base" : chemin_base = valeur
                    Case "ip_sauv" : ip_sauv = valeur
                    Case "Nom_catalog" : Nom_catalog = valeur
                    Case "Nom_table_sup" : nom_table_sup = valeur
                    Case "Nom_table_zone" : nom_table_zone = valeur
                    Case "Nom_table_opt" : Nom_table_opt = valeur
                    Case "Table_lignes_pointage" : Table_lignes_pointage = valeur
                    Case "Nom_table" : Nom_table = valeur
                    Case "Nom_code" : Nom_code = valeur
                    Case "Nom_libelle" : Nom_libelle = valeur
                    Case "Interface" : Interfaces = Val(valeur)
                End Select
            Loop

            objReader.Close()
        Catch ex As Exception
            MsgBox("fichier ini de pointage est introuvable : " & ex.Message)
        End Try
        '-----------tester s'il ya des fichiers non envoyés vers le serveur--------------------'
        nombre_fichiers_Non_ENV = test_fichiers_NE()
        If nombre_fichiers_Non_ENV <> 0 Then
            Application.Run(Att)
        Else
            Select Case Interfaces
                Case 1 : Application.Run(f1)
                Case 2 : Application.Run(f2)
                Case 3 : Application.Run(f3)
            End Select
        End If
        '--------------------------------------------------------------------------'
    End Sub
    Public Function nb_records_sdf() As Integer
        Dim req As String = "select count(code_article) from lignes_be_mobile "
        Dim cmd As New SqlCeCommand(req, connectionUser)
        Dim nbr_reader As SqlCeDataReader
        nb_records_sdf = 0

        Try
            nbr_reader = cmd.ExecuteReader()
            If nbr_reader.Read Then nb_records_sdf = nbr_reader.GetSqlInt32(0)
            nbr_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob de comptage des lignes du sdf local file --> " & ex.Message)
        End Try


    End Function
    
    Public Function cherche_article(ByVal code As String) As String

        Dim req As String = "select " & Nom_libelle & " from articles Where " & Nom_code & "= '" & code & "'"
        Dim cmd_serveur As New SqlCeCommand(req, Connexion_Article_locale)
        Dim rdr_serveur As SqlCeDataReader
        cherche_article = ""
        '-------------------------------------- lecture lib article ----------------------------------------'
        Try
            rdr_serveur = cmd_serveur.ExecuteReader()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            If rdr_serveur.Read() Then
                If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(0).ToString) Then
                    cherche_article = rdr_serveur.GetString(0).ToString
                    code_origine = code
                End If

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '---------------------------------------------------------------------------------------------------'

        rdr_serveur.Close()
        rdr_serveur.Dispose()


    End Function
    Public Function cherche_article_online(ByVal code As String) As String

        Dim req As String = "select " & Nom_libelle & " from article Where " & Nom_code & "= '" & code & "'"
        Dim cmd_serveur As New SqlCommand(req, Connexion_serveur_mobile)
        Dim rdr_serveur As SqlDataReader
        cherche_article_online = ""
        '-------------------------------------- lecture lib article ----------------------------------------'
        Try
            rdr_serveur = cmd_serveur.ExecuteReader()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            If rdr_serveur.Read() Then
                If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(0).ToString) Then
                    cherche_article_online = rdr_serveur.GetString(0).ToString
                    code_origine = code
                End If

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '---------------------------------------------------------------------------------------------------'

        rdr_serveur.Close()
        rdr_serveur.Dispose()


    End Function
    Function cherche_codesup(ByVal indice As Integer) As String
        Dim req As String = "select code_sup from lignes_be_mobile where indice =" & indice
        Dim cmd As New SqlCeCommand(req, connectionUser)
        Dim nbr_reader As SqlCeDataReader
        cherche_codesup = ""

        Try
            nbr_reader = cmd.ExecuteReader()

            If nbr_reader.Read Then cherche_codesup = nbr_reader.GetSqlString(0)
            nbr_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob de recherche code_supp dans table bidon --> " & ex.Message)
        End Try

    End Function

    Public Function cherche_code_origine(ByVal code As String) As String

        Dim req As String = "select  CODE from code_sup Where code_2 = '" & code & "'"
        Dim cmd_serveur As New SqlCeCommand(req, Connexion_Article_locale)
        Dim rdr_serveur As SqlCeDataReader
        cherche_code_origine = code
        '-------------------------------------- lecture lib article ----------------------------------------'
        Try
            rdr_serveur = cmd_serveur.ExecuteReader()

            If rdr_serveur.Read() Then
                If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(0).ToString) Then
                    cherche_code_origine = rdr_serveur.GetString(0).ToString
                End If

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '---------------------------------------------------------------------------------------------------'

        rdr_serveur.Close()
        rdr_serveur.Dispose()


    End Function


    Public Function cherche_article_sup(ByVal code As String, ByRef qte As Double) As String

        Dim req As String = "select  CODE,qt from code_sup Where code_2 = '" & code & "'"
        Dim cmd_serveur As New SqlCeCommand(req, Connexion_Article_locale)
        Dim rdr_serveur As SqlCeDataReader
        cherche_article_sup = ""
        '-------------------------------------- lecture lib article ----------------------------------------'
        Try
            rdr_serveur = cmd_serveur.ExecuteReader()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            If rdr_serveur.Read() Then
                If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(0).ToString) Then
                    cherche_article_sup = rdr_serveur.GetString(0).ToString
                    code_sup = code
                    If Val(CDbl(rdr_serveur.GetFloat(1))) <> 0 Then qte = CDbl(rdr_serveur.GetFloat(1))

                End If

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '---------------------------------------------------------------------------------------------------'

        rdr_serveur.Close()
        rdr_serveur.Dispose()


    End Function
    Public Function cherche_article_sup_online(ByVal code As String, ByRef qte As Double) As String

        Dim req As String = "select  CODE,qt from code_sup Where code_2 = '" & code & "'"
        Dim cmd_serveur As New SqlCommand(req, Connexion_serveur_mobile)
        Dim rdr_serveur As SqlDataReader
        cherche_article_sup_online = ""
        '-------------------------------------- lecture lib article ----------------------------------------'
        Try
            rdr_serveur = cmd_serveur.ExecuteReader()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            If rdr_serveur.Read() Then
                If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(0).ToString) Then
                    cherche_article_sup_online = rdr_serveur.GetString(0).ToString
                    code_sup = code
                    If Val(CDbl(rdr_serveur.GetFloat(1))) <> 0 Then qte = CDbl(rdr_serveur.GetFloat(1))

                End If

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '---------------------------------------------------------------------------------------------------'

        rdr_serveur.Close()
        rdr_serveur.Dispose()


    End Function

    Public Function cherche_qte_par_code(ByVal code As String) As Double
        Dim qte As Double

        If String.IsNullOrEmpty(cherche_article_sup(code, qte)) Then
            cherche_qte_par_code = 1
        Else
            cherche_qte_par_code = qte
        End If

    End Function

    Public Function test_fichiers_NE() As Integer
        dirs = Directory.GetFiles(path_application & "\BONS\", "*_NE.txt")
        Return dirs.Length
    End Function
    Public Function test_pointage(ByRef op As String, ByVal zone As String, ByVal rayon As String) As Boolean
        Dim etat_zone As String = ""
        Dim req As String = "select operateur,etat from " & nom_table_zone & " where rayon = '" & rayon & "' and zone = '" & zone & "'"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim test_pointage_reader As SqlDataReader
        op = ""
        Try
            test_pointage_reader = cmd.ExecuteReader()

            If test_pointage_reader.Read Then
                If Not String.IsNullOrEmpty(test_pointage_reader.GetString(0).ToString) Then op = test_pointage_reader.GetString(0).ToString
                If Not String.IsNullOrEmpty(test_pointage_reader.GetString(1).ToString) Then etat_zone = test_pointage_reader.GetString(1).ToString
                If Trim(etat_zone) = "pointée" Then
                    test_pointage = True
                End If
            End If


            test_pointage_reader.Close()
            cmd.Dispose()


        Catch ex As Exception
            MsgBox("prob test pointage --> " & ex.Message)
        End Try

    End Function

    Public Function etat_pointage(ByRef op As String, ByVal zone As String, ByVal rayon As String) As String
        Dim etat_zone As String = ""
        Dim req As String = "select operateur,etat from " & nom_table_zone & " where rayon = '" & rayon & "' and zone = '" & zone & "'"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim test_pointage_reader As SqlDataReader

        op = ""
        etat_pointage = ""

        Try
            test_pointage_reader = cmd.ExecuteReader()

            If test_pointage_reader.Read Then
                If Not String.IsNullOrEmpty(test_pointage_reader.GetString(0).ToString) Then op = test_pointage_reader.GetString(0).ToString
                If Not String.IsNullOrEmpty(test_pointage_reader.GetString(1).ToString) Then etat_pointage = test_pointage_reader.GetString(1).ToString

            End If


            test_pointage_reader.Close()
            cmd.Dispose()


        Catch ex As Exception
            MsgBox("prob etat pointage --> " & ex.Message)
        End Try

    End Function

    Public Sub connexion_begin()

        If Connexion_serveur_mobile.State <> Data.ConnectionState.Closed Then Connexion_serveur_mobile.Close()

        Connexion_serveur_mobile.ConnectionString = "Data Source=" & chemin_base & ";Initial Catalog=" & Nom_catalog & ";User Id=frigui;Password=rosa;"

        Try
            If Connexion_serveur_mobile.State <> Data.ConnectionState.Open Then Connexion_serveur_mobile.Open()
        Catch ex As Exception
            MsgBox("probléme de connexion au serveur SQL")
        End Try

    End Sub
    Public Sub connexion_end()
        Try
            If Connexion_serveur_mobile.State <> Data.ConnectionState.Closed Then Connexion_serveur_mobile.Close()
            Connexion_serveur_mobile.Dispose()
        Catch ex As Exception
            MsgBox("prob fermeture connexion --> " & ex.Message)
        End Try

    End Sub
    Public Function connexion_begin_test() As Boolean

        If Connexion_test.State <> Data.ConnectionState.Closed Then Connexion_test.Close()

        Connexion_test.ConnectionString = "Data Source=" & chemin_base & ";Initial Catalog=" & Nom_catalog & ";User Id=frigui;Password=rosa;Connection Timeout=3"

        Try
            If Connexion_test.State <> Data.ConnectionState.Open Then Connexion_test.Open()
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    Public Function get_champ(ByVal buffer As String) As String
        Dim start As Integer
        Dim endd As Integer
        On Error GoTo fin

        start = 1
        endd = InStr(start, buffer, ":") - 1
        get_champ = Trim(Mid(buffer, 1, endd))

        Exit Function
fin:    MsgBox("la structure du fichier .ini est erronée")
    End Function

    Public Function get_valeur(ByVal buffer As String)
        Dim start As Integer
        Dim endd As Integer
        Dim longeur As Integer
        On Error GoTo fin

        start = InStr(1, buffer, "#") + 1
        endd = InStr(start, buffer, "#")
        longeur = endd - start

        get_valeur = Trim(Mid(buffer, start, longeur))

        Exit Function
fin:    MsgBox("la structure du fichier .ini est erronée")
    End Function


    Public Sub maj_etat_zone(ByVal etat As String, ByVal zone As String, ByVal rayon As String)
        Dim req As String = "update  " & nom_table_zone & " set etat = '" & etat & "' where zone = '" & zone & "' and rayon = '" & rayon & "'"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("prob update etat zone --> " & ex.Message)
        End Try

        cmd.Dispose()
    End Sub
    Public Function val2(ByVal ts As String) As Object

        If (String.IsNullOrEmpty(ts)) Then
            val2 = 0
            Exit Function
        End If

        For i = 0 To Len(ts)
            If Mid(ts, i + 1, 1) = "," Then Mid(ts, i + 1, 1) = "."
        Next i

        val2 = ts
    End Function
    Public Function val3(ByVal ts As String) As Object

        If (String.IsNullOrEmpty(ts)) Then
            val3 = 0
            Exit Function
        End If

        For i = 0 To Len(ts)
            If Mid(ts, i + 1, 1) = "." Then Mid(ts, i + 1, 1) = ","
        Next i

        val3 = ts
    End Function
    Public Sub beeper(ByVal limite As Integer)
        Dim x As New SoundPlayer
        Dim i As Integer
        For i = 0 To limite
            x.Play()
        Next i

    End Sub

    Public Function Get_price(ByVal Code As String) As Double
        Dim req As String = "select Prix1 from Prix_caisse where CODART ='" & Code & "'"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim price_reader As SqlDataReader

        Try
            price_reader = cmd.ExecuteReader()

            If price_reader.Read Then
                If Not String.IsNullOrEmpty(price_reader.GetDouble(0).ToString) Then
                    Get_price = price_reader.GetDouble(0)
                End If
            End If

            price_reader.Close()
            cmd.Dispose()

        Catch ex As Exception
            MsgBox("Prob lecture prix online --> " & ex.Message)
        End Try



    End Function

End Module
