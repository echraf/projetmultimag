﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class sorties
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(sorties))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.usage = New System.Windows.Forms.RadioButton
        Me.retour = New System.Windows.Forms.RadioButton
        Me.casse = New System.Windows.Forms.RadioButton
        Me.vol = New System.Windows.Forms.RadioButton
        Me.perime = New System.Windows.Forms.RadioButton
        Me.op = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.valid = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Panel1.Controls.Add(Me.usage)
        Me.Panel1.Controls.Add(Me.retour)
        Me.Panel1.Controls.Add(Me.casse)
        Me.Panel1.Controls.Add(Me.vol)
        Me.Panel1.Controls.Add(Me.perime)
        Me.Panel1.Controls.Add(Me.op)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(232, 120)
        '
        'usage
        '
        Me.usage.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.usage.Location = New System.Drawing.Point(7, 89)
        Me.usage.Name = "usage"
        Me.usage.Size = New System.Drawing.Size(205, 19)
        Me.usage.TabIndex = 17
        Me.usage.Text = "Usage Magazin"
        '
        'retour
        '
        Me.retour.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.retour.Location = New System.Drawing.Point(141, 64)
        Me.retour.Name = "retour"
        Me.retour.Size = New System.Drawing.Size(77, 19)
        Me.retour.TabIndex = 16
        Me.retour.Text = "Retour"
        '
        'casse
        '
        Me.casse.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.casse.Location = New System.Drawing.Point(7, 39)
        Me.casse.Name = "casse"
        Me.casse.Size = New System.Drawing.Size(95, 19)
        Me.casse.TabIndex = 15
        Me.casse.Text = "Casse"
        '
        'vol
        '
        Me.vol.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.vol.Location = New System.Drawing.Point(7, 64)
        Me.vol.Name = "vol"
        Me.vol.Size = New System.Drawing.Size(79, 19)
        Me.vol.TabIndex = 14
        Me.vol.Text = "Vol"
        '
        'perime
        '
        Me.perime.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.perime.Location = New System.Drawing.Point(141, 39)
        Me.perime.Name = "perime"
        Me.perime.Size = New System.Drawing.Size(77, 19)
        Me.perime.TabIndex = 13
        Me.perime.Text = "Périmé"
        '
        'op
        '
        Me.op.Location = New System.Drawing.Point(99, 4)
        Me.op.Name = "op"
        Me.op.Size = New System.Drawing.Size(122, 23)
        Me.op.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(7, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 21)
        Me.Label1.Text = "Opérateur :"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(10, 129)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(121, 153)
        '
        'valid
        '
        Me.valid.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.valid.Location = New System.Drawing.Point(163, 257)
        Me.valid.Name = "valid"
        Me.valid.Size = New System.Drawing.Size(72, 25)
        Me.valid.TabIndex = 9
        Me.valid.Text = "&Valider"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.Location = New System.Drawing.Point(163, 226)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 25)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "&Accueil"
        '
        'sorties
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(238, 290)
        Me.ControlBox = False
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.valid)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "sorties"
        Me.Text = "Mouvements De Sorties ..."
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents op As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents usage As System.Windows.Forms.RadioButton
    Friend WithEvents retour As System.Windows.Forms.RadioButton
    Friend WithEvents casse As System.Windows.Forms.RadioButton
    Friend WithEvents vol As System.Windows.Forms.RadioButton
    Friend WithEvents perime As System.Windows.Forms.RadioButton
    Friend WithEvents valid As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
