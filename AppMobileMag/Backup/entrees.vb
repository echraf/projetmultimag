﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlServerCe
Public Class entrees

    Private Sub valid_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles valid.Click

        If Not String.IsNullOrEmpty(bc.Text) And gratuit.Checked Then
            MsgBox("le BE peut être soit gratuit ou suite à un BC " & vbNewLine & "Mais pas les deux!!!", MsgBoxStyle.Critical)
            Exit Sub
        End If
        '---------------------------------------------------'
        If Not verifier_bc(Val(bc.Text)) And Not gratuit.Checked Then MsgBox("BC non valide") : bc.Focus() : bc.SelectionLength = bc.TextLength : Exit Sub
        '----------------------------------------------------'
        prefixe = "BE_"
        operation = "Be"
        operateur = op.Text

        If operateur <> "" Then
            If Not gratuit.Checked Then
                suffixe = operateur & "_BC_" & bc.Text
            Else
                suffixe = operateur & "_GRATUIT"
            End If

        Else
            If Not gratuit.Checked Then
                suffixe = "BC_" & bc.Text
            Else
                suffixe = "BC_GRATUIT"
            End If
        End If
        '------------------------------------------------'

        '   suffixe = suffixe & "GRATUIT"

        '------------------------------------------------'
        Form1.Show()
        Form1.code_article.Focus()
        Me.Hide()
    End Sub

    Private Sub entrees_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bc.Focus()
    End Sub

    Private Sub bc_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles bc.GotFocus
        bc.BackColor = Color.Aqua
        bc.Select(0, Len(op.Text))
        bc.Focus()
    End Sub

    Private Sub bc_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles bc.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then
            If String.IsNullOrEmpty(bc.Text) Then
                Exit Sub
            Else
                bc.SelectAll()
                op.Focus()
                'valid_Click_1(Nothing, Nothing)
            End If
        End If
    End Sub

    Private Function verifier_bc(ByVal bc As Integer) As Boolean
        Dim req As String = "select count(ord) from LIGNE_BCFOUR Where LBE_NUMBE = " & bc
        Dim cmd_serveur As New SqlCommand(req, Connexion_serveur_mobile)
        Dim rdr_serveur As SqlDataReader

        verifier_bc = False

        connexion_begin()
        '--------------------------------------------------------'
        Try
            rdr_serveur = cmd_serveur.ExecuteReader()

            If rdr_serveur.Read() Then
                If Val(rdr_serveur.GetInt32(0)) <> 0 Then
                    verifier_bc = True
                End If
            End If

        Catch ex As Exception
            MsgBox("Erreur de verif num bc " & ex.Message)
        End Try
        '---------------------------------------------------------'

        rdr_serveur.Close()
        connexion_end()

    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Form_0.Show()
        Me.Hide()
    End Sub

    Private Sub CheckBox1_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gratuit.CheckStateChanged

    End Sub

    Private Sub bc_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles bc.LostFocus
        bc.BackColor = Color.White
    End Sub

    Private Sub bc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bc.TextChanged

    End Sub

    Private Sub op_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles op.GotFocus
        op.BackColor = Color.Aqua
        op.Select(0, Len(op.Text))
        op.Focus()
    End Sub

    Private Sub op_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles op.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then
            valid_Click_1(Nothing, Nothing)
        End If
    End Sub

    Private Sub op_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles op.LostFocus
        op.BackColor = Color.White
    End Sub

    Private Sub op_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles op.TextChanged

    End Sub
End Class