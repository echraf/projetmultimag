﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class hamza
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label
        Me.bc = New System.Windows.Forms.TextBox
        Me.ProgressBar = New System.Windows.Forms.ProgressBar
        Me.MAHDIA = New System.Windows.Forms.Button
        Me.Pointage = New System.Windows.Forms.Button
        Me.Update = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(24, 135)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 21)
        Me.Label2.Text = "Note :"
        '
        'bc
        '
        Me.bc.Location = New System.Drawing.Point(81, 133)
        Me.bc.Name = "bc"
        Me.bc.Size = New System.Drawing.Size(133, 23)
        Me.bc.TabIndex = 13
        '
        'ProgressBar
        '
        Me.ProgressBar.Location = New System.Drawing.Point(24, 61)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(190, 21)
        Me.ProgressBar.Value = 70
        Me.ProgressBar.Visible = False
        '
        'MAHDIA
        '
        Me.MAHDIA.Font = New System.Drawing.Font("Andalus", 14.0!, System.Drawing.FontStyle.Bold)
        Me.MAHDIA.Location = New System.Drawing.Point(19, 231)
        Me.MAHDIA.Name = "MAHDIA"
        Me.MAHDIA.Size = New System.Drawing.Size(195, 31)
        Me.MAHDIA.TabIndex = 12
        Me.MAHDIA.Text = "Quitter"
        '
        'Pointage
        '
        Me.Pointage.Font = New System.Drawing.Font("Andalus", 14.0!, System.Drawing.FontStyle.Bold)
        Me.Pointage.Location = New System.Drawing.Point(24, 88)
        Me.Pointage.Name = "Pointage"
        Me.Pointage.Size = New System.Drawing.Size(190, 39)
        Me.Pointage.TabIndex = 11
        Me.Pointage.Text = "Pointage"
        '
        'Update
        '
        Me.Update.Font = New System.Drawing.Font("Andalus", 14.0!, System.Drawing.FontStyle.Bold)
        Me.Update.Location = New System.Drawing.Point(24, 18)
        Me.Update.Name = "Update"
        Me.Update.Size = New System.Drawing.Size(190, 37)
        Me.Update.TabIndex = 9
        Me.Update.Text = "Mise à jour BD"
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Button6.Location = New System.Drawing.Point(19, 179)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(195, 33)
        Me.Button6.TabIndex = 15
        Me.Button6.Text = "Tickets et Prix"
        '
        'hamza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(238, 290)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.bc)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.MAHDIA)
        Me.Controls.Add(Me.Pointage)
        Me.Controls.Add(Me.Update)
        Me.Name = "hamza"
        Me.Text = "Avant de commencer ..."
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents bc As System.Windows.Forms.TextBox
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents MAHDIA As System.Windows.Forms.Button
    Friend WithEvents Pointage As System.Windows.Forms.Button
    Friend WithEvents Update As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
End Class
