﻿Imports System.Data.SqlClient
Imports System.Data.SqlServerCe
Public Class Reclamation
    Dim ds As DataSet = New DataSet()
    Dim impression As String = "IMP_" & Date.Now.ToString("yyyy-MM-dd") & "_" & TimeOfDay.ToString("hh-mm-ss")
    Dim codesup As String

    Private Sub Tickets_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim f3 As New hamza
        Dim f2 As New Form_0
        Dim f1 As New Form0
        '---------------------- Confirmation from user ----------------------------'
        If nb_records_sdf() > 0 Then
            Dim rep As Integer
            rep = MsgBox("êtes vous sûr de vouloir Quitter et Abondonner les tickets en cours ??? ", MsgBoxStyle.YesNo, "Attention")
            If rep = 7 Then
                e.Cancel = True
                Exit Sub
            End If
        End If
        '----------------------------- Supprimer la base sdf locale ------------------------------'
        Dim req As String = "delete from lignes_be_mobile"
        Dim cmd As New SqlCeCommand(req, connectionUser)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("prob delete from sdf file --> " & ex.Message)
        End Try

        cmd.Dispose()

        '----------------------- Refrech datagrid --------------------------'
        Dim cmd2 As SqlCeCommand = New SqlCeCommand("SELECT indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice", connectionUser)
        Dim da As New SqlCeDataAdapter(cmd2)
        ds.Clear()
        da.Fill(ds)
        DataGrid1.Refresh()
        cmd2.Dispose()

        '-------------- Retourner à l'interface source --------------'
        Select Case Interfaces
            Case 1 : Me.Hide() : Form0.Show()
            Case 2 : Me.Hide() : Form_0.Show()
            Case 3 : Me.Hide() : hamza.Show()
        End Select
    End Sub



    Private Sub code_article_GotFocus1(ByVal sender As Object, ByVal e As System.EventArgs) Handles code_article.GotFocus
        code_article.BackColor = Color.Aqua
        code_article.Select(0, Len(code_article.Text))
        code_article.Focus()
    End Sub

    Private Sub code_article_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles code_article.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then

            Dim lib_art As String
            Dim qte As Double = 1

            connexion_begin()
            If code_article.Text = "" Then Exit Sub

            code_origine = ""
            quantite_par_code = 1
            code_sup = ""

            lib_art = cherche_article_online(Trim(code_article.Text))

            If Not String.IsNullOrEmpty(lib_art) Then
                lib_article.Text = lib_art
            Else
                codesup = cherche_article_sup_online(Trim(code_article.Text), qte)
                If Not String.IsNullOrEmpty(codesup) Then
                    lib_art = cherche_article_online(codesup)
                    quantite_par_code = qte
                Else
                    lib_art = "Non enregistré"
                    code_origine = code_article.Text
                End If
            End If

            lib_article.Text = lib_art
            prix.Text = Get_price(Trim(code_origine)).ToString
            connexion_end()
            Application.DoEvents()

            '-------------------- Mode vérificateur des prix ------------------'
            '       If affichage.Checked Then
            code_article.Focus()
                code_article.Select(0, code_article.TextLength)
                Exit Sub
            End If
            '------------------------------------------------------------------'

            Select Case TabControl1.SelectedIndex
                Case 0 : obs.Text = "" : obs.Focus()
                '        Case 1 : nbr_tickets.Focus()
                '   Case 2 : TabControl1.SelectedIndex = 1 : nbr_tickets.Focus()
        End Select

        '  End If

    End Sub

    Private Sub code_article_LostFocus1(ByVal sender As Object, ByVal e As System.EventArgs) Handles code_article.LostFocus
        code_article.BackColor = Color.White
    End Sub

    Private Sub obs_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles obs.GotFocus
        obs.BackColor = Color.Aqua
        obs.Select(0, Len(obs.Text))
    End Sub

    Private Sub obs_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles obs.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then
            Button1_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub obs_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles obs.LostFocus
        obs.BackColor = Color.White
    End Sub


    Private Sub remq_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        'remq.BackColor = Color.White

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim reclamation As String = "REC_" & Date.Now.ToString("yyyy-MM-dd") & "_" & TimeOfDay.ToString("hh-mm-ss")

        '------------------ controles ---------------------'
        If String.IsNullOrEmpty(code_article.Text) Then
            MsgBox("Champs code vide!!!", MsgBoxStyle.Critical, "Attention")
            code_article.Focus()
            Exit Sub
        End If

        If String.IsNullOrEmpty(obs.Text) Then
            MsgBox("Champs Obs vide!!!", MsgBoxStyle.Critical, "Attention")
            obs.Focus()
            Exit Sub
        End If

        '---------Ajouter ligne OBS dans sql server mobile ----------------------'
        connexion_begin()
        Dim req_insert_server_mobile As String = "insert into " & Table_lignes_pointage & " (nom_file, indice, code_article, lib_article, qte_article,obs) values ('" & reclamation & "',1,'" & code_origine & "','" & lib_article.Text & "',0,'" & obs.Text & "')"
        Dim cmd_serveur_mobile As New SqlCommand(req_insert_server_mobile, Connexion_serveur_mobile)
        Try
            cmd_serveur_mobile.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Erreur Réseau ou reclamation trops longue !!!")
            Exit Sub
        End Try

        'MsgBox("Observation envoyée concernant :" & lib_article.Text)
        connexion_end()
        '--------- INIT INTERFACE --------------'
        CheckBox1.Checked = False
        CheckBox2.Checked = False
        CheckBox3.Checked = False
        code_article.Text = ""
        obs.Text = ""
        prix.Text = "<Prix>"
        lib_article.Text = ""
        code_article.Focus()

    End Sub

    Private Sub valide_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)


        If nb_records_sdf() = 0 Then
            MsgBox("La table de tickets est vide", MsgBoxStyle.Critical)
            code_article.Focus()
            Exit Sub
        End If

        connexion_begin()
        '-------------------------- Lire Table sdf local des tickets ---------------------------'
        Dim req_read_sdf As String = "SELECT nom_file, indice, code_article, lib_article, qte_article, obs FROM lignes_be_mobile order by indice"
        Dim cmd_read_sdf As New SqlCeCommand(req_read_sdf, connectionUser)
        Dim sdf_data_reader As SqlCeDataReader
        Dim req_insert_server_mobile As String

        sdf_data_reader = cmd_read_sdf.ExecuteReader()

        Do While sdf_data_reader.Read()

            req_insert_server_mobile = "insert into " & Table_lignes_pointage & " (nom_file, indice, code_article, lib_article, qte_article,obs) values ('" & sdf_data_reader.GetString(0).ToString & "'," & Val(sdf_data_reader.GetInt32(1)) & ",'" & sdf_data_reader.GetString(2).ToString & "','" & sdf_data_reader.GetString(3).ToString & "'," & val2(sdf_data_reader.GetSqlDouble(4).ToString) & ",'" & sdf_data_reader.GetSqlString(5).ToString & "')"

            Dim cmd_serveur_mobile As New SqlCommand(req_insert_server_mobile, Connexion_serveur_mobile)

            '---------Ajouter lignes dans sql server mobile ----------------------'
            Try
                cmd_serveur_mobile.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox("Problème d'insersion des tickets dans le serveur--> " & ex.Message)
            End Try

            req_insert_server_mobile = Nothing
            cmd_serveur_mobile.Dispose()
        Loop

        sdf_data_reader.Close()

        MsgBox("Oppération terminée avec succé", MsgBoxStyle.Information)
        connexion_end()
        '--------------------------- Nettoyer interface et base locale ----------------------------'
        Dim req As String = "delete from lignes_be_mobile"
        Dim cmd As New SqlCeCommand(req, connectionUser)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Prob delete from sdf file --> " & ex.Message)
        End Try

        cmd.Dispose()

        '----------------------- refrech datagrid --------------------------'
        Dim cmd2 As SqlCeCommand = New SqlCeCommand("SELECT indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice", connectionUser)
        Dim da As New SqlCeDataAdapter(cmd2)
        ds.Clear()
        da.Fill(ds)
        DataGrid1.Refresh()
        cmd2.Dispose()
        '-----------------------------------------------------------------------------------------'
        code_article.Text = ""
        ' nbr_tickets.Text = ""
        lib_article.Text = ""
        prix.Text = "<Prix>"
        code_article.Focus()

    End Sub


    Private Sub Annuler_tickets_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rep As Integer
        rep = MsgBox("êtes vous sûr de vouloir supprimer les tickets en cours ", MsgBoxStyle.YesNo, "Attention")
        If rep = 7 Then
            Exit Sub
        End If

        '----------------------------- supprimer la base sdf locale ------------------------------'
        Dim req As String = "delete from lignes_be_mobile"
        Dim cmd As New SqlCeCommand(req, connectionUser)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("prob delete from sdf file --> " & ex.Message)
        End Try

        cmd.Dispose()

        '----------------------- refrech datagrid --------------------------'
        Dim cmd2 As SqlCeCommand = New SqlCeCommand("SELECT indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice", connectionUser)
        Dim da As New SqlCeDataAdapter(cmd2)
        ds.Clear()
        da.Fill(ds)
        DataGrid1.Refresh()
        cmd2.Dispose()
        '------------------------ INIT Champs ----------------------------------------------'
        code_article.Text = ""
        '  nbr_tickets.Text = ""
        lib_article.Text = ""
        prix.Text = "<Prix>"
        code_article.Focus()
    End Sub

    Private Sub Annuler_obs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Annuler_obs.Click
        obs.Text = ""
        obs.Focus()
    End Sub


    Private Sub DataGrid1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGrid1.DoubleClick
        Dim indice_datagrid As Integer
        Dim qte_supp As Double

        Dim code_modif As String

        '-------------- Confirmation from User -------------------------'
        Dim rep As Integer
        rep = MsgBox("êtes vous sûr de vouloir supprimer le tickets séléctionné?? ", MsgBoxStyle.YesNo, "Attention")
        If rep = 7 Then
            Exit Sub
        End If

        '------- extraire Code de ticket sélectionnée from datagrid (meme si c'est un code+)------'
        indice_datagrid = DataGrid1(DataGrid1.CurrentRowIndex, 0)
        code_modif = cherche_codesup(indice_datagrid)
        If code_modif = "" Then code_modif = DataGrid1(DataGrid1.CurrentRowIndex, 1)
        qte_supp = DataGrid1(DataGrid1.CurrentRowIndex, 3)
        '------------------- Supprimer le ticket dans sdf -------------------------'
        Dim req3 As String = "delete from  lignes_be_mobile where code_article = '" & code_modif & "' and qte_article =" & qte_supp
        Dim cmd3 As New SqlCeCommand(req3, connectionUser)

        Try
            cmd3.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("prob suppression ticket depuis le sdf local--> " & ex.Message)
        End Try

        cmd3.Dispose()
        Application.DoEvents()

        '------------------- Mise a jour des indices après supp du tiket depuis le sdf ---------------------------------'
        Dim req4 As String = "update  lignes_be_mobile set indice = indice - 1 where indice > " & indice_datagrid
        Dim cmd4 As New SqlCeCommand(req4, connectionUser)

        Try
            cmd4.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("prob de décrimenation des indices après supp du ticket--> " & ex.Message)
        End Try

        cmd4.Dispose()
        Application.DoEvents()

        '----------------------- refrech datagrid --------------------------'
        Dim cmd2 As SqlCeCommand = New SqlCeCommand("SELECT indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice", connectionUser)
        Dim da As New SqlCeDataAdapter(cmd2)
        ds.Clear()
        da.Fill(ds)
        DataGrid1.Refresh()
        cmd2.Dispose()
        code_article.Focus()
        Application.DoEvents()

    End Sub

    Private Sub TabControl1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.Click
        code_article.Focus()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            If Not String.IsNullOrEmpty(Trim(obs.Text)) Then
                obs.Text = obs.Text & "||Prix Incorrect"
            Else
                obs.Text = obs.Text & "Prix Incorrect"
            End If
        Else
            If InStr(obs.Text, "||Prix Incorrect") <> 0 Then
                obs.Text = Replace(obs.Text, "||Prix Incorrect", "")
            Else
                obs.Text = Replace(obs.Text, "Prix Incorrect", "")
            End If
        End If
        If obs.Text Like "||*" Then obs.Text = Mid(obs.Text, 3)
        obs.Focus()
        obs.Select(obs.TextLength, 0)
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then
            If Not String.IsNullOrEmpty(Trim(obs.Text)) Then
                obs.Text = obs.Text & "||Défaut de rangement"
            Else
                obs.Text = obs.Text & "Défaut de rangement"
            End If
        Else
            If InStr(obs.Text, "||Défaut de rangement") <> 0 Then
                obs.Text = Replace(obs.Text, "||Défaut de rangement", "")
            Else
                obs.Text = Replace(obs.Text, "Défaut de rangement", "")
            End If
        End If
        If obs.Text Like "||*" Then obs.Text = Mid(obs.Text, 3)
        obs.Focus()
        obs.Select(obs.TextLength, 0)
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            If Not String.IsNullOrEmpty(Trim(obs.Text)) Then
                obs.Text = obs.Text & "||Manque de Ticket"
            Else
                obs.Text = obs.Text & "Manque de Ticket"
            End If
        Else
            If InStr(obs.Text, "||Manque de Ticket") <> 0 Then
                obs.Text = Replace(obs.Text, "||Manque de Ticket", "")
            Else
                obs.Text = Replace(obs.Text, "Manque de Ticket", "")
            End If
        End If
        If obs.Text Like "||*" Then obs.Text = Mid(obs.Text, 3)
        obs.Focus()
        obs.Select(obs.TextLength, 0)
    End Sub


End Class