﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Form0
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form0))
        Me.Tanyour = New System.Windows.Forms.Button()
        Me.SALTANIA = New System.Windows.Forms.Button()
        Me.MAHDIA = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.conf = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.conf, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Tanyour
        '
        Me.Tanyour.BackColor = System.Drawing.Color.White
        Me.Tanyour.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tanyour.Location = New System.Drawing.Point(24, 86)
        Me.Tanyour.Name = "Tanyour"
        Me.Tanyour.Size = New System.Drawing.Size(190, 46)
        Me.Tanyour.TabIndex = 0
        Me.Tanyour.Text = "Tanyour"
        Me.Tanyour.UseVisualStyleBackColor = False
        '
        'SALTANIA
        '
        Me.SALTANIA.BackColor = System.Drawing.Color.White
        Me.SALTANIA.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SALTANIA.Location = New System.Drawing.Point(24, 138)
        Me.SALTANIA.Name = "SALTANIA"
        Me.SALTANIA.Size = New System.Drawing.Size(190, 46)
        Me.SALTANIA.TabIndex = 1
        Me.SALTANIA.Text = "SALTANIA"
        Me.SALTANIA.UseVisualStyleBackColor = False
        '
        'MAHDIA
        '
        Me.MAHDIA.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.MAHDIA.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MAHDIA.Location = New System.Drawing.Point(24, 190)
        Me.MAHDIA.Name = "MAHDIA"
        Me.MAHDIA.Size = New System.Drawing.Size(190, 46)
        Me.MAHDIA.TabIndex = 2
        Me.MAHDIA.Text = "MAHDIA"
        Me.MAHDIA.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(157, 262)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(81, 28)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "Quitter"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.SeaShell
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(180, 37)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Muti Magasin"
        '
        'conf
        '
        Me.conf.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.conf.BackColor = System.Drawing.Color.Transparent
        Me.conf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.conf.Image = CType(resources.GetObject("conf.Image"), System.Drawing.Image)
        Me.conf.Location = New System.Drawing.Point(191, 3)
        Me.conf.Name = "conf"
        Me.conf.Size = New System.Drawing.Size(43, 44)
        Me.conf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.conf.TabIndex = 8
        Me.conf.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SeaGreen
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.conf)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(238, 52)
        Me.Panel1.TabIndex = 10
        '
        'Form0
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(238, 290)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.MAHDIA)
        Me.Controls.Add(Me.SALTANIA)
        Me.Controls.Add(Me.Tanyour)
        Me.Font = New System.Drawing.Font("Bookman Old Style", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinimizeBox = False
        Me.Name = "Form0"
        Me.Text = "Avant de commencer ..."
        CType(Me.conf, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Tanyour As System.Windows.Forms.Button
    Friend WithEvents SALTANIA As System.Windows.Forms.Button
    Friend WithEvents MAHDIA As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents conf As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel1 As Panel
End Class
